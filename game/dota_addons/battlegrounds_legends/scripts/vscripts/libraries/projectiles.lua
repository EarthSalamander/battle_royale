-------------------------------------------------------------------------------------------------------
-- lua based Tracking Projectiles manager, allowing for some custom and dynamic tracking projectiles.
-------------------------------------------------------------------------------------------------------

TrackingProjectiles = TrackingProjectiles or class({})

function TrackingProjectiles:Projectile( params )
	--PrintTable(params)

	params.hCasterPosition = params.hCaster:GetAbsOrigin()
	params.hTargetPositionOriginal = params.hTargetPosition

	if params.hTargetPosition then
		local direction = (params.hTargetPosition - params.hCasterPosition):Normalized()
		params.hTargetPosition = direction * params.flMaxDistance + params.hCasterPosition
	end

--	if params.hTarget == "dummy" then
--		params.hTarget = CreateUnitByName("npc_dummy_unit", params.hTargetPosition, false, nil, nil, params.hCaster:GetTeamNumber())

--		if params.flExpireTime == nil then
--			params.flExpireTime = GameRules:GetGameTime() + 10.0
--		end

--		Timers:CreateTimer(params.flExpireTime, function()
--			UTIL_Remove(params.hTarget)
--		end)
--	end

	--Set creation time in the parameters
	params.creation_time = GameRules:GetGameTime()
	
	--Fetch initial projectile location
	local projectile
	if params.vSpawnOrigin then
		projectile = params.vSpawnOrigin
	elseif params.iSourceAttachment then
		projectile = params.hCaster:GetAttachmentOrigin( params.iSourceAttachment )
	else
		projectile = params.hCasterPosition
	end
	--Make the particle
	local particle = ParticleManager:CreateParticle( params.EffectName, PATTACH_CUSTOMORIGIN, params.hCaster)
	--Source CP
	ParticleManager:SetParticleControl( particle, 0, projectile )

	--TargetCP
	if params.bLinearProjectile then
		ParticleManager:SetParticleControl(particle, 1, Vector(params.flMaxDistance, params.iMoveSpeed, 0))
	else
		ParticleManager:SetParticleControlEnt( particle, 1, params.hTarget, PATTACH_POINT_FOLLOW, "attach_hitloc", params.hTarget:GetAbsOrigin(), true )
	end
	--Speed CP
	ParticleManager:SetParticleControl( particle, 2, Vector( params.iMoveSpeed, 0, 0 ) )
	--Color CP
	if params.vColor then
		ParticleManager:SetParticleControl( particle, 4, params.vColor )
	end
	if params.vColor2 then
		ParticleManager:SetParticleControl( particle, 5, params.vColor2 )
	end

	-- Creating a projectileID so it can be referred to later
	local projectileID = {projectile=projectile,particle=particle}
	TrackingProjectiles:Think(params,projectileID)
	return projectileID
end

function TrackingProjectiles:Think(params,projectileID)
	local visionRadius = params.flVisionRadius or 0
	local acceleration = params.flAcceleration or 1
	acceleration = math.pow(acceleration,1/32) -- Converting it to 1/32th second

	Timers:CreateTimer(function()
		-- Check if the destroy function has been called
		if projectileID.destroy then
			ParticleManager:DestroyParticle( projectileID.particle, false )
			ParticleManager:ReleaseParticleIndex(projectileID.particle)
			pcall(params.OnProjectileDestroy, params, projectileID)
			return nil
		end
		-- Check if the timer for the projectile has ran out
		if params.flExpireTime and GameRules:GetGameTime() - params.creation_time > params.flExpireTime then
			ParticleManager:DestroyParticle( projectileID.particle, false )
			ParticleManager:ReleaseParticleIndex(projectileID.particle)
			pcall(params.OnProjectileDestroy, params, projectileID)
			return nil
		end
		-- If there is a new speed, store that
		if projectileID.newSpeed then
			params.iMoveSpeed = projectileID.flSpeed
			projectileID.flSpeed = nil
		end
		-- If there is a new acceleration, store that
		if projectileID.Acceleration then
			acceleration = projectileID.flAcceleration
			projectileID.flAcceleration = nil
		end
		-- If there is a new target, store that
		if projectileID.hTarget then
			params.hTarget = projectileID.hTarget
			projectileID.hTarget = nil
		end

		-- Multiply speed by acceleration
		params.iMoveSpeed = params.iMoveSpeed * acceleration

		--Move the projectile towards the target and update the position
		if params.bBounceOnTree and GridNav:IsNearbyTree(projectileID.projectile, 40, true) then
			print("BOUNCE!")
			RotatePosition(projectileID.projectile, QAngle(0, 180, 0), projectileID.projectile + params.hTargetPosition)
		else
			projectileID.projectile = projectileID.projectile + ( params.hTargetPosition - projectileID.projectile ):Normalized() * params.iMoveSpeed * 1/32
			projectileID.position = projectileID.projectile
		end

		-- Add vision
		if visionRadius then
			AddFOWViewer(params.hCaster:GetTeam(),projectileID.projectile,visionRadius,2/32,true)
		end

		-- Check if projectile has hit a platform
		if params.bDestroyOnGroundHit and GetGroundPosition(projectileID.projectile,nil).z - 50 > projectileID.projectile.z then
			-- Platform hit
			ParticleManager:DestroyParticle( projectileID.particle, false )
			ParticleManager:ReleaseParticleIndex(projectileID.particle)
			pcall(params.OnProjectileDestroy, params, projectileID)
			return nil
		end

		-- Check if projectile has hit a wall
		if params.bDestroyOnWallHit and GridNav:IsWall(projectiles) then
			-- Platform hit
			ParticleManager:DestroyParticle( projectileID.particle, false )
			ParticleManager:ReleaseParticleIndex(projectileID.particle)
			pcall(params.OnProjectileDestroy, params, projectileID)
			return nil
		end

		-- Check if projectile has hit a tree
		if GridNav:IsNearbyTree(projectileID.position, 40, true) then
			-- Platform hit
			if params.bDestroyOnTreeHit then
				ParticleManager:DestroyParticle( projectileID.particle, false )
				ParticleManager:ReleaseParticleIndex(projectileID.particle)
				pcall(params.OnProjectileDestroy, params, projectileID)
				return nil
			end
		end

		if params.bLinearProjectile then
			local target_flag = DOTA_UNIT_TARGET_FLAG_NONE

			if params.iUnitTargetFlag then
				target_flag = params.iUnitTargetFlag
			end

			if not params.hit_enemies_table then
				params.hit_enemies_table = {}
			end

			local enemies = FindUnitsInRadius(params.hCaster:GetTeamNumber(), projectileID.projectile, nil, params.flRadius, params.iUnitTargetTeam, params.iUnitTargetType, target_flag, FIND_CLOSEST, false)

			for _, enemy in pairs(enemies) do
				local is_enemy_hit = false

				for _, hit_enemies in pairs(params.hit_enemies_table) do
					if hit_enemies == enemy then
						is_enemy_hit = true
						break
					end
				end

				if is_enemy_hit == false then
					table.insert(params.hit_enemies_table, enemy)
					ApplyDamage({victim = enemy, attacker = params.hCaster, ability = params.Ability, damage = params.damage, damage_type = DAMAGE_TYPE_PHYSICAL})

					if params.sSoundDamage then
						enemy:EmitSound(params.sSoundDamage)
					end

					if params.EffectNameOnHit then
						local nFXIndex = ParticleManager:CreateParticle( params.EffectNameOnHit, PATTACH_CUSTOMORIGIN, enemy )
						ParticleManager:SetParticleControlEnt( nFXIndex, 0, enemy, PATTACH_POINT_FOLLOW, "attach_hitloc", enemy:GetAbsOrigin(), true )
						ParticleManager:ReleaseParticleIndex( nFXIndex )
					end
				end

				if params.bDeleteOnHit == true then
					ParticleManager:DestroyParticle( projectileID.particle, false )
					ParticleManager:ReleaseParticleIndex(projectileID.particle)
					pcall(params.OnProjectileDestroy, params, projectileID)

					return nil
				end

				break
			end
		end

		--Check the distance to the target
--		if (params.hTargetPosition - projectileID.projectile):Length() - params.flRadius < params.iMoveSpeed * 1/32 then -- Length2D() / Length()!
--			--Target has reached destination!
--			TrackingProjectiles:OnProjectileHitUnit( params,projectileID )
--			ParticleManager:DestroyParticle( projectileID.particle, false )
--			ParticleManager:ReleaseParticleIndex(projectileID.particle)
--			return nil
--		-- Check if the target is too far
--		elseif params.flMaxDistance and (params.hCasterPosition - projectileID.projectile):Length() > params.flMaxDistance then
		if params.flMaxDistance and (params.hCasterPosition - projectileID.projectile):Length() > params.flMaxDistance then
			ParticleManager:DestroyParticle( projectileID.particle, false )
			ParticleManager:ReleaseParticleIndex(projectileID.particle)
			pcall(params.OnProjectileDestroy, params, projectileID)
			return nil
		else
			return 1/32
		end
	end)
end

function TrackingProjectiles:GetProjectilePosition(projectileID)
	return projectileID.position
end

-- Use this to destroy the particle at any time you want it to
function TrackingProjectiles:DestroyProjectile(projectileID)
	projectileID.destroy = true
end

-- Function to redirect to a new target
function TrackingProjectiles:ChangeTarget(projectileID,hTarget)
	projectileID.hTarget = hTarget
end
-- Function to update the speed
function TrackingProjectiles:ChangeSpeed(projectileID,flSpeed)
	projectileID.flSpeed = flSpeed
end
-- Function to update the acceleration
function TrackingProjectiles:ChangeAcceleration(projectileID,flAcceleration)
	projectileID.flAcceleration = flAcceleration
end
--Called when the projectile hits the target, params contains the params of the projectile plus a creation_time field.
function TrackingProjectiles:OnProjectileHitUnit( params,projectileID )
	--[[print( params.hCaster:GetUnitName() .. '\'s particle hit ' .. params.hTarget:GetUnitName() .. ' after ' ..
		( GameRules:GetGameTime() - params.creation_time ) .. ' seconds.' )]]
--	if not params.bProjectileDodged then
		pcall(params.OnProjectileHitUnit, params, projectileID)
--	end
end
--[[
-- Based on the hooking done in statcollection.
-- This makes sure that when a projectile is dodgeable and the ProjectileManager.ProjectileDodge function gets called you dodge the projectile.
-- Not sure if this makes these projectiles dodgeable with the default spells
function TrackingProjectiles:hookFunctions()
	local this = self
	
	local oldProjectileDodge = ProjectileManager.ProjectileDodge
	ProjectileManager.ProjectileDodge = function(projectileManager,unit)
		-- Set the unit do be dodging
		if unit then
			unit.dodged = true
		end
		-- Run the real function for other projectiles
		oldProjectileDodge(projectileManager,unit)
	end
end
--]]
function TrackingProjectiles:init()
--	self:hookFunctions()    
end

TrackingProjectiles:init()

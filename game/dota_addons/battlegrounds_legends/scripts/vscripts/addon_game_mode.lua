-- This is the entry-point to your game mode and should be used primarily to precache models/particles/sounds/etc

require('internal/util')
require('gamemode')

function Precache( context )
	DebugPrint("[BAREBONES] Performing pre-load precache")

	LinkLuaModifier("modifier_bush_invisible", "components/modifiers/modifier_bush_invisible.lua", LUA_MODIFIER_MOTION_NONE)
	LinkLuaModifier("modifier_companion", "components/modifiers/modifier_companion.lua", LUA_MODIFIER_MOTION_NONE)
	LinkLuaModifier("modifier_prevent_attack", "components/modifiers/modifier_prevent_attack.lua", LUA_MODIFIER_MOTION_NONE)
	LinkLuaModifier("modifier_invulnerable_hidden", "components/modifiers/modifier_invulnerable_hidden.lua", LUA_MODIFIER_MOTION_NONE)
	LinkLuaModifier("modifier_truesight_aura", "components/modifiers/modifier_truesight_aura.lua", LUA_MODIFIER_MOTION_NONE)
	LinkLuaModifier("modifier_behemoth_ai", "components/modifiers/modifier_behemoth_ai.lua", LUA_MODIFIER_MOTION_NONE)

	PrecacheItemByNameSync("item_gem_grab", context)
	PrecacheItemByNameSync("item_power_cube", context)
	PrecacheItemByNameSync("item_bounty_rune", context)

	PrecacheResource("soundfile", "soundevents/game_sounds_alchemist.vsndevts", context) -- Chemical Rage mutation
	PrecacheResource("soundfile", "soundevents/game_sounds_custom.vsndevts", context)
end

-- Create the game mode when we activate
function Activate()
	GameRules.GameMode = GameMode()
	GameRules.GameMode:_InitGameMode()
end

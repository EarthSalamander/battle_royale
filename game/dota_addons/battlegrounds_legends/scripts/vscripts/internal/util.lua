function DebugPrint(...)
	local spew = Convars:GetInt('barebones_spew') or -1
	if spew == -1 and BAREBONES_DEBUG_SPEW then
		spew = 1
	end

	if spew == 1 then
		print(...)
	end
end

function DebugPrintTable(...)
	local spew = Convars:GetInt('barebones_spew') or -1
	if spew == -1 and BAREBONES_DEBUG_SPEW then
		spew = 1
	end

	if spew == 1 then
		PrintTable(...)
	end
end

function PrintTable(t, indent, done)
	--print ( string.format ('PrintTable type %s', type(keys)) )
	if type(t) ~= "table" then return end

	done = done or {}
	done[t] = true
	indent = indent or 0

	local l = {}
	for k, v in pairs(t) do
		table.insert(l, k)
	end

	table.sort(l)
	for k, v in ipairs(l) do
		-- Ignore FDesc
		if v ~= 'FDesc' then
			local value = t[v]

			if type(value) == "table" and not done[value] then
				done [value] = true
				print(string.rep ("\t", indent)..tostring(v)..":")
				PrintTable (value, indent + 2, done)
			elseif type(value) == "userdata" and not done[value] then
				done [value] = true
				print(string.rep ("\t", indent)..tostring(v)..": "..tostring(value))
				PrintTable ((getmetatable(value) and getmetatable(value).__index) or getmetatable(value), indent + 2, done)
			else
				if t.FDesc and t.FDesc[v] then
					print(string.rep ("\t", indent)..tostring(t.FDesc[v]))
				else
					print(string.rep ("\t", indent)..tostring(v)..": "..tostring(value))
				end
			end
		end
	end
end

-- Colors
COLOR_NONE = '\x06'
COLOR_GRAY = '\x06'
COLOR_GREY = '\x06'
COLOR_GREEN = '\x0C'
COLOR_DPURPLE = '\x0D'
COLOR_SPINK = '\x0E'
COLOR_DYELLOW = '\x10'
COLOR_PINK = '\x11'
COLOR_RED = '\x12'
COLOR_LGREEN = '\x15'
COLOR_BLUE = '\x16'
COLOR_DGREEN = '\x18'
COLOR_SBLUE = '\x19'
COLOR_PURPLE = '\x1A'
COLOR_ORANGE = '\x1B'
COLOR_LRED = '\x1C'
COLOR_GOLD = '\x1D'


function DebugAllCalls()
	if not GameRules.DebugCalls then
		print("Starting DebugCalls")
		GameRules.DebugCalls = true

		debug.sethook(function(...)
			local info = debug.getinfo(2)
			local src = tostring(info.short_src)
			local name = tostring(info.name)
			if name ~= "__index" then
				print("Call: ".. src .. " -- " .. name .. " -- " .. info.currentline)
			end
		end, "c")
	else
		print("Stopped DebugCalls")
		GameRules.DebugCalls = false
		debug.sethook(nil, "c")
	end
end

function ChangeCameraTarget(iPlayerID, hEntity, iNewTargetID)
	if BATTLE_ROYALE_USE_CAMERA_LOCKED == false then return end
	if iPlayerID == nil or iPlayerID == -1 then return end
	local hEntityID

	if hEntity then
		hEntityID = hEntity:GetPlayerID()
	else
		for _, hero in pairs(HeroList:GetAllHeroes()) do
			if hero:IsAlive() then
				iNewTargetID = hero:GetPlayerID()

				-- not working
--				PlayerResource:GetSelectedHeroEntity(iNewTargetID):MakeVisibleToTeam(PlayerResource:GetTeam(iPlayerID), 9999.9)

				break
			end
		end
	end

	hEntityID = iNewTargetID

	PLAYER_CAMERA_TARGET_ID[iPlayerID] = hEntity:GetPlayerID()
	PlayerResource:SetCameraTarget(iPlayerID, hEntity)
end

function FindTower(bTeamNumber)
	for _, tower in pairs(Entities:FindAllByClassname("npc_dota_tower")) do
		if tower:GetTeamNumber() == bTeamNumber then
			return tower
		end
	end

	return nil
end

function CountdownTimer()
	nCOUNTDOWNTIMER = nCOUNTDOWNTIMER - 1
	local t = nCOUNTDOWNTIMER
--	print(t)
	local minutes = math.floor(t / 60)
	local seconds = t - (minutes * 60)
	local m10 = math.floor(minutes / 10)
	local m01 = minutes - (m10 * 10)
	local s10 = math.floor(seconds / 10)
	local s01 = seconds - (s10 * 10)
	local broadcast_gametimer = {
		timer_minute_10 = m10,
		timer_minute_01 = m01,
		timer_second_10 = s10,
		timer_second_01 = s01,
	}

	CustomGameEventManager:Send_ServerToAllClients("countdown", broadcast_gametimer)

	if GetMapName() == "battle_royale" then
		CustomNetTables:SetTableValue("mutation_info", "tempest", broadcast_gametimer)
		CustomGameEventManager:Send_ServerToAllClients("update_mutations", {})
	end

--	if t <= 120 then
--		CustomGameEventManager:Send_ServerToAllClients("time_remaining", broadcast_gametimer)
--	end

	if t == 4 then
		if GetMapName() == "battle_royale" and BATTLE_ROYALE_MUTATION["positive"] == "chemical_rage" then
			-- play a specific particle + sound to announce chemical rage item will spawn soon? (also use overthrow advertize?)
			local pos = BATTLE_ROYALE_CHEMICAL_RUNE_SPAWNERS[RandomInt(1, #BATTLE_ROYALE_CHEMICAL_RUNE_SPAWNERS)]
			local vector = Vector(pos[1], pos[2], pos[3])
			local radius = 300

			local warning = ParticleManager:CreateParticle("particles/econ/events/darkmoon_2017/darkmoon_generic_aoe.vpcf", PATTACH_CUSTOMORIGIN, caster)
			ParticleManager:SetParticleControl(warning, 0, vector)
			ParticleManager:SetParticleControl(warning, 1, Vector(radius, 0, 0))
			ParticleManager:SetParticleControl(warning, 2, Vector(6, 0, 1))
			ParticleManager:SetParticleControl(warning, 3, Vector(200, 0, 0))
			ParticleManager:SetParticleControl(warning, 4, vector)

			GridNav:DestroyTreesAroundPoint(vector, radius, false)

			for i = 2, 13 do
				AddFOWViewer(i, vector, radius, 7.0, false)
			end

			local dummy = CreateUnitByName("npc_dummy_unit", vector, true, nil, nil, 4)
			dummy:EmitSound("Hero_Alchemist.UnstableConcoction.Fuse")

			Timers:CreateTimer(5.0, function()
				SpawnItemEntity(vector, false, "item_chemical_rune")
				dummy:StopSound("Hero_Alchemist.UnstableConcoction.Fuse")
				dummy:EmitSound("Hero_Alchemist.UnstableConcoction.Stun")

				Timers:CreateTimer(2.0, function()
					UTIL_Remove(dummy)
				end)
			end)
		end
	end

	if t == 0 and PREVENT_TIMER_RUNNING == false then
		if GetMapName() == "battle_royale" then
			BattleRoyale.CURRENT_ARENA_SIZE = math.max(BattleRoyale.CURRENT_ARENA_SIZE - BATTLE_ROYALE_ARENA_SIZE_REDUCTION, BATTLE_ROYALE_ARENA_SIZE_REDUCTION)
--			print("Reduce tempest size to:", BattleRoyale.CURRENT_ARENA_SIZE)

			local tempest_particle_duration = BATTLE_ROYALE_FOG_SPAWN_DELAY

			if BattleRoyale.CURRENT_ARENA_SIZE <= BATTLE_ROYALE_ARENA_MIN_SIZE then
				tempest_particle_duration = 3600
			end

			if BattleRoyale.CURRENT_ARENA_SIZE == BATTLE_ROYALE_ARENA_MIN_SIZE then
				PREVENT_TIMER_RUNNING = true
			else
				SetTimer(BATTLE_ROYALE_FOG_SPAWN_DELAY)
			end

			if BATTLE_ROYALE_FOG_REDUCE_WITH_ARENA_SIZE == true then
				BattleRoyale:SetFogVision(BattleRoyale.CURRENT_ARENA_SIZE)
			end

			BattleRoyale:TempestParticle(BattleRoyale.CURRENT_ARENA_SIZE, tempest_particle_duration + 1)
		elseif GetMapName() == "boss_fight" then
			BEHEMOTH_SPAWN_COUNT = BEHEMOTH_SPAWN_COUNT + 1
			CustomNetTables:SetTableValue("game_score", "behemoth_count", {BEHEMOTH_SPAWN_COUNT})
			PREVENT_TIMER_RUNNING = true

			local team = nil

			if GameMode:GetRoundScore(2) > GameMode:GetRoundScore(3) then
				team = 2
			elseif GameMode:GetRoundScore(2) < GameMode:GetRoundScore(3) then
				team = 3
			end

			if team == nil then
				GemGrab:CheckEndGame()
				Notifications:TopToAll({text="Draw!", duration=5.0})
				SetTimer(BOSS_FIGHT_BASE_TIMER)
				PREVENT_TIMER_RUNNING = false
			else
				local tower = FindTower(team)

				if tower then
					local team_name = "Radiant"
					if team == 3 then team_name = "Dire" end
					Notifications:TopToAll({text=team_name.." launched a Behemoth level "..GameMode:GetRoundScore(team).."!", duration=5.0})
					local Behemoth = CreateUnitByName("npc_boss_behemoth", tower:GetAbsOrigin(), true, nil, nil, team)
					Behemoth:AddNewModifier(Behemoth, self, "modifier_behemoth_ai", {})
				end

				GameMode:UpdateRoundScore(team, -GameMode:GetRoundScore(team))
			end
		end
	end
end

function SetTimer(time)
--	print("Set the timer to: " .. time)
	nCOUNTDOWNTIMER = time
end

function ShowBossBar(hUnit)
	local icon = "npc_dota_hero_nevermore"

	local light_color = "#009933"
	local dark_color = "#003311"

	if hUnit:GetTeamNumber() == 3 then
--		icon = "npc_dota_tower"
		light_color = "#ff6600"
		dark_color = "#320000"
	end

	CustomGameEventManager:Send_ServerToAllClients("show_boss_hp", {
		boss_name = hUnit:GetUnitName(),
		team = hUnit:GetTeamNumber(),
		boss_icon = icon,
		light_color = light_color,
		dark_color = dark_color,
		boss_health = hUnit:GetHealth(),
		boss_max_health = hUnit:GetMaxHealth()
	})
end

function SpawnItemEntity(vSpawnPoint, bOverbossSource, item_name)
	EmitGlobalSound("Item.PickUpGemWorld")

	local item_string = MAIN_ITEM_DROP

	if item_name then
		item_string = item_name
	end

	local newItem = CreateItem(item_string, nil, nil)

	newItem:SetPurchaseTime(0)

	if newItem:IsPermanent() and newItem:GetShareability() == ITEM_FULLY_SHAREABLE then
		item:SetStacksWithOtherOwners(true)
	end

	local drop = CreateItemOnPositionForLaunch(vSpawnPoint, newItem)
	local dropRadius

	if bOverbossSource == true then
		dropRadius = RandomFloat(LOOT_SPAWN_RADIUS_MIN, LOOT_SPAWN_RADIUS_MAX)
	else
		dropRadius = RandomFloat(LOOT_SPAWN_RADIUS_MIN / 2, LOOT_SPAWN_RADIUS_MAX / 2)
	end

	local launch_delay = 0.75
	local pos = vSpawnPoint + RandomVector(dropRadius)

	newItem:LaunchLootInitialHeight(false, 0, 500, launch_delay, pos)

	Timers:CreateTimer(launch_delay - 0.1, function()
		GridNav:DestroyTreesAroundPoint(pos, 100, false)
	end)
end

function IsTeamAlive(iTeamNumber)
	if PlayerResource:GetPlayerCountForTeam(iTeamNumber) > 0 then
		for _, hero in pairs(HeroList:GetAllHeroes()) do
			if hero:GetTeamNumber() == iTeamNumber then
				if hero:IsAlive() then
					return true
				end
			end
		end
	end

	return false
end

function CDOTA_BaseNPC:FindAlliedHero()
	for _, hero in pairs(HeroList:GetAllHeroes()) do
		if hero ~= self and hero:GetTeamNumber() == self:GetTeamNumber() then
			return hero
		end
	end
end

-- credits to yahnich for the following
function CDOTA_BaseNPC:IsFakeHero()
	if self:IsIllusion() or (self:HasModifier("modifier_monkey_king_fur_army_soldier") or self:HasModifier("modifier_monkey_king_fur_army_soldier_hidden")) or self:IsTempestDouble() or self:IsClone() then
		return true
	else return false end
end

function CDOTA_BaseNPC:IsRealHero()
	if not self:IsNull() then
		return self:IsHero() and not ( self:IsIllusion() or self:IsClone() ) and not self:IsFakeHero()
	end
end

-- Yahnich's calculate distance and direction functions
function CalculateDistance(ent1, ent2)
	local pos1 = ent1
	local pos2 = ent2
	if ent1.GetAbsOrigin then pos1 = ent1:GetAbsOrigin() end
	if ent2.GetAbsOrigin then pos2 = ent2:GetAbsOrigin() end
	local distance = (pos1 - pos2):Length2D()
	return distance
end

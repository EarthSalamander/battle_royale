-- Credits: EarthSalamander
-- Date (DD/MM/YYYY): 10/02/2019

LinkLuaModifier("modifier_scepter_of_mastery_passive", "components/items/item_treasure_scepter_of_mastery.lua", LUA_MODIFIER_MOTION_NONE)

item_treasure_scepter_of_mastery = class({})

function item_treasure_scepter_of_mastery:GetIntrinsicModifierName()
	return "modifier_scepter_of_mastery_passive"
end

--------------------------------------------------------------

modifier_scepter_of_mastery_passive = class({})

function modifier_scepter_of_mastery_passive:GetTexture()
	return "modifiers/item_treasure_scepter_of_mastery"
end

function modifier_scepter_of_mastery_passive:IsHidden() return false end
function modifier_scepter_of_mastery_passive:IsPurgable() return false end
function modifier_scepter_of_mastery_passive:IsPurgeException() return false end
function modifier_scepter_of_mastery_passive:IsDebuff() return false end

function modifier_scepter_of_mastery_passive:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_EXTRA_MANA_BONUS,
	}
end

function modifier_scepter_of_mastery_passive:GetModifierExtraManaBonus()
	return self:GetAbility():GetSpecialValueFor("bonus_mana")
end

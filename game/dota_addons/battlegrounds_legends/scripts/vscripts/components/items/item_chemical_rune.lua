-- Credits: 
-- Coder: EarthSalamander #42
-- Date: 04/04/2019

item_chemical_rune = class({})

function item_chemical_rune:GetBehavior()
	return DOTA_ABILITY_BEHAVIOR_IMMEDIATE
end

function item_chemical_rune:OnSpellStart()
	if IsServer() then
		self:GetCaster():AddNewModifier(self:GetCaster(), self, "modifier_frostrose_chemical_rage", {duration=10.0})
		self:SpendCharge()
--		print(self:GetCurrentCharges())

--		if self:GetCurrentCharges() == 0 then
--			UTIL_Remove(self)
--		end
	end
end

LinkLuaModifier("modifier_frostrose_chemical_rage", "components/items/item_chemical_rune.lua", LUA_MODIFIER_MOTION_NONE)

modifier_frostrose_chemical_rage = modifier_frostrose_chemical_rage or class ({})

function modifier_frostrose_chemical_rage:GetTexture()
	return "alchemist_chemical_rage"
end

function modifier_frostrose_chemical_rage:OnCreated()
	self.damage_increase = self:GetAbility():GetSpecialValueFor("bonus_damage_pct")
	self.ms_increase = self:GetAbility():GetSpecialValueFor("bonus_movespeed_pct")
	self.size_increase = self:GetAbility():GetSpecialValueFor("bonus_size")

	if IsServer() then
		if self:GetParent():GetUnitName() == "npc_dota_hero_alchemist" then
			self:GetParent():StartGesture(ACT_DOTA_ALCHEMIST_CHEMICAL_RAGE_START)
		end

		self:GetParent():EmitSound("Hero_Alchemist.ChemicalRage.Cast")

		local particle_acid_aura_fx = ParticleManager:CreateParticle("particles/units/heroes/hero_alchemist/alchemist_chemical_rage.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent())
		ParticleManager:SetParticleControl(particle_acid_aura_fx, 0, self:GetParent():GetAbsOrigin())
		ParticleManager:SetParticleControl(particle_acid_aura_fx, 1, self:GetParent():GetAbsOrigin())
		self:AddParticle(particle_acid_aura_fx, false, false, -1, false, false)
	end
end

function modifier_frostrose_chemical_rage:OnRefresh()
	self:OnCreated()
end

function modifier_frostrose_chemical_rage:OnDestroy()
	if IsServer() then
		if self:GetParent():GetUnitName() == "npc_dota_hero_alchemist" then
			caster:StartGesture(ACT_DOTA_ALCHEMIST_CHEMICAL_RAGE_END)
		end
	end
end

function modifier_frostrose_chemical_rage:DeclareFunctions()
	local table = {
		MODIFIER_PROPERTY_DAMAGEOUTGOING_PERCENTAGE,
		MODIFIER_PROPERTY_SPELL_AMPLIFY_PERCENTAGE,
		MODIFIER_PROPERTY_MOVESPEED_BONUS_PERCENTAGE,
		MODIFIER_PROPERTY_TRANSLATE_ACTIVITY_MODIFIERS,
		MODIFIER_PROPERTY_TRANSLATE_ATTACK_SOUND,
	}
	return table
end

function modifier_frostrose_chemical_rage:GetActivityTranslationModifiers()
	return "chemical_rage"
end

-- find an alternate sound for every heroes?
function modifier_frostrose_chemical_rage:GetAttackSound()
	if self:GetParent():GetUnitName() == "npc_dota_hero_alchemist" then
		return "Hero_Alchemist.ChemicalRage.Attack"
	end
end

function modifier_frostrose_chemical_rage:GetModifierDamageOutgoing_Percentage()
	return self.damage_increase
end

function modifier_frostrose_chemical_rage:GetModifierSpellAmplify_Percentage()
	return self.damage_increase
end

function modifier_frostrose_chemical_rage:GetModifierMoveSpeedBonus_Percentage()
	return self.ms_increase
end

function modifier_frostrose_chemical_rage:GetModifierModelScale()
	if IsServer() then
		return self.size_increase
	end
end

function modifier_frostrose_chemical_rage:GetEffectName()
	return "particles/units/heroes/hero_alchemist/alchemist_chemical_rage.vpcf"
end

function modifier_frostrose_chemical_rage:GetEffectAttachType()
	return PATTACH_ABSORIGIN_FOLLOW
end

function modifier_frostrose_chemical_rage:GetStatusEffectName()
	return "particles/status_fx/status_effect_chemical_rage.vpcf"
end

function modifier_frostrose_chemical_rage:StatusEffectPriority()
	return 10
end

function modifier_frostrose_chemical_rage:GetHeroEffectName()
	return "particles/units/heroes/hero_alchemist/alchemist_chemical_rage_hero_effect.vpcf"
end

function modifier_frostrose_chemical_rage:HeroEffectPriority()
	return 10
end

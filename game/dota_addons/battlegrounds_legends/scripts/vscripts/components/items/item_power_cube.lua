-- Credits: 
-- Coder: EarthSalamander #42
-- Date: 04/04/2019

item_power_cube = class({})

function item_power_cube:GetBehavior()
	return DOTA_ABILITY_BEHAVIOR_IMMEDIATE
end

function item_power_cube:OnSpellStart()
	if IsServer() then
		if self:GetCaster():HasModifier("modifier_battle_royale_power_cube") then
			self:GetCaster():FindModifierByName("modifier_battle_royale_power_cube"):SetStackCount(self:GetCaster():FindModifierByName("modifier_battle_royale_power_cube"):GetStackCount() + 1)
		else
			self:GetCaster():AddNewModifier(self:GetCaster(), self, "modifier_battle_royale_power_cube", {}):SetStackCount(1)
		end

		self:GetCaster():CalculateStatBonus()
		self:GetCaster():Heal(self:GetSpecialValueFor("bonus_health"), self:GetCaster())
		self:SpendCharge()
	end
end

LinkLuaModifier("modifier_battle_royale_power_cube", "components/items/item_power_cube.lua", LUA_MODIFIER_MOTION_NONE)

modifier_battle_royale_power_cube = class({})

function modifier_battle_royale_power_cube:IsHidden() return false end
function modifier_battle_royale_power_cube:IsPurgable() return false end
function modifier_battle_royale_power_cube:RemoveOnDeath() return false end

function modifier_battle_royale_power_cube:GetTexture()
	return "power_rune"
end

function modifier_battle_royale_power_cube:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_EXTRA_HEALTH_BONUS,
		MODIFIER_PROPERTY_DAMAGEOUTGOING_PERCENTAGE, -- non-hero units
		MODIFIER_PROPERTY_SPELL_AMPLIFY_PERCENTAGE,
		MODIFIER_PROPERTY_MODEL_SCALE,
		MODIFIER_EVENT_ON_DEATH,
	}

	return funcs
end

function modifier_battle_royale_power_cube:OnCreated()
	self.health_increase = self:GetAbility():GetSpecialValueFor("bonus_health")
	self.damage_increase = self:GetAbility():GetSpecialValueFor("bonus_damage_pct")
	self.maximum_size = self:GetAbility():GetSpecialValueFor("maximum_size")

	if IsClient() then return end

	if self:GetParent():IsRealHero() then
		self.particle = ParticleManager:CreateParticle("particles/powerrune_overhead.vpcf", PATTACH_OVERHEAD_FOLLOW, self:GetParent())
		ParticleManager:SetParticleControlEnt(self.particle, 0, self:GetParent(), PATTACH_OVERHEAD_FOLLOW, "attach_hitloc", self:GetParent():GetAbsOrigin(), true)
		ParticleManager:SetParticleControl(self.particle, 1, Vector(0, 0, 0))
		ParticleManager:SetParticleControl(self.particle, 3, Vector(1, 0, 0))
	end

	self:StartIntervalThink(1.0)
end

function modifier_battle_royale_power_cube:OnIntervalThink()
	-- Not working feelsbadman
--	for _, unit in pairs(self:GetParent():GetAdditionalOwnedUnits()) do
--		print(unit:GetUnitName())
--	end

	if self:GetParent():GetUnitName() == "npc_dota_hero_lone_druid" then
		local units = FindUnitsInRadius(2, Vector(0, 0, 0), nil, FIND_UNITS_EVERYWHERE, DOTA_UNIT_TARGET_TEAM_BOTH, DOTA_UNIT_TARGET_ALL, DOTA_UNIT_TARGET_FLAG_INVULNERABLE + DOTA_UNIT_TARGET_FLAG_MAGIC_IMMUNE_ENEMIES, FIND_ANY_ORDER, false)

		for _, unit in pairs(units) do
			if unit:GetUnitName() == "npc_dota_lone_druid_bear" then
				if unit:HasModifier("modifier_battle_royale_power_cube") then
					if self:GetStackCount() ~= unit:FindModifierByName("modifier_battle_royale_power_cube"):GetStackCount() then
						unit:FindModifierByName("modifier_battle_royale_power_cube"):SetStackCount(self:GetStackCount())
					end
				else
					unit:AddNewModifier(unit, nil, "modifier_battle_royale_power_cube", {}):SetStackCount(self:GetStackCount())
				end
			end
		end
	elseif self:GetParent():GetUnitName() == "npc_dota_lone_druid_bear" then
		local hp_increase = self.health_increase * self:GetStackCount()

		-- Have to call this separately cause the health bonus modifier doesn't work on creeps?
		if self:GetParent():GetMaxHealth() ~= self:GetParent():GetBaseMaxHealth() + hp_increase then
			self:GetParent():SetMaxHealth(self:GetParent():GetBaseMaxHealth() + hp_increase)
			self:GetParent():Heal(hp_increase, self:GetParent())
		end
	end

	if self.particle then
		local new_stack = self:GetStackCount()

		if math.floor(self:GetStackCount() / 10) > 0 then
			new_stack = math.floor(self:GetStackCount() / 10) * 10 - self:GetStackCount()
			if math.floor(self:GetStackCount() / 10) >= 10 then
				ParticleManager:SetParticleControl(self.particle, 3, Vector(2, 0, 0))
			else
				ParticleManager:SetParticleControl(self.particle, 3, Vector(3, 0, 0))
			end
		else
			ParticleManager:SetParticleControl(self.particle, 3, Vector(1, 0, 0))
		end

		ParticleManager:SetParticleControl(self.particle, 1, Vector(0, new_stack, 0))
	end
end

function modifier_battle_royale_power_cube:GetModifierExtraHealthBonus()
	return self.health_increase * self:GetStackCount()
end

function modifier_battle_royale_power_cube:GetModifierDamageOutgoing_Percentage()
	return self.damage_increase * self:GetStackCount()
end

function modifier_battle_royale_power_cube:GetModifierSpellAmplify_Percentage()
	return self.damage_increase * self:GetStackCount()
end

function modifier_battle_royale_power_cube:OnDeath(keys)
	if not keys.unit:IsRealHero() then return end

	if keys.unit == self:GetParent() and not keys.unit:IsReincarnating() then
		ParticleManager:DestroyParticle(self.particle, false)

		for i = 1, self:GetStackCount() / 2 do
			SpawnItemEntity(self:GetParent():GetAbsOrigin(), false, "item_power_cube")
		end

		self:SetStackCount(0)
	end
end

function modifier_battle_royale_power_cube:GetModifierModelScale()
	if IsServer() then
		return math.min(self:GetStackCount() * 5, self.maximum_size)
	end
end

function modifier_battle_royale_power_cube:OnDestroy()
	if IsServer() then
		if self.particle then
			ParticleManager:DestroyParticle(self.particle, true)
		end
	end
end
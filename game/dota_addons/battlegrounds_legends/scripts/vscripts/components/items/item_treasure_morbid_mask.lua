-- Credits: EarthSalamander
-- Date (DD/MM/YYYY): 10/02/2019

LinkLuaModifier("modifier_morbid_mask_passive", "components/items/item_treasure_morbid_mask.lua", LUA_MODIFIER_MOTION_NONE)

item_treasure_morbid_mask = class({})

function item_treasure_morbid_mask:GetIntrinsicModifierName()
	return "modifier_morbid_mask_passive"
end

--------------------------------------------------------------

modifier_morbid_mask_passive = class({})

function modifier_morbid_mask_passive:GetTexture()
	return "modifiers/item_treasure_morbid_mask"
end

function modifier_morbid_mask_passive:IsHidden() return false end
function modifier_morbid_mask_passive:IsPurgable() return false end
function modifier_morbid_mask_passive:IsPurgeException() return false end
function modifier_morbid_mask_passive:IsDebuff() return false end

function modifier_morbid_mask_passive:DeclareFunctions()
	return {
		MODIFIER_EVENT_ON_TAKEDAMAGE,
	}
end

function modifier_morbid_mask_passive:OnTakeDamage(keys)
	if IsServer() then
		if keys.attacker and keys.unit then
			if self:GetParent() == keys.attacker then
				if keys.damage <= 0 then
					return
				end

				local heal = keys.damage * self:GetAbility():GetSpecialValueFor("lifesteal_percent") / 100
				SendOverheadEventMessage(nil, OVERHEAD_ALERT_HEAL, self:GetParent(), heal, nil)

				-- Heal and fire the particle
				self:GetParent():Heal(heal, self:GetParent())
				local lifesteal_pfx = ParticleManager:CreateParticle("particles/generic_gameplay/generic_lifesteal.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent())
				ParticleManager:SetParticleControl(lifesteal_pfx, 0, self:GetParent():GetAbsOrigin())
				ParticleManager:ReleaseParticleIndex(lifesteal_pfx)
			end
		end
	end
end

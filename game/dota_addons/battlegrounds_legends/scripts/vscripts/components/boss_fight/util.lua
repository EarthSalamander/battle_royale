function GemGrab:CheckEndGame()
	print("End game?", BEHEMOTH_SPAWN_COUNT, BEHEMOTH_MAX_SPAWN_COUNT)
	if BEHEMOTH_SPAWN_COUNT >= BEHEMOTH_MAX_SPAWN_COUNT then
		local tower_health = {}

		for i = 2, 3 do
			tower_health[i] = 0

			if FindTower(i) then
				tower_health[i] = FindTower(i):GetHealthPercent()
			end
		end

		if tower_health[2] > tower_health[3] then
			GameRules:SetGameWinner(2)
		elseif tower_health[3] > tower_health[2] then
			GameRules:SetGameWinner(3)
		else
			GameRules:SetGameWinner(4) -- draw
		end

		return
	end
end

if GemGrab == nil then
	GemGrab = class({})
end

LinkLuaModifier("modifier_boss_rune", "components/modifiers/modifier_boss_rune.lua", LUA_MODIFIER_MOTION_NONE)

require('components/overboss')
require('components/gem_grab/util')
require('components/boss_fight/util')

ListenToGameEvent('game_rules_state_change', function(keys)
	if GameRules:State_Get() == DOTA_GAMERULES_STATE_CUSTOM_GAME_SETUP then
		-- fountain setup
		local towers = Entities:FindAllByClassname("npc_dota_tower")

		for _, hTower in pairs(towers) do
			-- Attack range particle
			local danger_zone_pfx = ParticleManager:CreateParticle("particles/ambient/fountain_danger_circle.vpcf", PATTACH_CUSTOMORIGIN, nil)
			ParticleManager:SetParticleControl(danger_zone_pfx, 0, hTower:GetAbsOrigin())
			ParticleManager:ReleaseParticleIndex(danger_zone_pfx)
		end
	elseif GameRules:State_Get() == DOTA_GAMERULES_STATE_PRE_GAME then
		CustomNetTables:SetTableValue("game_score", "behemoth_count", {BEHEMOTH_SPAWN_COUNT})
		CustomNetTables:SetTableValue("game_score", "behemoth_max_count", {BEHEMOTH_MAX_SPAWN_COUNT})

		Timers:CreateTimer(1.0, function()
			local towers = Entities:FindAllByClassname("npc_dota_tower")

			for _, hTower in pairs(towers) do
				-- Attack range particle
				local danger_zone_pfx = ParticleManager:CreateParticle("particles/ambient/fountain_danger_circle.vpcf", PATTACH_CUSTOMORIGIN, nil)
				ParticleManager:SetParticleControl(danger_zone_pfx, 0, hTower:GetAbsOrigin())
				ParticleManager:ReleaseParticleIndex(danger_zone_pfx)

				ShowBossBar(hTower)
			end
		end)
	elseif GameRules:State_Get() == DOTA_GAMERULES_STATE_GAME_IN_PROGRESS then
		Timers:CreateTimer(function()
			if GameRules:State_Get() == DOTA_GAMERULES_STATE_POST_GAME then return nil end

			Overboss:SpawnItem()

			return BOSS_FIGHT_RUNE_SPAWN_DELAY
		end)

		-- OnFastThink
		Timers:CreateTimer(function()
			if GameRules:State_Get() == DOTA_GAMERULES_STATE_POST_GAME then return nil end

			for _, hero in pairs(HeroList:GetAllHeroes()) do
				if hero:IsRealHero() and hero:IsAlive() then
					for _, item in pairs(Entities:FindAllByClassname("dota_item_drop")) do
						if item:GetModelName() == "models/props_gameplay/rune_goldxp.vmdl" then
							if (item:GetAbsOrigin() - hero:GetAbsOrigin()):Length2D() <= 150 then
								if hero:HasModifier("modifier_boss_rune") then
									hero:FindModifierByName("modifier_boss_rune"):SetStackCount(hero:FindModifierByName("modifier_boss_rune"):GetStackCount() + 1)
								else
									hero:AddNewModifier(hero, nil, "modifier_boss_rune", {}):SetStackCount(1)
								end

								UTIL_Remove(item)
							end
						end
					end
				end
			end

			return 0.1
		end)

		-- OnThink
		Timers:CreateTimer(function()
			if GameRules:State_Get() == DOTA_GAMERULES_STATE_POST_GAME then return nil end

			if PREVENT_TIMER_RUNNING == false then
				CountdownTimer()

--				if nCOUNTDOWNTIMER == 30 then
--					CustomGameEventManager:Send_ServerToAllClients( "timer_alert", {} )
--				end
			end

			return 1.0
		end)
	elseif GameRules:State_Get() == DOTA_GAMERULES_STATE_POST_GAME then
		CustomGameEventManager:Send_ServerToAllClients("hide_boss_hp", {})
	end
end, nil)

ListenToGameEvent("dota_item_picked_up", function(keys)
	local item = EntIndexToHScript(keys.ItemEntityIndex)
	local owner = EntIndexToHScript(keys.HeroEntityIndex)

	if keys.itemname == "item_boss_rune" then
		if owner:HasModifier("modifier_boss_rune") then
			owner:FindModifierByName("modifier_boss_rune"):SetStackCount(owner:FindModifierByName("modifier_boss_rune"):GetStackCount() + 1)
		else
			owner:AddNewModifier(owner, nil, "modifier_boss_rune", {}):SetStackCount(1)
		end
	end
end, nil)

ListenToGameEvent('entity_hurt', function(keys)
	if keys.entindex_attacker == nil or keys.entindex_killed == nil then return end
	local damagebits = keys.damagebits -- This might always be 0 and therefore useless

	local entCause = EntIndexToHScript(keys.entindex_attacker)
	local entVictim = EntIndexToHScript(keys.entindex_killed)

	-- The ability/item used to damage, or nil if not damaged by an item/ability
	local damagingAbility = nil

	if keys.entindex_inflictor ~= nil then
		damagingAbility = EntIndexToHScript(keys.entindex_inflictor)
	end

	if entVictim:GetClassname() == "npc_dota_tower" then
		ShowBossBar(entVictim)
	end
end, nil)

ListenToGameEvent('entity_killed', function(keys)
--	print("Mutation: On Hero Dead")

	-- The Unit that was killed
	local killed_unit = EntIndexToHScript(keys.entindex_killed)
	if not killed_unit then return end

	if not killed_unit:IsRealHero() then
		if killed_unit:GetUnitName() == "npc_boss_behemoth" then
			GemGrab:CheckEndGame()

			PREVENT_TIMER_RUNNING = false
			SetTimer(BOSS_FIGHT_BASE_TIMER)
		end

		return
	end

	local hero = killed_unit

	-- The Killing entity
	local killer = nil

	if keys.entindex_attacker then
		killer = EntIndexToHScript(keys.entindex_attacker)
	end
end, nil)
if BattleRoyale == nil then
	BattleRoyale = class({})
end

LinkLuaModifier("modifier_battle_royale_power_cube_crate", "components/modifiers/modifier_battle_royale_power_cube_crate.lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_battle_royale_obstacle", "components/modifiers/modifier_battle_royale_obstacle.lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_battle_royale_treasure", "components/modifiers/modifier_battle_royale_treasure.lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_flying_vision", "components/modifiers/modifier_flying_vision.lua", LUA_MODIFIER_MOTION_NONE)

require('components/battle_royale/util')

BattleRoyale.CURRENT_ARENA_SIZE = BATTLE_ROYALE_ARENA_SIZE

ListenToGameEvent('game_rules_state_change', function(keys)
	if GameRules:State_Get() == DOTA_GAMERULES_STATE_PRE_GAME then
		GameRules:GetGameModeEntity():SetHUDVisible(1, false) -- Top Bar
		BattleRoyale:AddMapEntities()

		for i = 2, 13 do
			if PlayerResource:GetPlayerCountForTeam(i) > 0 then
				BattleRoyale:SetFogVision(BattleRoyale.CURRENT_ARENA_SIZE)
			end
		end

		Timers:CreateTimer(3.0, function()
			CustomGameEventManager:Send_ServerToAllClients("send_mutations", BATTLE_ROYALE_MUTATION)
		end)
	elseif GameRules:State_Get() == DOTA_GAMERULES_STATE_GAME_IN_PROGRESS then
--		BattleRoyale:GoldTick()
		BattleRoyale:ToxicFog()

		-- OnThink
		Timers:CreateTimer(function()
			if GameRules:State_Get() >= DOTA_GAMERULES_STATE_POST_GAME then
				return nil
			end

			if PREVENT_TIMER_RUNNING == false then
				CountdownTimer()

--				if nCOUNTDOWNTIMER == 30 then
--					CustomGameEventManager:Send_ServerToAllClients( "timer_alert", {} )
--				end
			end

			local all_units = FindUnitsInRadius(2, Vector(0, 0, 0), nil, FIND_UNITS_EVERYWHERE, DOTA_UNIT_TARGET_TEAM_BOTH, DOTA_UNIT_TARGET_ALL, DOTA_UNIT_TARGET_FLAG_NONE, FIND_ANY_ORDER, false)
			local units_in_arena = FindUnitsInRadius(2, Vector(0, 0, 0), nil, BattleRoyale.CURRENT_ARENA_SIZE, DOTA_UNIT_TARGET_TEAM_BOTH, DOTA_UNIT_TARGET_ALL, DOTA_UNIT_TARGET_FLAG_NONE, FIND_ANY_ORDER, false)

			for _, unit in pairs(all_units) do
				unit.storm_damage = true
			end

			for _, unit in pairs(units_in_arena) do
				unit.storm_damage = false
			end

			for _, unit in pairs(all_units) do
				if unit.storm_damage == true then
					-- Deal bullet damage
					ApplyDamage({
						attacker = GameMode.game_coordinator,
						victim = unit,
						ability = GameMode.game_coordinator:GetAbilityByIndex(0),
						damage = 1000,
						damage_type = DAMAGE_TYPE_PURE
					})

					SendOverheadEventMessage(nil, OVERHEAD_ALERT_DAMAGE, unit, 1000, nil)
				end
			end

			return 1.0
		end)
--[[
		-- OnFastThink
		Timers:CreateTimer(function()
			if GameRules:State_Get() == DOTA_GAMERULES_STATE_POST_GAME then return nil end

			for _, hero in pairs(HeroList:GetAllHeroes()) do
				if hero:IsRealHero() and hero:IsAlive() then
					for _, item in pairs(Entities:FindAllByClassname("dota_item_drop")) do
						if item:GetModelName() == "models/props_gameplay/rune_doubledamage01.vmdl" then
							if (item:GetAbsOrigin() - hero:GetAbsOrigin()):Length2D() <= 150 then
								if hero:HasModifier("modifier_battle_royale_power_cube") then
									hero:FindModifierByName("modifier_battle_royale_power_cube"):SetStackCount(hero:FindModifierByName("modifier_battle_royale_power_cube"):GetStackCount() + 1)
								else
									hero:AddNewModifier(hero, nil, "modifier_battle_royale_power_cube", {}):SetStackCount(1)
								end
							end
						end
					end
				end
			end

			return 0.1
		end)
--]]
	end
end, nil)

ListenToGameEvent('npc_spawned', function(event)
	local npc = EntIndexToHScript(event.entindex)

	if npc:IsRealHero() then
		if BATTLE_ROYALE_TEAMMATE_RESPAWN == true and npc:FindAlliedHero() then
			print(npc:GetUnitName().." respawned on: "..npc:FindAlliedHero():GetUnitName())
			FindClearSpaceForUnit(npc, npc:FindAlliedHero():GetAbsOrigin(), true)
		end
	end
end, nil)

ListenToGameEvent('entity_killed', function(keys)
	local killed_unit = EntIndexToHScript(keys.entindex_killed)

	local killer = nil

	if keys.entindex_attacker then
		killer = EntIndexToHScript(keys.entindex_attacker)
	end

	if killed_unit then
		if killed_unit:IsRealHero() then
--			if killer then
--				if killer:IsRealHero() then
--					ChangeCameraTarget(killed_unit:GetPlayerID(), killer, killer:GetPlayerID())
--				end
--			else
--				ChangeCameraTarget(killed_unit:GetPlayerID())
--			end

			for itemSlot = 0, 8 do
				local item = killed_unit:GetItemInSlot(itemSlot)
				if item then
					local newItem = CreateItem(item:GetName(), nil, nil)
					newItem:SetPurchaseTime(0)
					if newItem:IsPermanent() and newItem:GetShareability() == ITEM_FULLY_SHAREABLE then
						newItem:SetStacksWithOtherOwners( true )
					end
					local drop = CreateItemOnPositionForLaunch(killed_unit:GetAbsOrigin(), newItem)
					local dropRadius

					if bOverbossSource == true then
						dropRadius = RandomFloat(LOOT_SPAWN_RADIUS_MIN, LOOT_SPAWN_RADIUS_MAX)
					else
						dropRadius = RandomFloat(LOOT_SPAWN_RADIUS_MIN / 2, LOOT_SPAWN_RADIUS_MAX / 2)
					end

					newItem:LaunchLootInitialHeight(false, 0, 500, 0.75, killed_unit:GetAbsOrigin() + RandomVector(dropRadius))
					UTIL_Remove(item)
				end
			end

			-- place holder, make it dynamic later
			local alive_teams = 0
			local team_currently_checked = nil

			for i = 2, 13 do
				if PlayerResource:GetPlayerCountForTeam(i) > 0 then
					if IsTeamAlive(i) then
						alive_teams = alive_teams + 1
						team_currently_checked = i

						-- SetFixedRespawnTime is not working for reasons, this is temporary
						killed_unit:SetTimeUntilRespawn(BATTLE_ROYALE_RESPAWN_TIME)
					else
						BattleRoyale:SetHeroRespawnEnabled(i, false)
						BattleRoyale:SetSpectatorVision(i)
					end
				end
			end

			if team_currently_checked then
				if alive_teams <= 1 then
					BattleRoyale:WinRound(team_currently_checked)
				end
			end
		end
	end
end, nil)

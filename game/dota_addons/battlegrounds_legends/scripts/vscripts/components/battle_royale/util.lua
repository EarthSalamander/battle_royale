function BattleRoyale:TempestParticle(size, duration)
	self.tempest_particle = ParticleManager:CreateParticle("particles/econ/items/disruptor/disruptor_resistive_pinfold/disruptor_ecage_kineticfield.vpcf", PATTACH_WORLDORIGIN, nil)
	ParticleManager:SetParticleControl(self.tempest_particle, 0, Vector(0, 0, 0))
	ParticleManager:SetParticleControl(self.tempest_particle, 1, Vector(size, 1, 1))
	ParticleManager:SetParticleControl(self.tempest_particle, 2, Vector(duration,1,1))
--	self:AddParticle(self.tempest_particle, false, false, -1, false, false)
end

function BattleRoyale:SetFogVision(vision_radius, bSecondary)
	if BATTLE_ROYALE_FOG_REDUCE_WITH_ARENA_SIZE == false then return end

	for i = 2, 13 do
		if IsTeamAlive(i) and PlayerResource:GetPlayerCountForTeam(i) > 0 then
--			print("set dummy vision size ("..self.CURRENT_ARENA_SIZE..") on team: "..i)

			if not VISION_DUMMIES[i] then
				VISION_DUMMIES[i] = CreateUnitByName("npc_dummy_unit", Vector(0, 0, 0), true, nil, nil, i)
				VISION_DUMMIES[i]:AddNewModifier(VISION_DUMMIES[i], nil, "modifier_flying_vision", {})
				VISION_DUMMIES[i]:AddAbility("dummy_true_sight") -- not working?
			end

			VISION_DUMMIES[i]:SetDayTimeVisionRange(self.CURRENT_ARENA_SIZE)
			VISION_DUMMIES[i]:SetNightTimeVisionRange(self.CURRENT_ARENA_SIZE)

--			if self.CURRENT_ARENA_SIZE <= 4000 then
--				GameRules:GetGameModeEntity():SetFogOfWarDisabled(true)
--			end

--			if bSecondary then
--				GameRules:GetGameModeEntity():SetFogOfWarDisabled(false)
--			end
		end
	end
end

function BattleRoyale:AddMapEntities(bRespawn)
	if bRespawn then
		GameMode:CleanMap()
	end

	BattleRoyale:SpawnTreasure()

	for k, v in pairs(BATTLE_ROYALE_SPAWNERS) do
		local entities = Entities:FindAllByName(k)

		if k == "battle_royale_power_crate_spawner" then
			local entity_count = #entities * BATTLE_ROYALE_POWER_CUBE_COUNT_PCT / 100

			while #entities > entity_count do
				local random_int = RandomInt(1, #entities)

				if entities[random_int] then
					table.remove(entities, random_int)
				end
			end
		end

		for i, j in pairs(entities) do
			local unit = CreateUnitByName(v, j:GetAbsOrigin(), true, nil, nil, DOTA_TEAM_NEUTRALS)

			if unit:GetUnitName() == "npc_power_crate" then
				unit:AddNewModifier(unit, nil, "modifier_battle_royale_power_cube_crate", {})
			elseif string.find(unit:GetUnitName(), "obstacle") then
				unit:AddNewModifier(unit, nil, "modifier_battle_royale_obstacle", {})
			end
		end
	end
end

function BattleRoyale:ToxicFog()
	if self.tempest_particle then
		ParticleManager:DestroyParticle(self.tempest_particle, true)
		self.tempest_particle = nil
	end

	if self.CURRENT_ARENA_SIZE ~= BATTLE_ROYALE_ARENA_SIZE then
		self.CURRENT_ARENA_SIZE = BATTLE_ROYALE_ARENA_SIZE
	end

	-- For unknown reasons vision is smaller than expected on start, override it with max value
	BattleRoyale:SetFogVision(self.CURRENT_ARENA_SIZE)
	BattleRoyale:TempestParticle(self.CURRENT_ARENA_SIZE, BATTLE_ROYALE_FOG_SPAWN_DELAY + 1)

	SetTimer(BATTLE_ROYALE_FOG_SPAWN_DELAY)
end

function BattleRoyale:RestartRound(iWinningTeamNumber, bDraw)
	local team_name = {}
	team_name[2] = "#DOTA_GoodGuys"
	team_name[3] = "#DOTA_BadGuys"
	team_name[6] = "#DOTA_Custom1"
	team_name[7] = "#DOTA_Custom2"
	team_name[8] = "#DOTA_Custom3"
	team_name[9] = "#DOTA_Custom4"
	team_name[10] = "#DOTA_Custom5"
	team_name[11] = "#DOTA_Custom6"
	team_name[12] = "#DOTA_Custom7"
	team_name[13] = "#DOTA_Custom8"

	local text_content = "Draw!"

	if bDraw == false then
		if PlayerResource:GetPlayerCountForTeam(iWinningTeamNumber) == 1 then
			local player_name = ""

			for _, hero in pairs(HeroList:GetAllHeroes()) do
				if hero:GetTeamNumber() == iWinningTeamNumber then
					player_name = PlayerResource:GetPlayerName(hero:GetPlayerID())

					break
				end
			end

			Notifications:TopToAll({text=player_name, duration=BATTLE_ROYALE_DELAY_ROUND_WON})
		else
			Notifications:TopToAll({text=team_name[iWinningTeamNumber], duration=BATTLE_ROYALE_DELAY_ROUND_WON})
		end

		Notifications:TopToAll({text=" wins the round! Next round start in "..BATTLE_ROYALE_DELAY_ROUND_WON.." seconds...", duration=BATTLE_ROYALE_DELAY_ROUND_WON})
	else
		Notifications:TopToAll({text="Draw! Next round start in "..BATTLE_ROYALE_DELAY_ROUND_WON, duration=BATTLE_ROYALE_DELAY_ROUND_WON})
	end

	for _, hero in pairs(HeroList:GetAllHeroes()) do
		if hero:IsAlive() then
			hero:AddNewModifier(hero, nil, "modifier_invulnerable_hidden", {})
			hero.winner_particle = ParticleManager:CreateParticle("particles/leader/leader_overhead.vpcf", PATTACH_OVERHEAD_FOLLOW, hero)
			ParticleManager:SetParticleControlEnt(hero.winner_particle, PATTACH_OVERHEAD_FOLLOW, hero, PATTACH_OVERHEAD_FOLLOW, "follow_overhead", hero:GetAbsOrigin(), true)
		end
	end

	local modifiers_to_remove = {
		"modifier_battle_royale_power_cube",
		"modifier_invulnerable_hidden",
		"modifier_frostrose_chemical_rage",
	}

	Timers:CreateTimer(BATTLE_ROYALE_DELAY_ROUND_WON, function()
		for _, hero in pairs(HeroList:GetAllHeroes()) do
			if hero:IsAlive() then
				for _, modifier in pairs(modifiers_to_remove) do
					if hero:HasModifier(modifier) then
						hero:RemoveModifierByName(modifier)
					end
				end

				if hero.winner_particle then
					ParticleManager:DestroyParticle(hero.winner_particle, true)
					hero.winner_particle = nil
				end
			end

			for itemSlot = 0, 8 do
				local item = hero:GetItemInSlot(itemSlot)
				if item then
					UTIL_Remove(item)
				end
			end

			BattleRoyale:SpawnHeroes()
		end

		BattleRoyale:AddMapEntities(true)
		BattleRoyale:ToxicFog()

		Timers:CreateTimer(BATTLE_ROYALE_START_ROUND_DELAY, function()
			PREVENT_TIMER_RUNNING = false
		end)
	end)
end

function BattleRoyale:WinRound(iWinningTeamNumber)
	if PREVENT_TIMER_RUNNING == false then
		PREVENT_TIMER_RUNNING = true
	end

	BATTLE_ROYALE_TEAMMATE_RESPAWN = false

	if iWinningTeamNumber then
		GameMode:UpdateTeamScore(iWinningTeamNumber, 1)

		if GameMode:GetTeamScore(iWinningTeamNumber) >= BATTLE_ROYALE_ROUND_TO_WIN then
			GAME_WINNER_TEAM = iWinningTeamNumber
			GameRules:SetGameWinner(iWinningTeamNumber)
		else
			BattleRoyale:RestartRound(iWinningTeamNumber, false)
		end
	else
		BattleRoyale:RestartRound(iWinningTeamNumber, true)
	end

	for i = 2, 13 do
		if PlayerResource:GetPlayerCountForTeam(i) > 0 then
			BattleRoyale:SetHeroRespawnEnabled(i, true)
		end
	end
end

function BattleRoyale:SpawnTreasure()
	for _, entity in pairs(Entities:FindAllByName("battle_royale_treasure_spawner")) do
		local unit = CreateUnitByName("npc_treasure", entity:GetAbsOrigin(), true, nil, nil, DOTA_TEAM_NEUTRALS)
		unit:AddNewModifier(unit, nil, "modifier_battle_royale_treasure", {})
		unit:SetAngles(0, 270, 0)
	end
end

function BattleRoyale:OnTreasureOpen(hUnit)
	local item_name = BATTLE_ROYALE_TREASURES[RandomInt(1, #BATTLE_ROYALE_TREASURES)]
	local newItem = CreateItem(item_name, hUnit, hUnit)

	hUnit:AddItem(newItem)
--	EmitGlobalSound("Frostrose.Treasure")
	EmitGlobalSound("powerup_04")

	local overthrow_item_drop =
	{
		hero_id = hUnit:GetClassname(),
		dropped_item = item_name
	}
	CustomGameEventManager:Send_ServerToAllClients( "overthrow_item_drop", overthrow_item_drop )
end

function BattleRoyale:SetSpectatorVision(iTeamNumber)
	if PlayerResource:GetPlayerCountForTeam(iTeamNumber) > 0 then
		if VISION_DUMMIES[iTeamNumber] and VISION_DUMMIES[iTeamNumber]:GetDayTimeVisionRange() ~= 4000 then
--			print("set dummy vision to 4000 for team: "..iTeamNumber)
			VISION_DUMMIES[iTeamNumber]:SetDayTimeVisionRange(4000)
			VISION_DUMMIES[iTeamNumber]:SetNightTimeVisionRange(4000)
		end
	end
end

function BattleRoyale:GoldTick()
	Timers:CreateTimer(function()
		if GameRules:State_Get() >= DOTA_GAMERULES_STATE_POST_GAME then
			return nil
		end

		-- temporary
		for _, hero in pairs(HeroList:GetAllHeroes()) do
			if hero:IsAlive() then
				if hero:GetGold() <= 100 - 2 then
					hero:ModifyGold(2, true, 0)
				elseif hero:GetGold() <= 100 - 1 then
					hero:ModifyGold(1, true, 0)
				end
			end
		end

		return 1.0
	end)
end

function BattleRoyale:SpawnHeroes()
	local team_spawns = {}
	local spawn_points = Entities:FindAllByClassname("info_player_start_dota")

	for i = 2, 13 do
		if PlayerResource:GetPlayerCountForTeam(i) > 0 then
			local random_int = RandomInt(1, #spawn_points)
			local spawn_count = 1

			while team_spawns[i] == nil do
				local random_int = RandomInt(1, #spawn_points)

				if spawn_points[random_int] then
					team_spawns[i] = spawn_points[random_int]:GetAbsOrigin()
					table.remove(spawn_points, random_int)
				end
			end
		end
	end

	for _, hero in pairs(HeroList:GetAllHeroes()) do
		if not hero:IsAlive() then
			hero:RespawnHero(false, false)
		end

		FindClearSpaceForUnit(hero, team_spawns[hero:GetTeamNumber()], true)
		hero:AddNewModifier(hero, nil, "modifier_prevent_attack", {duration=BATTLE_ROYALE_START_ROUND_DELAY})
	end

	BATTLE_ROYALE_TEAMMATE_RESPAWN = true
end

function BattleRoyale:SetHeroRespawnEnabled(iTeamNumber, bCanRespawn)
	for _, hero in pairs(HeroList:GetAllHeroes()) do
		if hero:GetTeamNumber() == iTeamNumber then
			if hero:UnitCanRespawn() then
				hero:SetUnitCanRespawn(bCanRespawn)
				hero:SetTimeUntilRespawn(-1)

				Timers:CreateTimer(1.0, function()
					hero:SetTimeUntilRespawn(-1)
				end)
			end
		end
	end
end

modifier_gem_grab = class({})

function modifier_gem_grab:GetTexture()
	return "item_gem"
end

function modifier_gem_grab:OnCreated()
	if IsServer() then
		if not self.particle then 
			self.particle = ParticleManager:CreateParticle("particles/hw_fx/candy_carrying_stack.vpcf", PATTACH_OVERHEAD_FOLLOW, self:GetParent())
			ParticleManager:SetParticleControl(self.particle, 0, self:GetParent():GetAbsOrigin())
			ParticleManager:SetParticleControl(self.particle, 2, Vector(0, 0, 0))
			ParticleManager:SetParticleControl(self.particle, 3, Vector(0, 0, 0))
		end

		self:StartIntervalThink(0.1)
	end
end

function modifier_gem_grab:OnIntervalThink()
	local stack_10 = math.floor(self:GetStackCount() / 10)

	if stack_10 > 0 then
		local new_stack = stack_10 * 10 - self:GetStackCount()
		ParticleManager:SetParticleControl(self.particle, 2, Vector(new_stack, stack_10, 0))
		ParticleManager:SetParticleControl(self.particle, 3, Vector(1, 0, 0))
	else
		ParticleManager:SetParticleControl(self.particle, 2, Vector(0, self:GetStackCount(), 0))
		ParticleManager:SetParticleControl(self.particle, 3, Vector(0, 0, 0))
	end
end

function modifier_gem_grab:OnDestroy()
	if IsServer() then
		ParticleManager:DestroyParticle(self.particle, false)
		ParticleManager:ReleaseParticleIndex(self.particle)
		GemGrab:SetScore(self:GetParent(), -self:GetStackCount())
	end
end

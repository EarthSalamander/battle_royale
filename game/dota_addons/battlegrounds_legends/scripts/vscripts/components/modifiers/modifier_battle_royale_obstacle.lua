-- Credits: 
-- Coder: EarthSalamander #42

modifier_battle_royale_obstacle = class({})

function modifier_battle_royale_obstacle:IsHidden() return true end
function modifier_battle_royale_obstacle:IsPurgable() return false end
function modifier_battle_royale_obstacle:IsPurgeException() return false end

function modifier_battle_royale_obstacle:CheckState()
	local states = 
	{
		[MODIFIER_STATE_NO_HEALTH_BAR] = true,
	}

	return states
end

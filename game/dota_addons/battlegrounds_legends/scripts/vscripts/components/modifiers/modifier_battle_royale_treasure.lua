modifier_battle_royale_treasure = class({})

--------------------------------------------------------------------------------

function modifier_battle_royale_treasure:IsHidden()
	return true
end

--------------------------------------------------------------------------------

function modifier_battle_royale_treasure:OnCreated( kv )
	if IsServer() then
		self:StartIntervalThink( 0.5 )

		self.ambient_pfx = ParticleManager:CreateParticle("particles/econ/events/ti7/golden_treasure_ti7_ambient.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent())
	end
end

--------------------------------------------------------------------------------

function modifier_battle_royale_treasure:DeclareFunctions()
	local funcs = {
		MODIFIER_EVENT_ON_ORDER,
		MODIFIER_EVENT_ON_TAKEDAMAGE,
		MODIFIER_PROPERTY_FIXED_DAY_VISION,
		MODIFIER_PROPERTY_FIXED_NIGHT_VISION,
	}
	return funcs
end

-----------------------------------------------------------------------

function modifier_battle_royale_treasure:OnOrder( params )
	if IsServer() then
		local hOrderedUnit = params.unit 
		local hTargetUnit = params.target
		local nOrderType = params.order_type
--		print(nOrderType, DOTA_UNIT_ORDER_MOVE_TO_TARGET)
--		if nOrderType ~= DOTA_UNIT_ORDER_MOVE_TO_TARGET then
--			return
--		end

		if hTargetUnit == nil or hTargetUnit ~= self:GetParent() then
			return
		end

		if hOrderedUnit ~= nil and hOrderedUnit:IsRealHero() then
			self.hPlayerEnt = hOrderedUnit
			self:StartIntervalThink( 0.25 )
			return
		end

		self:StartIntervalThink( -1 )
	end

	return 0
end

-----------------------------------------------------------------------

function modifier_battle_royale_treasure:OnTakeDamage( params )
	return 0
end

-----------------------------------------------------------------------

function modifier_battle_royale_treasure:OnIntervalThink()
	if IsServer() then
		if not self.bWasOpened then
			if self.hPlayerEnt ~= nil then
				local flOpenDistance = 150.0
				if flOpenDistance >= (self.hPlayerEnt:GetOrigin() - self:GetParent():GetOrigin()):Length2D() then
					self.hPlayerEnt:Interrupt()
					self:GetParent():StartGesture(ACT_DOTA_PRESENT_ITEM)
					BattleRoyale:OnTreasureOpen(self.hPlayerEnt)
					self.bWasOpened = true
					self.hPlayerEnt = nil
					self:StartIntervalThink( -1 )
					ParticleManager:DestroyParticle(self.ambient_pfx, false)
					ParticleManager:ReleaseParticleIndex(self.ambient_pfx)

					local open_pfx = ParticleManager:CreateParticle("particles/ui/ui_generic_treasure_impact.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent())
					ParticleManager:ReleaseParticleIndex(open_pfx)

					Timers:CreateTimer(2.0, function()
						UTIL_Remove(self:GetParent())
					end)

					return -1
				end
			end
		end
	end
end

--------------------------------------------------------------------------------

function modifier_battle_royale_treasure:GetFixedDayVision( params )
	return 1
end

--------------------------------------------------------------------------------

function modifier_battle_royale_treasure:GetFixedNightVision( params )
	return 1
end

--------------------------------------------------------------------------------

function modifier_battle_royale_treasure:CheckState()
	local state = {}
	if IsServer()  then
		state[MODIFIER_STATE_ATTACK_IMMUNE] = true
		state[MODIFIER_STATE_ROOTED] = true
		state[MODIFIER_STATE_NO_HEALTH_BAR] = true
		state[MODIFIER_STATE_BLIND] = true
		state[MODIFIER_STATE_NOT_ON_MINIMAP] = true
		state[MODIFIER_STATE_INVULNERABLE] = true
		
		if self.bWasOpened then
			state[MODIFIER_STATE_UNSELECTABLE] = true
			state[MODIFIER_STATE_NO_UNIT_COLLISION] = true
		end
	end
	
	return state
end

--------------------------------------------------------------------------------

-- Author:	Cookies (EarthSalamander)
-- Date:	05.11.2017

if modifier_behemoth_ai == nil then modifier_behemoth_ai = class({}) end
function modifier_behemoth_ai:IsPurgeException() return false end
function modifier_behemoth_ai:IsPurgable() return false end
function modifier_behemoth_ai:IsDebuff() return false end
function modifier_behemoth_ai:IsHidden() return true end

--[[
function modifier_behemoth_ai:CheckState()
	local state = {}

--	state[MODIFIER_STATE_NO_UNIT_COLLISION]	= true
	return state
end
--]]

function modifier_behemoth_ai:OnCreated()
	if IsServer() then
		self.last_movement = 0.0
		self.find_enemy_distance = 300

		self:StartIntervalThink(0.1)
	end
end

function modifier_behemoth_ai:OnIntervalThink()
if not IsServer() or self:GetParent():IsIllusion() or FindTower(self:GetParent():GetOpposingTeamNumber()) == nil then return end

	if self:GetParent():IsStunned() or self:GetParent():IsHexed() then return end

	print("Behemoth attacking?", self:GetParent():IsAttacking())
	if not self:GetParent():IsAttacking() then
		local tower = FindTower(self:GetParent():GetOpposingTeamNumber())
		local distance = (self:GetParent():GetAbsOrigin() - tower:GetAbsOrigin()):Length2D()

		if distance < self.find_enemy_distance then
			print("Set attacking tower")
			self:GetParent():SetAttacking(tower)

			return
		else
			local enemies = FindUnitsInRadius(self:GetParent():GetTeamNumber(), self:GetParent():GetAbsOrigin(), nil, self.find_enemy_distance, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, DOTA_UNIT_TARGET_FLAG_NONE, FIND_CLOSEST, false)

			if #enemies > 0 then
				print("Set attacking closest enemy")
				self:GetParent():SetAttacking(enemies[0])
			else
				print("Move to tower")
				self:GetParent():MoveToPosition(tower:GetAbsOrigin())
			end
		end
	end
end

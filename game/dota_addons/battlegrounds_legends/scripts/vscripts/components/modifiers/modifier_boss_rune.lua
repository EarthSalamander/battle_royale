modifier_boss_rune = class({})

function modifier_boss_rune:GetTexture()
	return "bounty_rune"
end

function modifier_boss_rune:OnCreated()
	if IsServer() then
		if not self.particle then 
			self.particle = ParticleManager:CreateParticle("particles/hw_fx/candy_carrying_stack.vpcf", PATTACH_OVERHEAD_FOLLOW, self:GetParent())
			ParticleManager:SetParticleControl(self.particle, 0, self:GetParent():GetAbsOrigin())
			ParticleManager:SetParticleControl(self.particle, 2, Vector(0, 0, 0))
			ParticleManager:SetParticleControl(self.particle, 3, Vector(0, 0, 0))
		end

		self.delay = 2.0
		self.timer = 0.0
		self.interval = 0.1

		self:StartIntervalThink(self.interval)
	end
end

function modifier_boss_rune:OnIntervalThink()
	local stack_10 = math.floor(self:GetStackCount() / 10)

	if stack_10 > 0 then
		local new_stack = stack_10 * 10 - self:GetStackCount()
		ParticleManager:SetParticleControl(self.particle, 2, Vector(new_stack, stack_10, 0))
		ParticleManager:SetParticleControl(self.particle, 3, Vector(1, 0, 0))
	else
		ParticleManager:SetParticleControl(self.particle, 2, Vector(0, self:GetStackCount(), 0))
		ParticleManager:SetParticleControl(self.particle, 3, Vector(0, 0, 0))
	end

	self.timer = self.timer + self.interval

	if self.timer >= self.delay then
		print("Add 1 score!")
		self.timer = 0.0
		GemGrab:SetScore(self:GetParent(), 1)

		if self:GetStackCount() >= 1 then
			self:SetStackCount(self:GetStackCount() - 1)
		end

		if self:GetStackCount() == 0 then
			self:GetParent():RemoveModifierByName("modifier_boss_rune")
		end
	end
end

function modifier_boss_rune:OnRemoved()
	if IsServer() then
		ParticleManager:DestroyParticle(self.particle, false)
		ParticleManager:ReleaseParticleIndex(self.particle)

		if self:GetStackCount() > 0 then
			GemGrab:SetScore(self:GetParent(), -self:GetStackCount(), nil, false)
		end
	end
end

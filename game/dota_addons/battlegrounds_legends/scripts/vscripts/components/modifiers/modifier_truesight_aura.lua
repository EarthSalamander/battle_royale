modifier_truesight_aura = class({})
function modifier_truesight_aura:IsAura()
    return true
end

function modifier_truesight_aura:IsHidden()
    return true
end

function modifier_truesight_aura:IsPurgable()
    return false
end

function modifier_truesight_aura:GetAuraRadius()
    return 20000
end

function modifier_truesight_aura:GetModifierAura()
    return "modifier_truesight"
end
   
function modifier_truesight_aura:GetAuraSearchTeam()
    return DOTA_UNIT_TARGET_TEAM_ENEMY
end

function modifier_truesight_aura:GetAuraSearchFlags()
    return DOTA_UNIT_TARGET_FLAG_MAGIC_IMMUNE_ENEMIES
end

function modifier_truesight_aura:GetAuraSearchType()
    return DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC + DOTA_UNIT_TARGET_OTHER
end

function modifier_truesight_aura:GetAuraDuration()
    return 0.5
end
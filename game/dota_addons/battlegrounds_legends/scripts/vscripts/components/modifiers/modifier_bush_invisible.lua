modifier_bush_invisible = class({})

function modifier_bush_invisible:IsHidden() return true end
function modifier_bush_invisible:IsDebuff() return false end
function modifier_bush_invisible:IsPurgable() return false end
function modifier_bush_invisible:IsPurgeException() return false end

function modifier_bush_invisible:GetAttributes()
	return MODIFIER_ATTRIBUTE_MULTIPLE
end

function modifier_bush_invisible:CheckState()
	if self:GetStackCount() == 0 then
		return {
			[MODIFIER_STATE_INVISIBLE] = false
		}
	end

	return {
		[MODIFIER_STATE_INVISIBLE] = true
	}
end

function modifier_bush_invisible:DeclareFunctions()
	return {
		MODIFIER_EVENT_ON_ABILITY_FULLY_CAST,
		MODIFIER_EVENT_ON_TAKEDAMAGE,
		MODIFIER_PROPERTY_INVISIBILITY_LEVEL,
	}
end

function modifier_bush_invisible:GetTexture()
	return "item_invis_sword"
end

function modifier_bush_invisible:OnCreated()
	if IsServer() then
		self:SetStackCount(1)
		self:StartIntervalThink(0.1)
	end
end

function modifier_bush_invisible:OnIntervalThink()
	local heroes = FindUnitsInRadius(self:GetParent():GetTeamNumber(), self:GetParent():GetAbsOrigin(), nil, 300, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, DOTA_UNIT_TARGET_FLAG_MAGIC_IMMUNE_ENEMIES + DOTA_UNIT_TARGET_FLAG_INVULNERABLE, FIND_ANY_ORDER, false)

	if #heroes > 0 then
		if self:GetStackCount() == 1 then
			for _, hero in pairs(heroes) do
				if hero:GetTeamNumber() ~= DOTA_TEAM_NEUTRALS then
					self:SetStackCount(0)

					break
				end
			end
		end
	else
		if self:GetStackCount() == 0 then
			self:SetStackCount(1)
		end
	end
end

function modifier_bush_invisible:GetModifierInvisibilityLevel()
	return self:GetStackCount()
end

function modifier_bush_invisible:OnTakeDamage(keys)
	if IsClient() then return end

	if keys.unit == self:GetCaster() then
		self:StartIntervalThink(-1)
		self:SetStackCount(0)

		Timers:CreateTimer(0.3, function()
			if self then
				self:SetStackCount(1)
				self:StartIntervalThink(0.1)
			end
		end)

		-- not working, don't know why
--		keys.unit:MakeVisibleToTeam(keys.attacker:GetTeamNumber(), 0.3)
	end
end

function modifier_bush_invisible:OnAbilityFullyCast(keys)
	if IsClient() then return end

	if keys.unit == self:GetCaster() then
		self:StartIntervalThink(-1)
		self:SetStackCount(0)

		Timers:CreateTimer(0.3, function()
			if self then
				self:SetStackCount(1)
				self:StartIntervalThink(0.1)
			end
		end)
	end
end

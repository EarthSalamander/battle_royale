
modifier_battle_royale_power_cube_crate = class({})

--------------------------------------------------------------------------------

function modifier_battle_royale_power_cube_crate:IsHidden()
	return true
end

--------------------------------------------------------------------------------

function modifier_battle_royale_power_cube_crate:OnCreated( kv )
	if IsServer() then
		self:GetParent():SetCustomHealthLabel(tostring(self:GetParent():GetMaxHealth()), 250, 50, 50)
	end
end

--------------------------------------------------------------------------------

function modifier_battle_royale_power_cube_crate:CheckState()
	local state = {}

	if IsServer() then
		state[MODIFIER_STATE_ROOTED] = true
		state[MODIFIER_STATE_BLIND] = true
--		state[MODIFIER_STATE_NOT_ON_MINIMAP] = true
	end

	return state
end

--------------------------------------------------------------------------------

function modifier_battle_royale_power_cube_crate:DeclareFunctions()
	local funcs = {
		MODIFIER_EVENT_ON_DEATH,
		MODIFIER_PROPERTY_PROVIDES_FOW_POSITION,
		MODIFIER_EVENT_ON_TAKEDAMAGE,
	}

	return funcs
end

--------------------------------------------------------------------------------

function modifier_battle_royale_power_cube_crate:GetModifierProvidesFOWVision( params )
	return 1
end

--------------------------------------------------------------------------------

function modifier_battle_royale_power_cube_crate:OnTakeDamage(keys)
	local attacker = keys.attacker
	local target = keys.unit
	local damage = keys.damage
	local damage_type = keys.damage_type

	-- Only apply if the parent of this buff is attacked by an enemy
--	print(target == self:GetParent(), self:GetParent():GetTeamNumber(), attacker:GetTeamNumber())
	if target == self:GetParent() then
		if damage > 0 then
--			print("Set health to:", self:GetParent():GetHealth(), damage)
			self:GetParent():SetCustomHealthLabel(tostring(self:GetParent():GetHealth()), 250, 50, 50)
		end
	end
end

-----------------------------------------------------------------------

function modifier_battle_royale_power_cube_crate:OnDeath( params )
	if IsServer() then
		if params.unit == self:GetParent() then
			--print( string.format( "breakable container \"%s\" destroyed by \"%s\"", self:GetParent():GetUnitName() or "Unknown Attacker", self.hAttacker:GetUnitName() ) )
			if RandomInt( 0, 1 ) >= 1 then
				EmitSoundOn("Dungeon.SmashCrateShort", self:GetParent())
			else
				EmitSoundOn("Dungeon.SmashCrateLong", self:GetParent())
			end

			SpawnItemEntity(self:GetParent():GetAbsOrigin(), false, "item_power_cube")
		end
	end
end

-----------------------------------------------------------------------

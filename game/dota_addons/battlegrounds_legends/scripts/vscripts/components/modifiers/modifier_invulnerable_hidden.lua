modifier_invulnerable_hidden = class({})

function modifier_invulnerable_hidden:CheckState()
	return {
		[MODIFIER_STATE_INVULNERABLE] = true,
	}
end

function modifier_invulnerable_hidden:GetEffectName()
	return "particles/test_particle/damage_immunity.vpcf"
end

function modifier_invulnerable_hidden:GetEffectAttachType()
	return PATTACH_ABSORIGIN_FOLLOW
end

if Overboss == nil then
	Overboss = class({})
end

function Overboss:ThrowCoin( args )
--	print( "ThrowCoin" )
	local coinAttach = args.caster:ScriptLookupAttachment( "coin_toss_point" )
	local coinSpawn = Vector( 0, 0, 0 )
	if coinAttach ~= -1 then
		coinSpawn = args.caster:GetAttachmentOrigin( coinAttach )
	end
--	print( coinSpawn )
	SpawnItemEntity(coinSpawn, true)
end

function Overboss:SpawnItem(vPos)
	if vPos then
		SpawnItemEntity(vPos, false)
		return
	end

	if PREVENT_TIMER_RUNNING == true then
		return
	end

	local overBoss = Entities:FindByName( nil, "@overboss" )
	local throwCoin = nil
	local throwCoin2 = nil

	if overBoss then
		throwCoin = overBoss:FindAbilityByName( 'dota_ability_throw_coin' )
		throwCoin2 = overBoss:FindAbilityByName( 'dota_ability_throw_coin_long' )
	end

	-- sometimes play the long anim
	if throwCoin2 and RandomInt( 1, 100 ) > 80 then
		overBoss:CastAbilityNoTarget( throwCoin2, -1 )
	elseif throwCoin then
		overBoss:CastAbilityNoTarget( throwCoin, -1 )
	else
		SpawnItemEntity(Vector( 0, 0, 0 ), true)
	end
end

if CandyGrab == nil then
	CandyGrab = class({})
end

LinkLuaModifier("modifier_gem_grab", "components/modifiers/modifier_gem_grab.lua", LUA_MODIFIER_MOTION_NONE)

require('components/overboss')
require('components/gem_grab/util')

ListenToGameEvent('game_rules_state_change', function(keys)
if GameRules:State_Get() == DOTA_GAMERULES_STATE_GAME_IN_PROGRESS then
		Timers:CreateTimer(function()
			if GameRules:State_Get() == DOTA_GAMERULES_STATE_POST_GAME then return nil end

			Overboss:SpawnItem()

			return CANDY_GRAB_GEM_SPAWN_DELAY
		end)

		-- OnThink
		Timers:CreateTimer(function()
			if GameRules:State_Get() == DOTA_GAMERULES_STATE_POST_GAME then return nil end

			return 0.1
		end)
	end
end, nil)

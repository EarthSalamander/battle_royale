local score_timer = nil
function GemGrab:SetScore(hOwner, iAmount, hItem, bCreateItem)
	GameMode:UpdateRoundScore(hOwner:GetTeamNumber(), iAmount)

	if iAmount < 0 then
		SendOverheadEventMessage(hOwner, OVERHEAD_ALERT_MANA_LOSS, hOwner, iAmount, nil)
	else
		SendOverheadEventMessage(hOwner, OVERHEAD_ALERT_MANA_ADD, hOwner, iAmount, nil)		
	end

	if hItem then
		UTIL_Remove(hItem) -- otherwise it pollutes the player inventory
	end

	if GetMapName() ~= "gem_grab" then return end

	if GameMode:GetRoundScore(hOwner:GetTeamNumber()) >= GEM_GRAB_SCORE_TO_WIN and GameMode:GetRoundScore(hOwner:GetTeamNumber()) ~= GameMode:GetRoundScore(hOwner:GetOpposingTeamNumber()) then
		if GEM_GRAB_WINNING_PHASE == true then return end

		local team_color = {}
		team_color[2] = "Green"
		team_color[3] = "Red"

		Notifications:TopToAll({text=team_color[hOwner:GetTeamNumber()].." team", duration=5.0, style = {color=team_color[hOwner:GetTeamNumber()]}})

		GEM_GRAB_WINNING_PHASE = true

		local countdown = GEM_GRAB_DELAY_TO_WIN
		score_timer = Timers:CreateTimer(function()
			Notifications:TopToAll({text="COUNTDOWN: "..countdown, duration=1.0})
			countdown = countdown - 1

			if countdown == 0 then
				GameMode:UpdateTeamScore(hOwner:GetTeamNumber(), iAmount)

				if GameMode:GetTeamScore(hOwner:GetTeamNumber()) >= GEM_GRAB_TEAM_SCORE_TO_WIN then
					GameRules:SetGameWinner(hOwner:GetTeamNumber())
				else
					GemGrab:RestartRound(hOwner:GetTeamNumber())
				end

				return nil
			end

			return 1.0
		end)
	else
		if GEM_GRAB_WINNING_PHASE == true then
			GEM_GRAB_WINNING_PHASE = false

			if score_timer then
				Timers:RemoveTimer(score_timer)
				score_timer = nil
			end
		end

		if bCreateItem == false then return end

		if iAmount < 0 then
			for i = 1, (iAmount * -1) do
				Overboss:SpawnItem(hOwner:GetAbsOrigin())
			end
		end
	end
end

function GemGrab:RestartRound(iWinningTeamNumber)
	local team_name = {}
	team_name[2] = "#DOTA_GoodGuys"
	team_name[3] = "#DOTA_BadGuys"

	Notifications:TopToAll({text=team_name[iWinningTeamNumber], duration=GEM_GRAB_DELAY_ROUND_WON})
	Notifications:TopToAll({text=" wins the round! Next round start in "..GEM_GRAB_DELAY_ROUND_WON.." seconds...", duration=GEM_GRAB_DELAY_ROUND_WON})

	for _, hero in pairs(HeroList:GetAllHeroes()) do
		if hero:IsAlive() then
			hero:AddNewModifier(hero, nil, "modifier_invulnerable_hidden", {})
			hero.winner_particle = ParticleManager:CreateParticle("particles/leader/leader_overhead.vpcf", PATTACH_OVERHEAD_FOLLOW, hero)
			ParticleManager:SetParticleControlEnt(hero.winner_particle, PATTACH_OVERHEAD_FOLLOW, hero, PATTACH_OVERHEAD_FOLLOW, "follow_overhead", hero:GetAbsOrigin(), true)
		end
	end

	Timers:CreateTimer(GEM_GRAB_DELAY_ROUND_WON, function()
		for _, hero in pairs(HeroList:GetAllHeroes()) do
			if hero:IsAlive() then
				if hero:HasModifier("modifier_gem_grab") then
					hero:RemoveModifierByName("modifier_gem_grab")
				end

				if hero:HasModifier("modifier_invulnerable_hidden") then
					hero:RemoveModifierByName("modifier_invulnerable_hidden")
				end

				if hero.winner_particle then
					ParticleManager:DestroyParticle(hero.winner_particle, true)
					hero.winner_particle = nil
				end
			end

			for itemSlot = 0, 8 do
				local item = hero:GetItemInSlot(itemSlot)
				if item then
					UTIL_Remove(item)
				end
			end

			hero:RespawnHero(false, false)
			hero:AddNewModifier(hero, nil, "modifier_prevent_attack", {duration=GEM_GRAB_START_ROUND_DELAY})
		end

		GameMode:CleanMap()

		Timers:CreateTimer(GEM_GRAB_START_ROUND_DELAY, function()
			PREVENT_TIMER_RUNNING = false
		end)
	end)
end
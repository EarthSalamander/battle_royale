if GemGrab == nil then
	GemGrab = class({})
end

LinkLuaModifier("modifier_gem_grab", "components/modifiers/modifier_gem_grab.lua", LUA_MODIFIER_MOTION_NONE)

require('components/overboss')
require('components/gem_grab/util')

ListenToGameEvent('game_rules_state_change', function(keys)
if GameRules:State_Get() == DOTA_GAMERULES_STATE_GAME_IN_PROGRESS then
		Timers:CreateTimer(function()
			if GameRules:State_Get() == DOTA_GAMERULES_STATE_POST_GAME then return nil end

			Overboss:SpawnItem()

			return GEM_GRAB_GEM_SPAWN_DELAY
		end)

		-- OnThink
		Timers:CreateTimer(function()
			if GameRules:State_Get() == DOTA_GAMERULES_STATE_POST_GAME then return nil end

			for _, hero in pairs(HeroList:GetAllHeroes()) do
				if hero:IsRealHero() and hero:IsAlive() then
					for _, item in pairs(Entities:FindAllByClassname("dota_item_drop")) do
						if item:GetModelName() == "models/props_gameplay/gem01.vmdl" then
							if (item:GetAbsOrigin() - hero:GetAbsOrigin()):Length2D() <= 150 then
								if hero:HasModifier("modifier_gem_grab") then
									hero:FindModifierByName("modifier_gem_grab"):SetStackCount(hero:FindModifierByName("modifier_gem_grab"):GetStackCount() + 1)
								else
									hero:AddNewModifier(hero, nil, "modifier_gem_grab", {}):SetStackCount(1)
								end

								GemGrab:SetScore(hero, 1, item)
							end
						end
					end
				end
			end

			return 0.1
		end)
	end
end, nil)

ListenToGameEvent("dota_item_picked_up", function(keys)
	local item = EntIndexToHScript(keys.ItemEntityIndex)
	local owner = EntIndexToHScript(keys.HeroEntityIndex)

	if keys.itemname == "item_gem_grab" then
		if owner:HasModifier("modifier_gem_grab") then
			owner:FindModifierByName("modifier_gem_grab"):SetStackCount(owner:FindModifierByName("modifier_gem_grab"):GetStackCount() + 1)
		else
			owner:AddNewModifier(owner, nil, "modifier_gem_grab", {}):SetStackCount(1)
		end

		GemGrab:SetScore(owner, 1, item)
	end
end, nil)

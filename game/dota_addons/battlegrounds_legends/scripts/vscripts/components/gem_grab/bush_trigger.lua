function EnterBush(keys)
	if keys.activator:GetUnitName() ~= "npc_treasure" then
		keys.activator:AddNewModifier(keys.activator, nil, "modifier_bush_invisible", {})
	end
end

function ExitBush(keys)
	if keys.activator:HasModifier("modifier_bush_invisible") then
		keys.activator:RemoveModifierByName("modifier_bush_invisible")
	end
end

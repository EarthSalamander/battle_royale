lone_druid_shockwave = lone_druid_shockwave or class({})

function lone_druid_shockwave:OnSpellStart()
	if IsServer() then
		local direction = (self:GetCursorPosition() - self:GetCaster():GetAbsOrigin()):Normalized()

		self:GetCaster():EmitSound("Hero_Magnataur.ShockWave.Particle.Anvil")

		local projectile = {
			Ability				= self,
			EffectName			= "particles/econ/items/magnataur/shock_of_the_anvil/magnataur_shockanvil.vpcf",
			vSpawnOrigin		= self:GetCaster():GetAbsOrigin() + direction * 50 + Vector(0, 0, 50),
			fDistance			= self:GetCastRange(self:GetCaster():GetAbsOrigin(), self:GetCaster()),
			fStartRadius		= self:GetSpecialValueFor("radius"),
			fEndRadius			= self:GetSpecialValueFor("radius"),
			Source				= self:GetCaster(),
			bHasFrontalCone		= false,
			bReplaceExisting	= false,
			iUnitTargetTeam		= DOTA_UNIT_TARGET_TEAM_ENEMY,
--			iUnitTargetFlags	= DOTA_UNIT_TARGET_FLAG_MAGIC_IMMUNE_ENEMIES,
			iUnitTargetType		= DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC,
--			fExpireTime			= ,
			bDeleteOnHit		= true,
			vVelocity			= direction * self:GetSpecialValueFor("speed"),
			bProvidesVision		= true,
--			iVisionRadius		= projectile_vision,
			iVisionTeamNumber	= self:GetCaster():GetTeamNumber(),
			UnitTest = function( params, unit )
				if unit:GetTeamNumber() ~= params.Source:GetTeamNumber() then
					return true
				end
			end,
			OnUnitHit = function( params, unit )
				local damageTable = {
					victim = unit,
					attacker = params.Source,
					damage = params.Ability:GetSpecialValueFor("damage"),
					damage_type = DAMAGE_TYPE_PHYSICAL,
					ability = params.Ability
				}

				ApplyDamage(damageTable)
				unit:EmitSound("Hero_Magnataur.ShockWave.Target")
			end,
		}

		Projectiles:CreateProjectile(projectile)
	end
end

LinkLuaModifier("modifier_summon_bear_ai", "components/abilities/heroes/lone_druid.lua", LUA_MODIFIER_MOTION_NONE)

lone_druid_summon_bear = lone_druid_summon_bear or class({})

function lone_druid_summon_bear:OnSpellStart()
	if IsServer() then
		self.dummy_unit = CreateUnitByName("npc_dummy_unit", self:GetCursorPosition(), false, self:GetCaster(), self:GetCaster(), self:GetCaster():GetTeamNumber())

		local distance = (self:GetCaster():GetAbsOrigin() - self:GetCursorPosition()):Length2D()
		local delay = self:GetSpecialValueFor("delay")

		-- Base projectile information
		local projectile = {
			Target = self.dummy_unit,
			Source = self:GetCaster(),
			Ability = self,
			bDodgeable = false,
			EffectName = "particles/units/heroes/hero_alchemist/alchemist_unstable_concoction_projectile.vpcf",
			iMoveSpeed = distance / delay,
		}

		self:GetCaster():EmitSound("Hero_Alchemist.UnstableConcoction.Throw")
		ProjectileManager:CreateTrackingProjectile(projectile)
	end
end

function lone_druid_summon_bear:OnProjectileHit(hTarget, vLocation)
	if IsServer() then
		if self.alive_bear and IsValidEntity(self.alive_bear) then
			self.alive_bear:ForceKill(false)
		end

		local bear = CreateUnitByName("npc_dota_lone_druid_bear", vLocation, true, nil, nil, self:GetCaster():GetTeamNumber())
		bear:SetOwner(self:GetCaster())
		self.alive_bear = bear
		bear:AddNewModifier(bear, self, "modifier_summon_bear_ai", {})

		if hTarget then
			hTarget:EmitSound("Hero_Alchemist.UnstableConcoction.Stun")

			-- remove dummy unit
			UTIL_Remove(hTarget)
		end
	end
end

modifier_summon_bear_ai = class({})

function modifier_summon_bear_ai:IsDebuff() return false end
function modifier_summon_bear_ai:IsHidden() return true end
function modifier_summon_bear_ai:IsPurgable() return false end
function modifier_summon_bear_ai:IsPurgeException() return false end

function modifier_summon_bear_ai:GetTexture()
	return "lone_druid_spirit_bear"
end
--[[
function modifier_summon_bear_ai:DeclareFunctions()
	return {
		MODIFIER_EVENT_ON_DEATH,
	}
end
--]]
function modifier_summon_bear_ai:OnCreated()
	if IsServer() then
		local bear_health = self:GetAbility():GetSpecialValueFor("bear_health")
		local bear_damage = self:GetAbility():GetSpecialValueFor("bear_damage")

		self:GetParent():SetBaseMaxHealth(bear_health)
		self:GetParent():SetMaxHealth(bear_health)
		self:GetParent():SetHealth(bear_health)
		self:GetParent():SetBaseDamageMin(bear_damage - 10)
		self:GetParent():SetBaseDamageMax(bear_damage + 10)

		self:StartIntervalThink(0.1)
	end
end

function modifier_summon_bear_ai:OnIntervalThink()
	local enemies = FindUnitsInRadius(self:GetParent():GetTeamNumber(), self:GetParent():GetAbsOrigin(), nil, FIND_UNITS_EVERYWHERE, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO, DOTA_UNIT_TARGET_FLAG_MAGIC_IMMUNE_ENEMIES, FIND_CLOSEST, false)

	if self:GetParent():GetForceAttackTarget() then
		if not self:GetParent():GetForceAttackTarget():IsAlive() then
			self:GetParent():SetForceAttackTarget(nil)
		end
	end

	if self:GetParent():GetForceAttackTarget() == nil then
		for _, enemy in pairs(enemies) do
			if self:GetParent():GetForceAttackTarget() ~= enemy and enemy:IsAlive() then
				self:GetParent():SetForceAttackTarget(enemy)
				break
			end
		end
	end
end
--[[
function modifier_summon_bear_ai:OnDeath(params)
	if IsServer() then
		if params.unit == self:GetParent():GetForceAttackTarget() then
			self:GetParent():SetForceAttackTarget(nil)
		end
	end
end
--]]
function modifier_summon_bear_ai:OnDestroy()
	if IsServer() then
		if self:GetAbility().alive_bear then
			self:GetAbility().alive_bear = nil
		end
	end
end

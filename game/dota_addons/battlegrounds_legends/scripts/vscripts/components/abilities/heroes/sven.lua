bl_sven_holy_cleave = class({})

function bl_sven_holy_cleave:OnAbilityPhaseStart()
	if RandomInt(1, 100) > 50 then
		self:GetCaster():StartGesture(ACT_DOTA_ATTACK2)
	else
		self:GetCaster():StartGesture(ACT_DOTA_ATTACK)
	end

	self:GetCaster():EmitSound("Hero_Sven.PreAttack")

	return true
end

function bl_sven_holy_cleave:OnAbilityPhaseInterrupted()
	self:GetCaster():FadeGesture(ACT_DOTA_ATTACK)
	self:GetCaster():FadeGesture(ACT_DOTA_ATTACK2)
end

function bl_sven_holy_cleave:OnSpellStart()
	if IsServer() then
		local target_loc = self:GetCursorPosition()
		self.current_duration = 0.0

		-- Parameters
		local damage = self:GetSpecialValueFor("damage")
		local start_radius = self:GetSpecialValueFor("start_radius")
		local end_radius = self:GetSpecialValueFor("end_radius")
		local travel_distance = self:GetSpecialValueFor("travel_distance")
		local projectile_speed = self:GetSpecialValueFor("projectile_speed")
		local direction
		self.current_radius = start_radius
		self.cast_position = self:GetCaster():GetAbsOrigin()

		if target_loc == self.cast_position then
			direction = self:GetCaster():GetForwardVector()
		else
			direction = (target_loc - self.cast_position):Normalized()
		end

		local projectile =
		{
			Ability				= self,
			EffectName			= "particles/econ/items/sven/sven_ti7_sword/sven_ti7_sword_spell_great_cleave.vpcf",
			vSpawnOrigin		= self.cast_position,
			fDistance			= travel_distance,
			fStartRadius		= start_radius,
			fEndRadius			= end_radius,
			Source				= self:GetCaster(),
			bHasFrontalCone		= true,
			bReplaceExisting	= false,
			iUnitTargetTeam		= self:GetAbilityTargetTeam(),
			iUnitTargetFlags	= self:GetAbilityTargetFlags(),
			iUnitTargetType		= self:GetAbilityTargetType(),
			fExpireTime 		= GameRules:GetGameTime() + 10.0,
			bDeleteOnHit		= true,
			vVelocity			= Vector(direction.x,direction.y,0) * projectile_speed,
			bProvidesVision		= false,
			ExtraData			= {damage = damage}
		}

		ProjectileManager:CreateLinearProjectile(projectile)
		self:GetCaster():EmitSound("Hero_Sven.Attack")
	end
end

--[[
function bl_sven_holy_cleave:OnProjectileThink_ExtraData(vLocation, ExtraData)
	self.current_duration = self.current_duration + FrameTime()

	local radius_increase = self:GetSpecialValueFor("end_radius") / self:GetSpecialValueFor("start_radius") * 100 * FrameTime() - 3.333333
	self.current_radius = self.current_radius + radius_increase

	if GridNav:IsNearbyTree(vLocation, self.current_radius, true) then
		GridNav:DestroyTreesAroundPoint(vLocation, self.current_radius, true)
	end
end
--]]

function bl_sven_holy_cleave:OnProjectileHit_ExtraData(target, location, ExtraData)
	if target then
		target:EmitSound("Hero_Sven.Attack.Impact")

		ApplyDamage({attacker = self:GetCaster(), victim = target, ability = self, damage = ExtraData.damage, damage_type = self:GetAbilityDamageType()})

--		if target:IsAlive() == false then
--			if (math.random(1,100) <= 15) and (self:GetCaster():GetName() == "npc_dota_hero_queenofpain") then
--				self:GetCaster():EmitSound("queenofpain_pain_ability_sonicwave_0"..math.random(1,4))
--			end
--		end
	end
end

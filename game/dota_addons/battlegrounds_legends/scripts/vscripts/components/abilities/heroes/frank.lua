local function HammerOnSpellStart(hAbility)
	local target_loc = hAbility:GetCursorPosition()
	local start_radius = hAbility:GetSpecialValueFor("start_radius")
	local end_radius = hAbility:GetSpecialValueFor("end_radius")
	local speed = hAbility:GetSpecialValueFor("speed")

	local direction = hAbility:GetCursorPosition() - hAbility:GetCaster():GetAbsOrigin()
	direction.z = 0
	direction = direction:Normalized()

	--use linear proj for finding units in cone shaped area
	local proj = ProjectileManager:CreateLinearProjectile({
		EffectName = "particles/econ/items/sven/sven_ti7_sword/sven_ti7_sword_spell_great_cleave.vpcf",
		Ability = hAbility,
		vSpawnOrigin = hAbility:GetCaster():GetAbsOrigin(), 
		fStartRadius = start_radius,
		fEndRadius = end_radius,
		vVelocity = direction * speed,
		fDistance = hAbility:GetSpecialValueFor("distance"),
		Source = hAbility:GetCaster(),
		iUnitTargetTeam = DOTA_UNIT_TARGET_TEAM_ENEMY,
		iUnitTargetType = DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC,
		bHasFrontalCone = true,
		fExpireTime = GameRules:GetGameTime() + 3.0,
	})

	hAbility:GetCaster():EmitSound("Hero_Sven.IronWill")
end

local function HammerOnProjectileHit(hTarget, hAbility)
	-- Play hit sound
	hTarget:EmitSound("Hero_Magnataur.ShockWave.Target")

	if hAbility:GetAbilityName() == "frank_stunning_blow" then
		hTarget:AddNewModifier(hAbility:GetCaster(), hAbility, "modifier_stunned", {duration=hAbility:GetSpecialValueFor("stun_duration")})
	end

	ApplyDamage({
		attacker = hAbility:GetCaster(),
		victim = hTarget,
		ability = hAbility,
		damage = hAbility:GetSpecialValueFor("damage"),
		damage_type = DAMAGE_TYPE_PHYSICAL
	})
end

frank_hammer_hit = frank_hammer_hit or class({})

function frank_hammer_hit:OnSpellStart()
	-- Parameters
	if IsServer() then
		HammerOnSpellStart(self)
	end
end

function frank_hammer_hit:OnProjectileHit(hTarget, vLocation)
	if IsServer() and hTarget then
		HammerOnProjectileHit(hTarget, self)
	end
end

frank_stunning_blow = frank_stunning_blow or class({})

function frank_stunning_blow:OnSpellStart()
	-- Parameters
	if IsServer() then
		HammerOnSpellStart(self)
	end
end

function frank_stunning_blow:OnProjectileHit(hTarget, vLocation)
	if IsServer() and hTarget then
		HammerOnProjectileHit(hTarget, self)
	end
end

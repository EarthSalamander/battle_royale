bl_pudge_cleaver = class({})

function bl_pudge_cleaver:IsHiddenWhenStolen() return false end
function bl_pudge_cleaver:IsRefreshable() return true end
function bl_pudge_cleaver:IsStealable() return true end
function bl_pudge_cleaver:IsNetherWardStealable() return true end
function bl_pudge_cleaver:IsInnateAbility() return false end
function bl_pudge_cleaver:GetAbilityTextureName() return "pudge_cleaver" end

-------------------------------------------

function bl_pudge_cleaver:OnSpellStart()
	local vTargetPosition = self:GetCursorPosition()

	-- I want to remove the thing but I can't find it.
	if self:GetCaster() and self:GetCaster():IsHero() then
		local cleaver = self:GetCaster():GetTogglableWearable( DOTA_LOADOUT_TYPE_OFFHAND_WEAPON )
		if cleaver ~= nil then
			cleaver:AddEffects( EF_NODRAW )
		end
	end

	-- Determine projectile geometry
	local caster_loc = self:GetCaster():GetAbsOrigin()
	local target_loc = self:GetCursorPosition()
	local main_direction = (target_loc - caster_loc):Normalized()

	local projectile = {
		Ability				= self,
		EffectName			= "particles/hero/pudge/butchers_cleave_projectile.vpcf",
		vSpawnOrigin		= caster_loc + main_direction * 50 + Vector(0, 0, 50),
		fDistance			= self:GetCastRange(caster_loc, self:GetCaster()),
		fStartRadius		= self:GetSpecialValueFor("radius"),
		fEndRadius			= self:GetSpecialValueFor("radius"),
		Source				= self:GetCaster(),
		bHasFrontalCone		= false,
		bReplaceExisting	= false,
		iUnitTargetTeam		= DOTA_UNIT_TARGET_TEAM_ENEMY,
--		iUnitTargetFlags	= DOTA_UNIT_TARGET_FLAG_MAGIC_IMMUNE_ENEMIES,
		iUnitTargetType		= DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC,
--		fExpireTime			= ,
		bDeleteOnHit		= true,
		vVelocity			= main_direction * self:GetSpecialValueFor("speed"),
		bProvidesVision		= true,
--		iVisionRadius		= projectile_vision,
		iVisionTeamNumber	= self:GetCaster():GetTeamNumber(),
		UnitTest = function( params, unit )
			if unit:GetTeamNumber() ~= params.Source:GetTeamNumber() then
				return true
			end
		end,
		OnUnitHit = function( params, unit )
			local damageTable = {
				victim = unit,
				attacker = params.Source,
				damage = params.Ability:GetSpecialValueFor("damage"),
				damage_type = DAMAGE_TYPE_PHYSICAL,
				ability = params.Ability
			}

			ApplyDamage(damageTable)
		end,
	}

	Projectiles:CreateProjectile(projectile)

--	for i =0,30 do
--		local cleaver = self:GetCaster():GetTogglableWearable( i )
--		if cleaver ~= nil then
--			cleaver:RemoveEffects(EF_NODRAW)		
--		end
--	end
end

bl_pudge_meat_hook = bl_pudge_meat_hook or class({})

LinkLuaModifier("modifier_bl_pudge_meat_hook_caster_root","components/abilities/heroes/pudge", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_bl_hook_target_enemy","components/abilities/heroes/pudge", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_bl_hook_target_ally","components/abilities/heroes/pudge", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_bl_pudge_meat_hook_handler","components/abilities/heroes/pudge", LUA_MODIFIER_MOTION_NONE)

function bl_pudge_meat_hook:IsHiddenWhenStolen() return false end
function bl_pudge_meat_hook:IsRefreshable() return true end
function bl_pudge_meat_hook:IsStealable() return true end
function bl_pudge_meat_hook:IsNetherWardStealable() return true end

function bl_pudge_meat_hook:OnAbilityPhaseStart()
	if self.launched then
		return false
	end

	self:GetCaster():StartGesture( ACT_DOTA_OVERRIDE_ABILITY_1 )

	return true
end

function bl_pudge_meat_hook:OnAbilityPhaseInterrupted()
	self:GetCaster():RemoveGesture( ACT_DOTA_OVERRIDE_ABILITY_1 )

	--Should fix meat hook breaking sometimes
	self.launched = false
end

function bl_pudge_meat_hook:OnOwnerDied()
	self:GetCaster():RemoveGesture( ACT_DOTA_OVERRIDE_ABILITY_1 )
	self:GetCaster():RemoveGesture( ACT_DOTA_CHANNEL_ABILITY_1 )

	-- Allow again to launch meat hook
	self.launched = false
end

function bl_pudge_meat_hook:OnSpellStart()
	self.launched = true

	self:GetCaster():AddNewModifier(self:GetCaster(), self, "modifier_bl_pudge_meat_hook_caster_root", {})

	local vHookOffset = Vector( 0, 0, 96 )
	local target_position = GetGroundPosition(self:GetCursorPosition() + vHookOffset, self:GetCaster())

	self.hook_width = self:GetSpecialValueFor("hook_width")
	local hook_speed = self:GetSpecialValueFor("base_speed")
	local hook_range = self:GetSpecialValueFor("base_range")
	local stack_speed = 0
	local stack_range = 0

	if self:GetCaster():HasAbility("bl_pudge_light_hook") then
		stack_speed = self:GetCaster():FindAbilityByName("bl_pudge_light_hook"):GetSpecialValueFor("stack_speed")
		stack_range = self:GetCaster():FindAbilityByName("bl_pudge_light_hook"):GetSpecialValueFor("stack_range")
	end

	local hook_dmg = self:GetSpecialValueFor("base_damage")
	local stack_dmg = 0

	if self:GetCaster():HasAbility("bl_pudge_light_hook") then
		stack_dmg = self:GetCaster():FindAbilityByName("bl_pudge_sharp_hook"):GetSpecialValueFor("stack_damage")
	end

	--[[
	pfx shit:
	cp 0: pudge's hand
	cp 1: target position
	cp 2: speed, distance, width
	cp 3: max duration, 0, 0
	cp 4: 1,0,0
	cp 5: 0,0,0
	]]
	--[[finall date:
	hook_speed
	hook_range
	hook_dmg
	hook_width
	]]

	if not self:GetCaster().hook_pfx then
		self:GetCaster().hook_pfx = "particles/units/heroes/hero_pudge/pudge_meathook.vpcf"
--		self:GetCaster().hook_pfx = "particles/econ/items/pudge/pudge_dragonclaw/pudge_meathook_dragonclaw_bl.vpcf"
	end

	local vKillswitch = Vector(((hook_range / hook_speed) * 2) + 10, 0, 0)
	local hook_particle = ParticleManager:CreateParticle(self:GetCaster().hook_pfx, PATTACH_CUSTOMORIGIN, nil)
	ParticleManager:SetParticleAlwaysSimulate(hook_particle)
	ParticleManager:SetParticleControlEnt(hook_particle, 0, self:GetCaster(), PATTACH_POINT_FOLLOW, "attach_weapon_chain_rt", self:GetCaster():GetAbsOrigin() + vHookOffset, true)
	ParticleManager:SetParticleControl(hook_particle, 2, Vector(hook_speed, hook_range, self.hook_width))
	ParticleManager:SetParticleControl(hook_particle, 3, vKillswitch)
	ParticleManager:SetParticleControl(hook_particle, 4, Vector( 1, 0, 0 ) )
	ParticleManager:SetParticleControl(hook_particle, 5, Vector( 0, 0, 0 ) )

	if self:GetCaster().hook_pfx == "particles/units/heroes/hero_pudge/pudge_meathook.vpcf" then
		ParticleManager:SetParticleControlEnt(hook_particle, 7, self:GetCaster(), PATTACH_CUSTOMORIGIN, nil, self:GetCaster():GetOrigin(), true)
	end

	local projectile_info = {
		Ability = self,
		EffectName = nil,
		vSpawnOrigin = self:GetCaster():GetAbsOrigin(),
		fDistance = hook_range,
		fStartRadius = self.hook_width,
		fEndRadius = self.hook_width,
		Source = self:GetCaster(),
		bHasFrontalCone = false,
		bReplaceExisting = false,
		iUnitTargetTeam = DOTA_UNIT_TARGET_TEAM_BOTH,
		iUnitTargetFlags = DOTA_UNIT_TARGET_FLAG_MAGIC_IMMUNE_ENEMIES + DOTA_UNIT_TARGET_FLAG_NOT_ANCIENTS,
		iUnitTargetType = DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC,
		fExpireTime = GameRules:GetGameTime() + ((hook_range / hook_speed)),
		vVelocity = (target_position - self:GetCaster():GetAbsOrigin()):Normalized() * hook_speed,
		bProvidesVision = false,
		bDeleteOnHit = true,
		ExtraData = {
			hook_dmg = hook_dmg,
			hook_spd = hook_speed,
			pfx_index = hook_particle,
			goorback = "go",
			rune = -1,
			tree = -1,
		}
	}
	self.hook_go = ProjectileManager:CreateLinearProjectile(projectile_info)

	if self:GetCaster() and self:GetCaster():IsHero() then
		local hHook = self:GetCaster().hook_wearable
		if hHook ~= nil and not hHook:IsNull() then
			hHook:AddEffects( EF_NODRAW )
		end
	end

	EmitSoundOnLocationWithCaster(self:GetCaster():GetAbsOrigin(), "Hero_Pudge.AttackHookExtend", caster)
end

local hooked_loc
function bl_pudge_meat_hook:OnProjectileThink_ExtraData(vLocation, ExtraData)

	if ExtraData.goorback == "go" then
		ParticleManager:SetParticleControl(ExtraData.pfx_index, 1, vLocation)
		-- Hook Rune Think
		local radius = self.hook_width
		local runes = {
			"models/props_gameplay/rune_goldxp.vmdl",
			"models/props_gameplay/rune_haste01.vmdl",
			"models/props_gameplay/rune_doubledamage01.vmdl",
			"models/props_gameplay/rune_regeneration01.vmdl",
			"models/props_gameplay/rune_arcane.vmdl",
			"models/props_gameplay/rune_invisibility01.vmdl",
			"models/props_gameplay/rune_illusion01.vmdl",
			"models/props_gameplay/rune_frost.vmdl",
			"models/props_gameplay/gold_coin001.vmdl",	-- Overthrow coin
			"models/props_gameplay/treasure_chest001.vmdl",
			"models/props_gameplay/gem01.vmdl", -- Gem Grab
		}

		--check if there are runes to grab
		for _, ent in pairs(Entities:FindAllInSphere(vLocation, self.hook_width)) do
			for _, model in pairs(runes) do
				for _, rune in pairs(Entities:FindAllByModel(model)) do
					if (vLocation - rune:GetAbsOrigin()):Length2D() < self.hook_width then
						ExtraData.rune = rune:entindex()
						self:OnProjectileHit_ExtraData(nil, vLocation, ExtraData) --grab the rune
					end
				end
			end
		end

		if GridNav:IsNearbyTree(vLocation, 40, true) then
			print("A tree is near!")
			GridNav:DestroyTreesAroundPoint(vLocation, 40, true)
			self:OnProjectileHit_ExtraData(nil, vLocation, ExtraData) -- End the hook
		end
	end

	if ExtraData.goorback ~= "back" then
		hooked_loc = vLocation
	elseif ExtraData.goorback == "back" then
		if EntIndexToHScript(ExtraData.rune) then
			local rune = EntIndexToHScript(ExtraData.rune)
			ParticleManager:SetParticleControlEnt(ExtraData.pfx_index, 1, rune, PATTACH_POINT_FOLLOW, "attach_hitloc", rune:GetAbsOrigin() + Vector( 0, 0, 96 ), true)
			rune:SetAbsOrigin(GetGroundPosition(vLocation, self:GetCaster()))
		else
			local target = EntIndexToHScript(ExtraData.hooked_target)
			local location = vLocation + (self:GetCaster():GetAbsOrigin() - target:GetAbsOrigin()):Normalized() * (ExtraData.hook_spd / (1 / FrameTime()))
			target:SetAbsOrigin(GetGroundPosition(vLocation, target))

			--Talent #7: Grabbed units are ruptured
--			if self:GetCaster():GetTeamNumber() ~= target:GetTeamNumber() and not target:IsRune() and self:GetCaster():HasTalent("special_bonus_bl_pudge_7") then
--				local damage_cap = self:GetCaster():FindTalentValue("special_bonus_bl_pudge_7", "damage_cap")
--				local rupture_damage = self:GetCaster():FindTalentValue("special_bonus_bl_pudge_7", "movement_damage_pct")
--				-- vector expected got nil
--				local distance_diff = (hooked_loc - target:GetAbsOrigin()):Length2D()

--				print(distance_diff)
--				if distance_diff < damage_cap then
--					local move_damage = distance_diff * rupture_damage
--					print(move_damage)
--					if move_damage > 0 then
--						if not target.is_ruptured then
--							target.is_ruptured = true
--							self.RuptureFX = ParticleManager:CreateParticle("particles/units/heroes/hero_bloodseeker/bloodseeker_rupture.vpcf", PATTACH_POINT_FOLLOW, target)
--							EmitSoundOn("hero_bloodseeker.rupture.cast", target)
--							EmitSoundOn("hero_bloodseeker.rupture", target)
--						end

--						ApplyDamage({victim = target, attacker = self:GetCaster(), damage = move_damage, damage_type = DAMAGE_TYPE_PURE, ability = self:GetCaster():FindAbilityByName("bl_pudge_meat_hook")})
--					end
--				end
--			end
		end
	end
end

function bl_pudge_meat_hook:OnProjectileHit_ExtraData(hTarget, vLocation, ExtraData)
--	if hTarget then
--		local buff1 = hTarget:FindModifierByName("modifier_bl_hook_target_enemy")
--		local buff2 = hTarget:FindModifierByName("modifier_bl_hook_target_ally")
--	end

--	if hTarget and self:GetCaster():GetTeamNumber() ~= hTarget:GetTeamNumber() then
--		return false
--	end

	if ExtraData.goorback == "go" then
		if self:GetCaster() == hTarget or buff1 or buff2 then
			return
		end
		local root_buff = self:GetCaster():FindModifierByName("modifier_bl_pudge_meat_hook_caster_root")
		if root_buff then
			root_buff:Destroy()
		end
		ParticleManager:SetParticleControl(ExtraData.pfx_index, 4, Vector( 0, 0, 0 ) )
		ParticleManager:SetParticleControl(ExtraData.pfx_index, 5, Vector( 1, 0, 0 ) )
		local target = hTarget
		local bVision = false

		if not target then
			target = CreateUnitByName("npc_dummy_unit", vLocation, false, self:GetCaster(), self:GetCaster(), self:GetCaster():GetTeamNumber())
		end

		ParticleManager:SetParticleControlEnt(ExtraData.pfx_index, 1, target, PATTACH_POINT_FOLLOW, "attach_hitloc", target:GetAbsOrigin() + Vector(0,0,96), true)

		if hTarget then
			EmitSoundOnLocationWithCaster(hTarget:GetAbsOrigin(), "Hero_Pudge.AttackHookImpact", hTarget)
			EmitSoundOnLocationWithCaster(hTarget:GetAbsOrigin(), "Hero_Pudge.AttackHookRetract", hTarget)
			local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_pudge/pudge_meathook_impact.vpcf", PATTACH_CUSTOMORIGIN, hTarget)
			ParticleManager:SetParticleControlEnt(nFXIndex, 0, hTarget, PATTACH_POINT_FOLLOW, "attach_hitloc", hTarget:GetAbsOrigin() + Vector( 0, 0, 96 ), true)
			ParticleManager:ReleaseParticleIndex(nFXIndex)
			bVision = true
			if hTarget:GetTeamNumber() ~= self:GetCaster():GetTeamNumber() then
				local dmg = ExtraData.hook_dmg
				local damageTable = {
					victim = hTarget,
					attacker = self:GetCaster(),
					damage = dmg,
					damage_type = DAMAGE_TYPE_PURE,
					damage_flags = DOTA_DAMAGE_FLAG_NONE, --Optional.
					ability = self, --Optional.
				}
				local actually_dmg = ApplyDamage(damageTable)
				SendOverheadEventMessage(nil, OVERHEAD_ALERT_DAMAGE, hTarget, actually_dmg, nil)
				hTarget:AddNewModifier(self:GetCaster(), self, "modifier_bl_hook_target_enemy", {})

--				if HasPudgeArcana(self:GetCaster()) then -- error for reasons, maybe because target is dead
--					if hTarget:IsRealHero() then
--						self:GetCaster().successful_hooks = self:GetCaster().successful_hooks + 1
--					else
--						self:GetCaster().successful_hooks = 0
--					end

--					if self:GetCaster().successful_hooks >= 2 then
--						EmitSoundOnLocationWithCaster(self:GetCaster():GetAbsOrigin(), "Hero_Pudge.HookDrag.Arcana", self:GetCaster())
--						local pfx = "particles/econ/items/pudge/pudge_arcana/pudge_arcana_red_hook_streak.vpcf"
--						if self:GetCaster().battlepass_arcana == 1 then
--							pfx = "particles/econ/items/pudge/pudge_arcana/pudge_arcana_hook_streak.vpcf"
--						end

--						local hook_counter = ParticleManager:CreateParticle(pfx, PATTACH_OVERHEAD_FOLLOW, self:GetCaster())
--						local stack_10 = math.floor(self:GetCaster().successful_hooks / 10)
--						ParticleManager:SetParticleControl(hook_counter, 2, Vector(stack_10, self:GetCaster().successful_hooks - stack_10 * 10, self:GetCaster().successful_hooks))
--						ParticleManager:ReleaseParticleIndex(hook_counter)
--					end
--				end
			elseif hTarget:GetTeamNumber() ~= self:GetCaster():GetTeamNumber() then
				hTarget:AddNewModifier(self:GetCaster(), self, "modifier_bl_hook_target_ally", {})
			else
--				if HasPudgeArcana(self:GetCaster()) then
--					self:GetCaster().successful_hooks = 0
--				end
			end
		else
--			if HasPudgeArcana(self:GetCaster()) then
--				self:GetCaster().successful_hooks = 0
--			end
		end

		local projectile_info = {
			Target = self:GetCaster(),
			Source = target,
			Ability = self,
			EffectName = nil,
			iMoveSpeed = ExtraData.hook_spd,
			vSourceLoc = target:GetAbsOrigin(),
			bDrawsOnMinimap = false,
			bDodgeable = false,
			bIsAttack = false,
			bVisibleToEnemies = true,
			bReplaceExisting = false,
			bProvidesVision = bVision,
			iVisionRadius = 400,
			iVisionTeamNumber = self:GetCaster():GetTeamNumber(),
			ExtraData = {
				hooked_target = target:entindex(),
				hook_spd = ExtraData.hook_spd,
				pfx_index = ExtraData.pfx_index,
				goorback = "back",
				rune = ExtraData.rune,
			}
		}
		ProjectileManager:CreateTrackingProjectile(projectile_info)
		if self:GetCaster():IsAlive() then
			self:GetCaster():FadeGesture(ACT_DOTA_OVERRIDE_ABILITY_1)
		end
		if self.hook_go then
			ProjectileManager:DestroyLinearProjectile(self.hook_go)
		end
		return true
	end

	if ExtraData.goorback == "back" then
		ParticleManager:DestroyParticle(ExtraData.pfx_index, true)
		ParticleManager:ReleaseParticleIndex(ExtraData.pfx_index)

		local target = EntIndexToHScript(ExtraData.hooked_target)
		target:SetUnitOnClearGround()
		EmitSoundOnLocationWithCaster(target:GetAbsOrigin(), "Hero_Pudge.AttackHookRetractStop", target)
		self:GetCaster():StopSound("Hero_Pudge.AttackHookExtend")

		if target:GetUnitName() == "npc_dummy_unit" then
			target:ForceKill(false)
		else
			target:StopSound("Hero_Pudge.AttackHookImpact")
			target:StopSound("Hero_Pudge.AttackHookRetract")
		end

		self:GetCaster():FadeGesture(ACT_DOTA_CHANNEL_ABILITY_1)

		if self:GetCaster() and self:GetCaster():IsHero() then
			local hHook = self:GetCaster().hook_wearable
			if hHook ~= nil and not hHook:IsNull() then
				hHook:RemoveEffects( EF_NODRAW )
			end

			if target.is_ruptured then
				target.is_ruptured = false
			end

			StopSoundOn( "Hero_Pudge.AttackHookRetract", self:GetCaster())
			StopSoundOn( "Hero_Pudge.AttackHookExtend", self:GetCaster())
			StopSoundOn( "Hero_Pudge.AttackHookRetractStop", self:GetCaster())
		end

		local buff1 = target:FindModifierByName("modifier_bl_hook_target_enemy")
		local buff2 = target:FindModifierByName("modifier_bl_hook_target_ally")
		if buff1 then buff1:Destroy() end
		if buff2 then buff2:Destroy() end

		self.launched = false

		if self.RuptureFX then
			ParticleManager:DestroyParticle(self.RuptureFX, true)
			ParticleManager:ReleaseParticleIndex(self.RuptureFX)
		end

		return true
	end
end

function bl_pudge_meat_hook:GetIntrinsicModifierName()
	return "modifier_bl_pudge_meat_hook_handler"
end

function bl_pudge_meat_hook:GetAbilityTextureName()
	if not IsClient() then return end
	if self:GetCaster().meat_hook_icon then
		return "custom/pudge_meat_hook_arcana_style"..self:GetCaster().meat_hook_icon
	else
		return "pudge_meat_hook"
	end
end

if modifier_bl_pudge_meat_hook_handler == nil then modifier_bl_pudge_meat_hook_handler = class({}) end

function modifier_bl_pudge_meat_hook_handler:IsHidden() return true end
function modifier_bl_pudge_meat_hook_handler:RemoveOnDeath() return false end

function modifier_bl_pudge_meat_hook_handler:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_TRANSLATE_ACTIVITY_MODIFIERS
	}

	return funcs
end

function modifier_bl_pudge_meat_hook_handler:OnCreated()
	if self:GetCaster():IsIllusion() then self:Destroy() return end

	if IsServer() then
		if self:GetCaster().battlepass_arcana == nil then self:Destroy() return end
		self:SetStackCount(self:GetCaster().battlepass_arcana + 1)
	end

	if IsClient() then
		if self:GetStackCount() == 0 then self:Destroy() return end
		self:GetCaster().meat_hook_icon = self:GetStackCount() - 1
	end
end

function modifier_bl_pudge_meat_hook_handler:GetActivityTranslationModifiers()
--	print(self:GetCaster().successful_hooks)
	if self:GetCaster().successful_hooks == nil then self:GetCaster().successful_hooks = 0 end

	if self:GetCaster().successful_hooks >= 1 and self:GetCaster().successful_hooks < 3 then
--		print("Small Streak!")
		return "hook_streak_small"
	elseif self:GetCaster().successful_hooks >= 3 and self:GetCaster().successful_hooks < 5 then
--		print("Medium Streak!")
		return "hook_streak_medium"
	elseif self:GetCaster().successful_hooks >= 5 then
--		print("Large Streak!")
		return "hook_streak_large"
	end
end

modifier_bl_pudge_meat_hook_caster_root = modifier_bl_pudge_meat_hook_caster_root or class({})

function modifier_bl_pudge_meat_hook_caster_root:IsDebuff() return true end
function modifier_bl_pudge_meat_hook_caster_root:IsHidden() return true end
function modifier_bl_pudge_meat_hook_caster_root:IsPurgable() return false end
function modifier_bl_pudge_meat_hook_caster_root:IsStunDebuff() return false end
function modifier_bl_pudge_meat_hook_caster_root:RemoveOnDeath() return true end

function modifier_bl_pudge_meat_hook_caster_root:CheckState()
	local state =
		{
			[MODIFIER_STATE_ROOTED] = true,
		}
	return state
end

function modifier_bl_pudge_meat_hook_caster_root:OnCreated()
	if not IsServer() then return end
	local disable_items = {"item_tpscroll", "item_travel_boots", "item_travel_boots_2"}
	local caster = self:GetCaster()
	self.disable = {}
	for i=0,8 do
		local item = caster:GetItemInSlot(i)
		for _, check in pairs(disable_items) do
			if item and item:GetAbilityName() == check then
				item:SetActivated(false)
				table.insert(self.disable, item)
				break
			end
		end
	end
end

function modifier_bl_pudge_meat_hook_caster_root:OnDestroy()
	if not IsServer() then return end
	for _,item in pairs(self.disable) do
		if item then
			item:SetActivated(true)
		end
	end
end

modifier_bl_hook_target_enemy = modifier_bl_hook_target_enemy or class({})

function modifier_bl_hook_target_enemy:IsDebuff()
	if self:GetCaster():GetTeamNumber() == self:GetParent():GetTeamNumber() then
		return false
	else
		return true
	end
end

function modifier_bl_hook_target_enemy:IsHidden() return false end
function modifier_bl_hook_target_enemy:IsPurgable() return false end
function modifier_bl_hook_target_enemy:IsStunDebuff() return false end
function modifier_bl_hook_target_enemy:RemoveOnDeath() return false end
function modifier_bl_hook_target_enemy:IsMotionController()  return true end
function modifier_bl_hook_target_enemy:GetMotionControllerPriority()  return DOTA_MOTION_CONTROLLER_PRIORITY_HIGHEST end

function modifier_bl_hook_target_enemy:CheckState()
	local state_ally =
		{
			[MODIFIER_STATE_ROOTED] = true,
		}
	local state_enemy =
		{
			[MODIFIER_STATE_STUNNED] = true,
		}
	if self:GetCaster():GetTeamNumber() == self:GetParent():GetTeamNumber() then
		return state_ally
	else
		return state_enemy
	end
end

modifier_bl_hook_target_ally = modifier_bl_hook_target_ally or class({})

function modifier_bl_hook_target_ally:IsDebuff()
	if self:GetCaster():GetTeamNumber() == self:GetParent():GetTeamNumber() then
		return false
	else
		return true
	end
end

function modifier_bl_hook_target_ally:IsHidden() return false end
function modifier_bl_hook_target_ally:IsPurgable() return false end
function modifier_bl_hook_target_ally:IsStunDebuff() return false end
function modifier_bl_hook_target_ally:RemoveOnDeath() return false end
function modifier_bl_hook_target_ally:IsMotionController()  return true end
function modifier_bl_hook_target_ally:GetMotionControllerPriority()  return DOTA_MOTION_CONTROLLER_PRIORITY_HIGHEST end

function modifier_bl_hook_target_ally:CheckState()
	local state_ally =
		{
			[MODIFIER_STATE_ROOTED] = true,
		}
	local state_enemy =
		{
			[MODIFIER_STATE_STUNNED] = true,
		}
	if self:GetCaster():GetTeamNumber() == self:GetParent():GetTeamNumber() then
		return state_ally
	else
		return state_enemy
	end
end

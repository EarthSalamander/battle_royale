sniper_gust = sniper_gust or class({})

function sniper_gust:OnSpellStart()
	if IsServer() then
		local caster_loc = self:GetCaster():GetAbsOrigin()
		local target_loc = self:GetCursorPosition()
		local projectile_cone = 30
		local projectile_count = self:GetSpecialValueFor("projectile_count")
		local projectile_launched = 0

		-- Determine projectile geometry
		local main_direction = (target_loc - caster_loc):Normalized()

		-- Launch projectiles
		Timers:CreateTimer(function()
			projectile_launched = projectile_launched + 1

--			local projectile =
--			{
--				bLinearProjectile	= true,
--				hCaster				= self:GetCaster(),
--				Ability				= self,
--				hTarget				= "dummy",
--				hTargetPosition		= target_loc,
--				EffectName			= "particles/units/heroes/hero_alchemist/alchemist_unstable_concoction_projectile.vpcf", -- "particles/econ/items/magnataur/shock_of_the_anvil/magnataur_shockanvil.vpcf",
--				EffectNameOnHit		= "particles/units/heroes/hero_pudge/pudge_meathook_impact.vpcf",
--				iMoveSpeed			= self:GetSpecialValueFor("speed"),
--				flRadius			= self:GetSpecialValueFor("radius"),
--				flMaxDistance		= self:GetCastRange(self:GetCaster():GetAbsOrigin(), self:GetCaster()),
--				damage				= self:GetSpecialValueFor("damage"),
--				sSoundDamage		= "Hero_Magnataur.ShockWave.Target",
--				iUnitTargetTeam		= DOTA_UNIT_TARGET_TEAM_ENEMY,
--				iUnitTargetType		= DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC,
--				bDeleteOnHit		= true,
--				bBounceOnTree		= true,
--				OnProjectileHitUnit = function( params, projectileID )

--				end,
--				OnProjectileDestroy = function( params, projectileID )

--				end,
--			}

--			TrackingProjectiles:Projectile(projectile)

			local projectile = {
				Ability				= self,
				EffectName			= "particles/econ/items/magnataur/shock_of_the_anvil/magnataur_shockanvil.vpcf",
				vSpawnOrigin		= caster_loc + main_direction * 50 + Vector(0, 0, 50),
				fDistance			= self:GetCastRange(self:GetCaster():GetAbsOrigin(), self:GetCaster()),
				fStartRadius		= self:GetSpecialValueFor("radius"),
				fEndRadius			= self:GetSpecialValueFor("radius"),
				Source				= self:GetCaster(),
				bHasFrontalCone		= false,
				bReplaceExisting	= false,
				iUnitTargetTeam		= DOTA_UNIT_TARGET_TEAM_ENEMY,
--				iUnitTargetFlags	= DOTA_UNIT_TARGET_FLAG_MAGIC_IMMUNE_ENEMIES,
				iUnitTargetType		= DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC,
--				fExpireTime			= ,
				bDeleteOnHit		= true,
				vVelocity			= main_direction * self:GetSpecialValueFor("speed"),
				bProvidesVision		= true,
--				iVisionRadius		= projectile_vision,
				iVisionTeamNumber	= self:GetCaster():GetTeamNumber(),
				TreeBehavior		= PROJECTILES_BOUNCE,
				UnitTest = function( params, unit )
					if unit:GetTeamNumber() ~= params.Source:GetTeamNumber() then
						return true
					end
				end,
				OnUnitHit = function( params, unit )
					local damageTable = {
						victim = unit,
						attacker = params.Source,
						damage = params.Ability:GetSpecialValueFor("damage"),
						damage_type = DAMAGE_TYPE_PHYSICAL,
						ability = params.Ability
					}

					ApplyDamage(damageTable)
				end,
			}

			Projectiles:CreateProjectile(projectile)

			if projectile_launched >= projectile_count then
				return nil
			else
				return self:GetSpecialValueFor("projectile_delay")
			end
		end)
	end
end

bl_sniper_assassinate = class({})
LinkLuaModifier("modifier_imba_assassinate_cross", "components/abilities/heroes/hero_sniper.lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_imba_assassinate_ministun", "components/abilities/heroes/hero_sniper.lua", LUA_MODIFIER_MOTION_NONE)

function bl_sniper_assassinate:GetAbilityTextureName()
	return "sniper_assassinate"
end

function bl_sniper_assassinate:IsHiddenWhenStolen()
	return false
end

function bl_sniper_assassinate:GetBehavior()
	local caster = self:GetCaster()
	local scepter = caster:HasScepter()

	if scepter then
		return DOTA_ABILITY_BEHAVIOR_POINT + DOTA_ABILITY_BEHAVIOR_NORMAL_WHEN_STOLEN + DOTA_ABILITY_BEHAVIOR_AOE
	else
		return DOTA_ABILITY_BEHAVIOR_UNIT_TARGET + DOTA_ABILITY_BEHAVIOR_NORMAL_WHEN_STOLEN
	end
end

function bl_sniper_assassinate:GetCastPoint()
	local cast_point = self.BaseClass.GetCastPoint(self)

	-- #6 Talent: Assassination's cast point increases. When executed, it launches three projectiles towards the target.
	cast_point = cast_point + self:GetCaster():FindTalentValue("special_bonus_imba_sniper_6", "cast_point_increase")
	return cast_point
end

function bl_sniper_assassinate:GetCastAnimation()
	return nil
end

function bl_sniper_assassinate:GetAOERadius()
	-- Ability properties
	local caster = self:GetCaster()
	local ability = self
	local scepter = caster:HasScepter()

	-- Ability specials
	local scepter_radius = ability:GetSpecialValueFor("scepter_radius")

	if scepter then
		return scepter_radius
	end

	return 0
end

function bl_sniper_assassinate:GetAssociatedSecondaryAbilities()
	return "imba_sniper_headshot"
end

function bl_sniper_assassinate:OnAbilityPhaseStart()
	-- Ability properties
	local caster = self:GetCaster()
	local ability = self
	local cast_response = {"sniper_snip_ability_assass_02", "sniper_snip_ability_assass_06", "sniper_snip_ability_assass_07", "sniper_snip_ability_assass_08"}
	local modifier_cross = "modifier_imba_assassinate_cross"
	local scepter = caster:HasScepter()

	-- Ability specials
	local sight_duration = ability:GetSpecialValueFor("sight_duration")
	local scepter_radius = ability:GetSpecialValueFor("scepter_radius")

	-- #6 Talent: Assassination's cast point increases. When executed, it launches three projectiles towards the target.
	-- Reset the animation before Sniper shoots
	if caster:HasTalent("special_bonus_imba_sniper_6") then
		-- "Reload" animation, to make it look good with the entire visual of the assassination talent
		caster:StartGestureWithPlaybackRate(ACT_DOTA_CAST_ABILITY_1,0.75)

		Timers:CreateTimer(1.75, function()
			caster:FadeGesture(ACT_DOTA_CAST_ABILITY_1)
			caster:StartGesture(ACT_DOTA_CAST_ABILITY_4)
		end)
	else
		-- Play assassination animation
		caster:StartGesture(ACT_DOTA_CAST_ABILITY_4)
	end

	-- Initialize index table
	if not self.enemy_table then
		self.enemy_table = {}
	end

	-- Targets
	local targets = {}

	-- Get targets
	if not scepter then
		targets[1] = self:GetCursorTarget()
	else
		-- Find all enemies in AoE
		local target_point = self:GetCursorPosition()
		targets = FindUnitsInRadius(caster:GetTeamNumber(),
			target_point,
			nil,
			scepter_radius,
			DOTA_UNIT_TARGET_TEAM_ENEMY,
			DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC,
			DOTA_UNIT_TARGET_FLAG_NO_INVIS + DOTA_UNIT_TARGET_FLAG_FOW_VISIBLE + DOTA_UNIT_TARGET_FLAG_INVULNERABLE + DOTA_UNIT_TARGET_FLAG_MAGIC_IMMUNE_ENEMIES,
			FIND_ANY_ORDER,
			false
		)
	end

	-- Play prepare cast response
	EmitSoundOn(cast_response[math.random(1, #cast_response)], caster)

	-- Apply crosshair on target(s)
	for _,target in pairs(targets) do
		target:AddNewModifier(caster, ability, modifier_cross, {duration = sight_duration})

		-- Index enemy
		table.insert(self.enemy_table, target)
	end

	return true
end

function bl_sniper_assassinate:OnAbilityPhaseInterrupted()
	-- Ability properties
	local caster = self:GetCaster()
	local ability = self
	local modifier_cross = "modifier_imba_assassinate_cross"

	-- Stop animations
	caster:FadeGesture(ACT_DOTA_CAST_ABILITY_1)
	caster:FadeGesture(ACT_DOTA_CAST_ABILITY_4)

	-- Find all enemies
	local enemies = FindUnitsInRadius(caster:GetTeamNumber(),
		caster:GetAbsOrigin(),
		nil,
		25000, -- global
		DOTA_UNIT_TARGET_TEAM_ENEMY,
		DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC,
		DOTA_UNIT_TARGET_FLAG_OUT_OF_WORLD + DOTA_UNIT_TARGET_FLAG_MAGIC_IMMUNE_ENEMIES + DOTA_UNIT_TARGET_FLAG_INVULNERABLE,
		FIND_ANY_ORDER,
		false)

	-- Remove the cross modifier from them, if they have it
	for _,enemy in pairs(enemies) do
		if enemy:HasModifier(modifier_cross) then
			enemy:RemoveModifierByName(modifier_cross)
		end
	end

	-- Clear enemy table
	self.enemy_table = nil
end

function bl_sniper_assassinate:OnSpellStart()
	-- Ability properties
	local caster = self:GetCaster()
	local ability = self
	local scepter = caster:HasScepter()

	-- Ability specials
	local scepter_radius = ability:GetSpecialValueFor("scepter_radius")
	local projectiles = ability:GetSpecialValueFor("projectiles")

	-- Targets
	local targets = {}

	-- Get targets
	if not scepter then
		targets[1] = self:GetCursorTarget()
	else
		-- Find all enemies that were marked in the AoE
		for _,enemy in pairs(self.enemy_table) do
			table.insert(targets, enemy)
		end

		-- Clear the enemy table for the next use
		self.enemy_table = nil
	end

	-- Make up a table of enemies hit by the current cast
	self.enemies_hit = {}

	-- Kill responses marker
	self.enemy_died = false

	-- #6 Talent: Assassination's cast point increases. When executed, it launches three projectiles towards the target.
	-- Increase projectiles to fire
	if caster:HasTalent("special_bonus_imba_sniper_6") then
		projectiles = caster:FindTalentValue("special_bonus_imba_sniper_6", "total_projectiles")
	end

	-- Fire projectiles!
	local projectiles_fired = 0
	Timers:CreateTimer(function()
		-- Increment projectile count
		projectiles_fired = projectiles_fired + 1

		-- Fire projectile
		self:FireAssassinateProjectile(targets, projectiles_fired)

		-- Check if we need to fire more projectiles (#6 Talent)
		if projectiles_fired < projectiles then
			local refire_delay = caster:FindTalentValue("special_bonus_imba_sniper_6", "refire_delay")
			return refire_delay
		end
	end)
end

function bl_sniper_assassinate:FireAssassinateProjectile(targets, projectile_num)
	if IsServer() then
		local caster = self:GetCaster()
		local ability = self
		local particle_projectile = "particles/units/heroes/hero_sniper/sniper_assassinate.vpcf"
		local sound_assassinate = "Ability.Assassinate"
		local sound_assassinate_launch = "Hero_Sniper.AssassinateProjectile"


		-- Play assassinate sound
		EmitSoundOn(sound_assassinate, caster)

		-- Play assassinate projectile sound
		EmitSoundOn(sound_assassinate_launch, caster)

		-- Ability specials
		local travel_speed = ability:GetSpecialValueFor("travel_speed")

		-- Mark the target(s) as a primary target
		for _,target in pairs(targets) do
			target.primary_assassination_target = true

			-- Launch assassinate projectile
			local assassinate_projectile
			assassinate_projectile = {Target = target,
				Source = caster,
				Ability = ability,
				EffectName = particle_projectile,
				iMoveSpeed = travel_speed,
				bDodgeable = true,
				bVisibleToEnemies = true,
				bReplaceExisting = false,
				bProvidesVision = false,
				ExtraData = {projectile_num = projectile_num}
			}

			ProjectileManager:CreateTrackingProjectile(assassinate_projectile)
		end
	end
end

function bl_sniper_assassinate:OnProjectileHit_ExtraData(target, location, extradata)
	-- If there was no target, do nothing
	if not target then
		return nil
	end

	-- Ability properties
	local caster = self:GetCaster()
	local ability = self
	local almost_kill_responses = {"sniper_snip_ability_fail_02", "sniper_snip_ability_fail_04", "sniper_snip_ability_fail_05", "sniper_snip_ability_fail_06", "sniper_snip_ability_fail_07", "sniper_snip_ability_fail_08"}
	local modifier_cross = "modifier_imba_assassinate_cross"
	local modifier_ministun = "modifier_imba_assassinate_ministun"

	-- Projectile's extra data
	local projectile_num = extradata.projectile_num

	-- Ability special
	local damage = ability:GetSpecialValueFor("damage")
	local ministun_duration = ability:GetSpecialValueFor("ministun_duration")

	-- If the target still has it, remove the crosshair modifier
	if target:HasModifier(modifier_cross) then
		target:RemoveModifierByName(modifier_cross)
	end

	self:AssassinateHit(target, projectile_num)

	-- Wait a small delay, then remove the primary target mark
	Timers:CreateTimer(0.3, function()
		target.primary_assassination_target = false
	end)

	-- Wait a very small delay, then check if nobody died, and the primary target is almost dead
	Timers:CreateTimer(0.1, function()
		if not self.enemy_died then
			local hp_pct = target:GetHealthPercent()
			if hp_pct <= 10 and target:IsAlive() then
				EmitSoundOn(almost_kill_responses[math.random(1, #almost_kill_responses)], caster)
			end
		end
	end)
end

function bl_sniper_assassinate:AssassinateHit(target, projectile_num)
	-- Ability properties
	local caster = self:GetCaster()
	local ability = self
	local kill_responses = {"sniper_snip_ability_assass_03", "sniper_snip_ability_assass_04", "sniper_snip_ability_assass_05", "sniper_snip_ability_assass_03", "sniper_snip_kill_03", "sniper_snip_kill_08", "sniper_snip_kill_10", "sniper_snip_kill_13", "sniper_snip_tf2_01", "sniper_snip_tf2_01"}
	local particle_sparks = "particles/units/heroes/hero_sniper/sniper_assassinate_impact_sparks.vpcf"
	local particle_light = "particles/units/heroes/hero_sniper/sniper_assassinate_endpoint.vpcf"
	local particle_stun = "particles/hero/sniper/perfectshot_stun.vpcf"
	local modifier_ministun = "modifier_imba_assassinate_ministun"
	local modifier_perfectshot = "modifier_imba_perfectshot_stun"
	local modifier_headshot = "modifier_imba_headshot_slow"
	local head_shot_ability = "imba_sniper_headshot"
	local scepter = caster:HasScepter()

	-- Ability special
	local damage = ability:GetSpecialValueFor("damage")
	local ministun_duration = ability:GetSpecialValueFor("ministun_duration")

	-- Add table key to target
	local target_key = target:entindex()..tostring(projectile_num)

	-- If that enemy was already hit on this cast, do nothing
	for _,enemy in pairs(self.enemies_hit) do
		if enemy == target_key then
			return nil
		end
	end

	-- Add that enemy to the list of enemies that got hit
	table.insert(self.enemies_hit, target_key)

	-- Primary target block with Linken and do not need the extra hit particle
	if target.primary_assassination_target then

		-- If target has Linken's Sphere off cooldown, do nothing
		if target:GetTeam() ~= caster:GetTeam() then
			if target:TriggerSpellAbsorb(ability) then
				return nil
			end
		end
	else
		-- Apply blow particles on non-primary targets that got hit
		local particle_sparks_fx = ParticleManager:CreateParticle(particle_sparks, PATTACH_CUSTOMORIGIN, caster)
		ParticleManager:SetParticleControlEnt(particle_sparks_fx, 0, caster, PATTACH_POINT_FOLLOW, "attach_hitloc", caster:GetAbsOrigin(), true)
		ParticleManager:SetParticleControlEnt(particle_sparks_fx, 1, target, PATTACH_POINT_FOLLOW, "attach_hitloc", target:GetAbsOrigin(), true)
		ParticleManager:ReleaseParticleIndex(particle_sparks_fx)

		local particle_light_fx = ParticleManager:CreateParticle(particle_light, PATTACH_CUSTOMORIGIN, caster)
		ParticleManager:SetParticleControlEnt(particle_light_fx, 0, target, PATTACH_POINT_FOLLOW, "attach_hitloc", target:GetAbsOrigin(), true)
		ParticleManager:ReleaseParticleIndex(particle_light_fx)
	end

	if not scepter then
		-- Deal damage to target, if it's not magic immune
		if not target:IsMagicImmune() then
			local damageTable = {victim = target,
				attacker = caster,
				damage = damage,
				damage_type = DAMAGE_TYPE_MAGICAL,
				ability = ability
			}

			ApplyDamage(damageTable)

			-- Apply a ministun to the target
			target:AddNewModifier(caster, ability, modifier_ministun, {duration = ministun_duration})
		end
	else
		-- Calculate the damage based on the attack damage of the caster, plus Perfectshot critical damage bonus
		if caster:HasAbility(head_shot_ability) then
			local head_shot_ability_handler = caster:FindAbilityByName(head_shot_ability)
			if head_shot_ability_handler then
				-- Get headshot ability specials
				local perfectshot_critical_dmg_pct = head_shot_ability_handler:GetSpecialValueFor("perfectshot_critical_dmg_pct")
				local headshot_duration = head_shot_ability_handler:GetSpecialValueFor("headshot_duration")
				local perfectshot_stun_duration = head_shot_ability_handler:GetSpecialValueFor("perfectshot_stun_duration")

				-- Get the caster's attack damage
				local damage = caster:GetAverageTrueAttackDamage(target) * (perfectshot_critical_dmg_pct * 0.01)

				-- Main damage type is physical. If the caster is wielding Spellfencer though, it becomes magical
				local damage_type = DAMAGE_TYPE_PHYSICAL
				if caster:HasModifier("modifier_item_imba_spell_fencer_unique") then
					damage_type = DAMAGE_TYPE_MAGICAL
				end

				--Deal damage to the target
				local damageTable = {victim = target,
					attacker = caster,
					damage = damage,
					damage_type = damage_type,
					ability = ability
				}

				ApplyDamage(damageTable)

				-- Imitate a critical by sending an overhead message
				SendOverheadEventMessage(nil, OVERHEAD_ALERT_CRITICAL, target, damage, nil)

				-- See if Take Aim is ready before the attack
				local take_aim_ability_handler = caster:FindAbilityByName("imba_sniper_take_aim")
				local take_aim_ready = false
				if take_aim_ability_handler then
					if take_aim_ability_handler:IsCooldownReady() then
						take_aim_ready = true
					end
				end

				-- Perform a fake attack to proc on-hit effects
				caster:PerformAttack(target, false, true, true, true, false, true, true)

				-- If after the fake attack Take Aim got reset, refresh its cooldown quickly
				if take_aim_ready then
					take_aim_ability_handler:EndCooldown()
				end

				-- Perfectshot stun the target, and headshot slow it
				target:AddNewModifier(caster, ability, modifier_headshot, {duration = headshot_duration})
				target:AddNewModifier(caster, ability, modifier_perfectshot, {duration = perfectshot_stun_duration})

				-- Add Perfectshot particle effects
				local particle_stun_fx = ParticleManager:CreateParticle(particle_stun, PATTACH_OVERHEAD_FOLLOW, target)
				ParticleManager:SetParticleControl(particle_stun_fx, 0, target:GetAbsOrigin())
				ParticleManager:SetParticleControl(particle_stun_fx, 1, target:GetAbsOrigin())

				-- Remove particles when they end
				Timers:CreateTimer(headshot_duration, function()
					ParticleManager:DestroyParticle(particle_stun_fx, false)
					ParticleManager:ReleaseParticleIndex(particle_stun_fx)
				end)
			end
		end
	end

	-- #2 Talent: Assassinate now knockbacks all units hit
	if caster:HasTalent("special_bonus_imba_sniper_2") then
		local push_distance = caster:FindTalentValue("special_bonus_imba_sniper_2")

		-- Knockback enemies up and towards the target point
		local knockbackProperties =
			{
				center_x = caster:GetAbsOrigin().x,
				center_y = caster:GetAbsOrigin().y,
				center_z = caster:GetAbsOrigin().z,
				duration = 0.2,
				knockback_duration = 0.2,
				knockback_distance = push_distance,
				knockback_height = 0
			}

		target:RemoveModifierByName("modifier_knockback")
		target:AddNewModifier(target, nil, "modifier_knockback", knockbackProperties)
	end

	-- Wait a game tick, and see if a target has died. If so, and it was the first to die, speak and mark the event
	Timers:CreateTimer(FrameTime(), function()
		if not target:IsAlive() and not self.enemy_died then
			self.enemy_died = true

			if RollPercentage(50) then
				EmitSoundOn(kill_responses[math.random(1, #kill_responses)], caster)
			end
		end
	end)
end

function bl_sniper_assassinate:OnProjectileThink_ExtraData(location, extradata)
	-- Ability properties
	local caster = self:GetCaster()
	local ability = self

	-- Extra Data
	local projectile_num = extradata.projectile_num

	-- Ability specials
	local bullet_radius = ability:GetSpecialValueFor("bullet_radius")

	-- Find enemy units around the area of the bullet
	local enemies = FindUnitsInRadius(caster:GetTeamNumber(),
		location,
		nil,
		bullet_radius,
		DOTA_UNIT_TARGET_TEAM_ENEMY,
		DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC,
		DOTA_UNIT_TARGET_FLAG_MAGIC_IMMUNE_ENEMIES,
		FIND_ANY_ORDER,
		false)

	for _,enemy in pairs(enemies) do
		if not enemy.primary_assassination_target then
			self:AssassinateHit(enemy, projectile_num)
		end
	end
end

function bl_sniper_assassinate:GetCastRange()
	return self:GetSpecialValueFor("cast_range")
end

-- Cross enemy debuff
modifier_imba_assassinate_cross = class({})

function modifier_imba_assassinate_cross:OnCreated()
	if IsServer() then
		self.caster = self:GetCaster()
		self.parent = self:GetParent()
		self.particle_cross = "particles/units/heroes/hero_sniper/sniper_crosshair.vpcf"

		-- Netural units do not share vision
		if self.parent:IsNeutralUnitType() then
			self.should_share_vision = false
		else
			self.should_share_vision = true
		end

		-- Apply crosshair for allies
		self.particle_cross_fx = ParticleManager:CreateParticleForTeam(self.particle_cross, PATTACH_OVERHEAD_FOLLOW, self.parent, self.caster:GetTeamNumber())
		ParticleManager:SetParticleControl(self.particle_cross_fx, 0, self.parent:GetAbsOrigin())
		self:AddParticle(self.particle_cross_fx, false, false, -1, false, true)
	end
end

function modifier_imba_assassinate_cross:GetAttributes()
	return MODIFIER_ATTRIBUTE_IGNORE_INVULNERABLE
end

function modifier_imba_assassinate_cross:IsHidden() return false end
function modifier_imba_assassinate_cross:IsPurgable() return false end
function modifier_imba_assassinate_cross:IsDebuff() return true end

function modifier_imba_assassinate_cross:CheckState()
	local state = nil
	if self:GetParent():HasModifier("modifier_slark_shadow_dance") then
		state = {[MODIFIER_STATE_PROVIDES_VISION] = true}
	end

	if self.should_share_vision then
		state = {[MODIFIER_STATE_PROVIDES_VISION] = true,
			[MODIFIER_STATE_INVISIBLE] = false}
	end

	return state
end

function modifier_imba_assassinate_cross:GetPriority()
	return MODIFIER_PRIORITY_HIGH
end

-- Ministun debuff
modifier_imba_assassinate_ministun = class({})

function modifier_imba_assassinate_ministun:IsHidden() return false end
function modifier_imba_assassinate_ministun:IsPurgeException() return true end
function modifier_imba_assassinate_ministun:IsStunDebuff() return true end

function modifier_imba_assassinate_ministun:CheckState()
	local state = {[MODIFIER_STATE_STUNNED] = true}
	return state
end

function modifier_imba_assassinate_ministun:GetEffectName()
	return "particles/generic_gameplay/generic_stunned.vpcf"
end

function modifier_imba_assassinate_ministun:GetEffectAttachType()
	return PATTACH_OVERHEAD_FOLLOW
end

shadow_strike = shadow_strike or class({})

function shadow_strike:OnSpellStart()
	-- Parameters
	if IsServer() then
		local projectile_directions = {}

		self:GetCaster():EmitSound("Hero_QueenOfPain.ShadowStrike")

		-- Determine projectile geometry
		for i = 1, self:GetSpecialValueFor("projectile_count") do
			local caster_loc = self:GetCaster():GetAbsOrigin()
			local main_direction = (self:GetCursorPosition() - self:GetCaster():GetAbsOrigin()):Normalized()
			local angle_step = self:GetSpecialValueFor("projectile_cone") / (self:GetSpecialValueFor("projectile_count") - 1)

			projectile_directions[i] = RotatePosition(self:GetCaster():GetAbsOrigin(), QAngle(0, (i - 1) * angle_step - self:GetSpecialValueFor("projectile_cone") * 0.5, 0), self:GetCaster():GetAbsOrigin() + main_direction * 50)

			local projectile = {
				Ability				= self,
				EffectName			= "particles/frostivus_herofx/queen_shadow_strike_linear.vpcf",
				vSpawnOrigin		= caster_loc + main_direction * 50 + Vector(0, 0, 50),
				fDistance			= self:GetCastRange(self:GetCaster():GetAbsOrigin(), self:GetCaster()),
				fStartRadius		= self:GetSpecialValueFor("radius"),
				fEndRadius			= self:GetSpecialValueFor("radius"),
				Source				= self:GetCaster(),
				bHasFrontalCone		= false,
				bReplaceExisting	= false,
				iUnitTargetTeam		= DOTA_UNIT_TARGET_TEAM_ENEMY,
--				iUnitTargetFlags	= DOTA_UNIT_TARGET_FLAG_MAGIC_IMMUNE_ENEMIES,
				iUnitTargetType		= DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC,
--				fExpireTime			= ,
				bDeleteOnHit		= true,
				vVelocity			= main_direction * self:GetSpecialValueFor("speed"),
				bProvidesVision		= true,
--				iVisionRadius		= projectile_vision,
				iVisionTeamNumber	= self:GetCaster():GetTeamNumber(),
				UnitTest = function( params, unit )
					if unit:GetTeamNumber() ~= params.Source:GetTeamNumber() then
						return true
					end
				end,
				OnUnitHit = function( params, unit )
					unit:EmitSound("Hero_QueenOfPain.ProjectileImpact")
					unit:AddNewModifier(unit, params.Ability, "modifier_shadow_strike_poison", {duration=self:GetSpecialValueFor("poison_duration")})

					local damageTable = {
						victim = unit,
						attacker = params.Source,
						damage = params.Ability:GetSpecialValueFor("damage"),
						damage_type = DAMAGE_TYPE_PHYSICAL,
						ability = params.Ability
					}

					ApplyDamage(damageTable)
				end,
			}

			projectile.vSpawnOrigin = projectile_directions[i] + Vector(0, 0, 100)
			projectile.vVelocity = (projectile_directions[i] - self:GetCaster():GetAbsOrigin()):Normalized() * self:GetSpecialValueFor("speed")
			projectile.vVelocity.z = 0

			Projectiles:CreateProjectile(projectile)
		end
	end
end

LinkLuaModifier("modifier_shadow_strike_poison", "components/abilities/heroes/queenofpain", LUA_MODIFIER_MOTION_NONE)

-------------------------------------------

modifier_shadow_strike_poison = class({})

function modifier_shadow_strike_poison:IsDebuff() return true end
function modifier_shadow_strike_poison:IsHidden() return false end
function modifier_shadow_strike_poison:IsPurgable() return true end
function modifier_shadow_strike_poison:IsPurgeException() return false end
function modifier_shadow_strike_poison:IsStunDebuff() return false end

-------------------------------------------

function modifier_shadow_strike_poison:OnCreated(params)
	if IsServer() then
		if not self.dagger_pfx then
			self.dagger_pfx = ParticleManager:CreateParticle("particles/units/heroes/hero_queenofpain/queen_shadow_strike_debuff.vpcf", PATTACH_POINT_FOLLOW, self:GetCaster())

			for _, cp in pairs({ 0, 2, 3 }) do
				ParticleManager:SetParticleControlEnt(self.dagger_pfx, cp, self:GetParent(), PATTACH_POINT_FOLLOW, "attach_hitloc", self:GetParent():GetAbsOrigin(), true)
			end

			self:AddParticle(self.dagger_pfx, false, false, 0, true, false)
		end
	end

	-- WARNING: Only works like this if the intervals are made in 0.5 steps!
	self:StartIntervalThink(0.5)
end

function modifier_shadow_strike_poison:OnRefresh(params)
	self:OnCreated(params)
end

function modifier_shadow_strike_poison:OnIntervalThink()
	if IsServer() then
		-- Damage handling
		ApplyDamage({victim = self:GetParent(), attacker = self:GetCaster(), ability = self:GetAbility(), damage = self:GetAbility():GetSpecialValueFor("poison_damage"), damage_type = self:GetAbility():GetAbilityDamageType()})
		SendOverheadEventMessage(nil, OVERHEAD_ALERT_BONUS_POISON_DAMAGE, self:GetParent(), self:GetAbility():GetSpecialValueFor("poison_damage"), nil)
	end
end

function modifier_shadow_strike_poison:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_MOVESPEED_BONUS_PERCENTAGE,
		MODIFIER_PROPERTY_PROVIDES_FOW_POSITION,
--		MODIFIER_PROPERTY_MOVESPEED_BONUS_PERCENTAGE,
	}
end
--[[
function modifier_shadow_strike_poison:GetModifierMoveSpeedBonus_Percentage()
	return self:GetAbility():GetSpecialValueFor("poison_slow") * (-1)
end
--]]
function modifier_shadow_strike_poison:GetModifierProvidesFOWVision()
	return 1
end

LinkLuaModifier("modifier_sonic_wave_lifesteal", "components/abilities/heroes/queenofpain", LUA_MODIFIER_MOTION_NONE)

sonic_wave = class({})

function sonic_wave:IsRefreshable() return true end

function sonic_wave:GetIntrinsicModifierName()
	return "modifier_sonic_wave_lifesteal"
end

function sonic_wave:OnSpellStart()
	if IsServer() then
		local target_loc = self:GetCursorPosition()
		self.current_duration = 0.0

		-- Parameters
		local damage = self:GetSpecialValueFor("damage")
		local start_radius = self:GetSpecialValueFor("start_radius")
		local end_radius = self:GetSpecialValueFor("end_radius")
		local travel_distance = self:GetSpecialValueFor("travel_distance")
		local projectile_speed = self:GetSpecialValueFor("projectile_speed")
		local direction
		self.current_radius = start_radius
		self.cast_position = self:GetCaster():GetAbsOrigin()

		if target_loc == self.cast_position then
			direction = self:GetCaster():GetForwardVector()
		else
			direction = (target_loc - self.cast_position):Normalized()
		end

		local projectile =
		{
			Ability				= self,
			EffectName			= "particles/units/heroes/hero_queenofpain/queen_sonic_wave.vpcf",
			vSpawnOrigin		= self.cast_position,
			fDistance			= travel_distance,
			fStartRadius		= start_radius,
			fEndRadius			= end_radius,
			Source				= self:GetCaster(),
			bHasFrontalCone		= true,
			bReplaceExisting	= false,
			iUnitTargetTeam		= self:GetAbilityTargetTeam(),
			iUnitTargetFlags	= self:GetAbilityTargetFlags(),
			iUnitTargetType		= self:GetAbilityTargetType(),
			fExpireTime 		= GameRules:GetGameTime() + 10.0,
			bDeleteOnHit		= true,
			vVelocity			= Vector(direction.x,direction.y,0) * projectile_speed,
			bProvidesVision		= false,
			ExtraData			= {damage = damage}
		}

		ProjectileManager:CreateLinearProjectile(projectile)
		self:GetCaster():EmitSound("Hero_QueenOfPain.SonicWave")
	end
end

function sonic_wave:OnProjectileThink_ExtraData(vLocation, ExtraData)
	self.current_duration = self.current_duration + FrameTime()

	local radius_increase = self:GetSpecialValueFor("end_radius") / self:GetSpecialValueFor("start_radius") * 100 * FrameTime() - 3.333333
	self.current_radius = self.current_radius + radius_increase

	if GridNav:IsNearbyTree(vLocation, self.current_radius, true) then
		GridNav:DestroyTreesAroundPoint(vLocation, self.current_radius, true)
	end
end

function sonic_wave:OnProjectileHit_ExtraData(target, location, ExtraData)
	if target then
		ApplyDamage({attacker = self:GetCaster(), victim = target, ability = self, damage = ExtraData.damage, damage_type = self:GetAbilityDamageType()})

		if target:IsAlive() == false then
			if (math.random(1,100) <= 15) and (self:GetCaster():GetName() == "npc_dota_hero_queenofpain") then
				self:GetCaster():EmitSound("queenofpain_pain_ability_sonicwave_0"..math.random(1,4))
			end
		else
			local duration = self:GetSpecialValueFor("projectile_speed") / self:GetSpecialValueFor("travel_distance")
			local distance_traveled = (self.cast_position - location):Length()
			local remaining_distance = self:GetSpecialValueFor("travel_distance") - distance_traveled

			-- Knockback enemies up and towards the target point
			local knockbackProperties =
			{
				center_x = self:GetCaster():GetAbsOrigin().x,
				center_y = self:GetCaster():GetAbsOrigin().y,
				center_z = self:GetCaster():GetAbsOrigin().z,
				duration = duration - self.current_duration,
				knockback_duration = duration - self.current_duration,
				knockback_distance = remaining_distance,
				knockback_height = 0
			}

			target:RemoveModifierByName("modifier_knockback")
			target:AddNewModifier(target, nil, "modifier_knockback", knockbackProperties)
		end
	end
end

-------------------------------------------
modifier_sonic_wave_lifesteal = class({})
function modifier_sonic_wave_lifesteal:IsDebuff() return false end
function modifier_sonic_wave_lifesteal:IsHidden() return true end
function modifier_sonic_wave_lifesteal:IsPurgable() return false end
function modifier_sonic_wave_lifesteal:IsPurgeException() return false end
function modifier_sonic_wave_lifesteal:IsStunDebuff() return false end
-------------------------------------------

function modifier_sonic_wave_lifesteal:DeclareFunctions()
	local decFuns =
	{
		MODIFIER_EVENT_ON_TAKEDAMAGE
	}

	return decFuns
end

function modifier_sonic_wave_lifesteal:OnTakeDamage( params )
	if IsServer() then
		if not self:GetParent():HasScepter() then return end

		if params.attacker == self:GetParent() and params.inflictor then
			if params.inflictor == self:GetAbility() then
				local lifesteal_amount = self:GetAbility():GetSpecialValueFor("lifesteal_pct")
				print("Lifesteal:", lifesteal_amount)

				-- Particle effect
				local lifesteal_pfx = ParticleManager:CreateParticle("particles/hero/queenofpain/lifesteal.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent())
				ParticleManager:SetParticleControl(lifesteal_pfx, 0, params.unit:GetAbsOrigin())
				ParticleManager:SetParticleControlEnt(lifesteal_pfx, 1, self:GetParent(), PATTACH_POINT_FOLLOW, "attach_hitloc", self:GetParent():GetAbsOrigin(), true)
				ParticleManager:SetParticleControl(lifesteal_pfx, 2, params.unit:GetAbsOrigin())
				ParticleManager:SetParticleControl(lifesteal_pfx, 3, Vector((CalculateDistance(params.unit,self:GetParent()) / 100), 0, 0))
				ParticleManager:ReleaseParticleIndex(lifesteal_pfx)

				local self_lifesteal_pfx = ParticleManager:CreateParticle("particles/hero/queenofpain/self_lifesteal.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent())
				ParticleManager:SetParticleControlEnt(self_lifesteal_pfx, 0, self:GetParent(), PATTACH_POINT_FOLLOW, "attach_hitloc", self:GetParent():GetAbsOrigin(), true)

				-- If the target is a real hero, heal for the full value
				if params.unit:IsRealHero() then
					self:GetParent():Heal(params.damage * lifesteal_amount / 100, self:GetParent())
					-- else, heal for half of it
--				else
--					self:GetParent():Heal(params.damage * lifesteal_amount / 200, self:GetParent())
				end
			end
		end
	end
end

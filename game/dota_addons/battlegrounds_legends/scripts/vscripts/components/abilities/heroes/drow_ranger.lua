multishot = multishot or class({})

function multishot:OnSpellStart()
	-- Parameters
	if IsServer() then
		local projectile_directions = {}

		self:GetCaster():EmitSound("Hero_DrowRanger.FrostArrows")

		-- Determine projectile geometry
		for i = 1, self:GetSpecialValueFor("projectile_count") do
			local caster_loc = self:GetCaster():GetAbsOrigin()
			local main_direction = (self:GetCursorPosition() - self:GetCaster():GetAbsOrigin()):Normalized()
			local angle_step = self:GetSpecialValueFor("projectile_cone") / (self:GetSpecialValueFor("projectile_count") - 1)

			projectile_directions[i] = RotatePosition(self:GetCaster():GetAbsOrigin(), QAngle(0, (i - 1) * angle_step - self:GetSpecialValueFor("projectile_cone") * 0.5, 0), self:GetCaster():GetAbsOrigin() + main_direction * 50)

			local projectile = {
				Ability				= self,
				EffectName			= "particles/frostivus_herofx/drow_linear_arrow.vpcf",
				vSpawnOrigin		= caster_loc + main_direction * 50 + Vector(0, 0, 50),
				fDistance			= self:GetCastRange(self:GetCaster():GetAbsOrigin(), self:GetCaster()),
				fStartRadius		= self:GetSpecialValueFor("radius"),
				fEndRadius			= self:GetSpecialValueFor("radius"),
				Source				= self:GetCaster(),
				bHasFrontalCone		= false,
				bReplaceExisting	= false,
				iUnitTargetTeam		= DOTA_UNIT_TARGET_TEAM_ENEMY,
--				iUnitTargetFlags	= DOTA_UNIT_TARGET_FLAG_MAGIC_IMMUNE_ENEMIES,
				iUnitTargetType		= DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC,
--				fExpireTime			= ,
				bDeleteOnHit		= true,
				vVelocity			= main_direction * self:GetSpecialValueFor("speed"),
				bProvidesVision		= true,
--				iVisionRadius		= projectile_vision,
				iVisionTeamNumber	= self:GetCaster():GetTeamNumber(),
				UnitTest = function( params, unit )
					if unit:GetTeamNumber() ~= params.Source:GetTeamNumber() then
						return true
					end
				end,
				OnUnitHit = function( params, unit )
					unit:EmitSound("Hero_DrowRanger.FrostArrows")
					unit:AddNewModifier(unit, params.Ability, "modifier_multishot_slow", {duration=params.Ability:GetSpecialValueFor("duration_slow")})

					local damageTable = {
						victim = unit,
						attacker = params.Source,
						damage = params.Ability:GetSpecialValueFor("damage"),
						damage_type = DAMAGE_TYPE_PHYSICAL,
						ability = params.Ability
					}

					ApplyDamage(damageTable)
				end,
			}

			projectile.vSpawnOrigin = projectile_directions[i] + Vector(0, 0, 100)
			projectile.vVelocity = (projectile_directions[i] - self:GetCaster():GetAbsOrigin()):Normalized() * self:GetSpecialValueFor("speed")
			projectile.vVelocity.z = 0

			Projectiles:CreateProjectile(projectile)
		end
	end
end

LinkLuaModifier("modifier_multishot_slow", "components/abilities/heroes/drow_ranger", LUA_MODIFIER_MOTION_NONE)

-- Slow modifier
modifier_multishot_slow = class({})

function modifier_multishot_slow:IsHidden() return false end
function modifier_multishot_slow:IsPurgable() return true end
function modifier_multishot_slow:IsDebuff() return true end

function modifier_multishot_slow:OnCreated()
	if IsServer() then
		EmitSoundOn("hero_Crystal.frostbite", self:GetParent())
	end
end

function modifier_multishot_slow:GetTexture()
	return "multishot"
end

function modifier_multishot_slow:GetEffectName()
	return "particles/generic_gameplay/generic_slowed_cold.vpcf"
end

function modifier_multishot_slow:GetEffectAttachType()
	return PATTACH_ABSORIGIN_FOLLOW
end

function modifier_multishot_slow:GetStatusEffectName()
	return "particles/status_fx/status_effect_frost.vpcf"
end

function modifier_multishot_slow:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_MOVESPEED_BONUS_PERCENTAGE,
	}
end

function modifier_multishot_slow:GetModifierMoveSpeedBonus_Percentage()
	return self:GetAbility():GetSpecialValueFor("movement_slow") * (-1)
end

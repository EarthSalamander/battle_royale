bl_windrunner_powershot = bl_windrunner_powershot or class({})

function bl_windrunner_powershot:OnSpellStart()
	if IsServer() then
		local direction = (self:GetCursorPosition() - self:GetCaster():GetAbsOrigin()):Normalized()
		local cast_position = self:GetCaster():GetAbsOrigin()

		self:GetCaster():EmitSound("Hero_Magnataur.ShockWave.Particle.Anvil")

		local projectile = {
			Ability				= self,
			EffectName			= "particles/econ/items/magnataur/shock_of_the_anvil/magnataur_shockanvil.vpcf",
			vSpawnOrigin		= self:GetCaster():GetAbsOrigin() + direction * 50 + Vector(0, 0, 50),
			fDistance			= self:GetCastRange(self:GetCaster():GetAbsOrigin(), self:GetCaster()),
			fStartRadius		= self:GetSpecialValueFor("radius"),
			fEndRadius			= self:GetSpecialValueFor("radius"),
			Source				= self:GetCaster(),
			bHasFrontalCone		= false,
			bReplaceExisting	= false,
			iUnitTargetTeam		= DOTA_UNIT_TARGET_TEAM_ENEMY,
--			iUnitTargetFlags	= DOTA_UNIT_TARGET_FLAG_MAGIC_IMMUNE_ENEMIES,
			iUnitTargetType		= DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC,
--			fExpireTime			= ,
			bDeleteOnHit		= true,
			vVelocity			= direction * self:GetSpecialValueFor("speed"),
			bProvidesVision		= true,
--			iVisionRadius		= projectile_vision,
			iVisionTeamNumber	= self:GetCaster():GetTeamNumber(),
			UnitTest = function( params, unit )
				if unit:GetTeamNumber() ~= params.Source:GetTeamNumber() then
					return true
				end
			end,
			OnUnitHit = function( params, unit )
				local distance =  (cast_position - unit:GetAbsOrigin()):Length2D()
				local damage = self:GetSpecialValueFor("max_damage") / (distance / self:GetCastRange(self:GetCaster():GetAbsOrigin(), self:GetCaster()))

				local damageTable = {
					victim = unit,
					attacker = params.Source,
					damage = damage,
					damage_type = DAMAGE_TYPE_PHYSICAL,
					ability = params.Ability
				}

				ApplyDamage(damageTable)
				unit:EmitSound("Hero_Magnataur.ShockWave.Target")
			end,
		}

		Projectiles:CreateProjectile(projectile)
	end
end

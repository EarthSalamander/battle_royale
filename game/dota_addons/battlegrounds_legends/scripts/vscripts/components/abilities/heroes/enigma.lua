enigma_eidolings = enigma_eidolings or class({})

LinkLuaModifier("modifier_imba_enigma_black_hole_thinker", "components/abilities/heroes/enigma.lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_imba_enigma_black_hole", "components/abilities/heroes/enigma.lua", LUA_MODIFIER_MOTION_NONE)
LinkLuaModifier("modifier_imba_enigma_black_hole_pull", "components/abilities/heroes/enigma.lua", LUA_MODIFIER_MOTION_NONE)

function enigma_eidolings:OnSpellStart()
	if IsServer() then
		local target_loc = self:GetCursorPosition()
		local projectile_cone = 30
		local projectile_count = self:GetSpecialValueFor("projectile_count")
		local projectile_launched = 0

		-- Launch projectiles
		Timers:CreateTimer(function()
			-- Determine projectile geometry
			local caster_loc = self:GetCaster():GetAbsOrigin()
			local main_direction = (target_loc - caster_loc):Normalized()

			projectile_launched = projectile_launched + 1

			local projectile = {
				Ability				= self,
				EffectName			= "particles/enigma_eidolon_attack.vpcf",
				vSpawnOrigin		= caster_loc + main_direction * 50 + Vector(0, 0, 50),
				fDistance			= self:GetCastRange(self:GetCaster():GetAbsOrigin(), self:GetCaster()),
				fStartRadius		= self:GetSpecialValueFor("radius"),
				fEndRadius			= self:GetSpecialValueFor("radius"),
				Source				= self:GetCaster(),
				bHasFrontalCone		= false,
				bReplaceExisting	= false,
				iUnitTargetTeam		= DOTA_UNIT_TARGET_TEAM_ENEMY,
--				iUnitTargetFlags	= DOTA_UNIT_TARGET_FLAG_MAGIC_IMMUNE_ENEMIES,
				iUnitTargetType		= DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC,
--				fExpireTime			= ,
				bDeleteOnHit		= true,
				vVelocity			= main_direction * self:GetSpecialValueFor("speed"),
				bProvidesVision		= true,
--				iVisionRadius		= projectile_vision,
				iVisionTeamNumber	= self:GetCaster():GetTeamNumber(),
				UnitTest = function( params, unit )
					if unit:GetTeamNumber() ~= params.Source:GetTeamNumber() then
						return true
					end
				end,
				OnUnitHit = function( params, unit )
					local damageTable = {
						victim = unit,
						attacker = params.Source,
						damage = params.Ability:GetSpecialValueFor("damage"),
						damage_type = DAMAGE_TYPE_PHYSICAL,
						ability = params.Ability
					}

					ApplyDamage(damageTable)
				end,
			}

			Projectiles:CreateProjectile(projectile)

			if projectile_launched >= projectile_count then
				return nil
			else
				return self:GetSpecialValueFor("projectile_delay")
			end
		end)
	end
end

enigma_pocket_black_hole = enigma_pocket_black_hole or class({})

function enigma_pocket_black_hole:OnSpellStart()
	if IsServer() then
		self.dummy_unit = CreateUnitByName("npc_dummy_unit", self:GetCursorPosition(), false, self:GetCaster(), self:GetCaster(), self:GetCaster():GetTeamNumber())

		local distance = (self:GetCaster():GetAbsOrigin() - self:GetCursorPosition()):Length2D()
		self.delay = self:GetSpecialValueFor("explosion_delay")
		self.radius = self:GetSpecialValueFor("radius")

		-- Base projectile information
		local projectile = {
			Target = self.dummy_unit,
			Source = self:GetCaster(),
			Ability = self,
			bDodgeable = false,
			EffectName = "particles/units/heroes/hero_alchemist/alchemist_unstable_concoction_projectile.vpcf",
			iMoveSpeed = distance / self.delay,
		}

		self:GetCaster():EmitSound("Hero_Alchemist.UnstableConcoction.Throw")
		ProjectileManager:CreateTrackingProjectile(projectile)
	end
end

function enigma_pocket_black_hole:OnProjectileHit(hTarget, vLocation)
	if IsServer() then
		local duration = self:GetSpecialValueFor("duration")

		if hTarget then
			hTarget:EmitSound("Hero_Alchemist.UnstableConcoction.Stun")
			self.thinker = CreateModifierThinker(self:GetCaster(), self, "modifier_imba_enigma_black_hole_thinker", {duration = duration}, vLocation, self:GetCaster():GetTeamNumber(), false)

			-- remove dummy unit
			UTIL_Remove(hTarget)
		end
	end
end

modifier_imba_enigma_black_hole_thinker = modifier_imba_enigma_black_hole_thinker or class({})
modifier_imba_enigma_black_hole = modifier_imba_enigma_black_hole or class({})
modifier_imba_enigma_black_hole_pull = modifier_imba_enigma_black_hole_pull or class({})

function modifier_imba_enigma_black_hole_thinker:IsAura() 					return true end
function modifier_imba_enigma_black_hole_thinker:GetAuraSearchTeam() 		return DOTA_UNIT_TARGET_TEAM_ENEMY end
function modifier_imba_enigma_black_hole_thinker:GetAuraSearchType() 		return DOTA_UNIT_TARGET_BASIC + DOTA_UNIT_TARGET_HERO end
function modifier_imba_enigma_black_hole_thinker:GetAuraSearchFlags()		return DOTA_UNIT_TARGET_FLAG_MAGIC_IMMUNE_ENEMIES end
function modifier_imba_enigma_black_hole_thinker:GetAuraRadius() 			return self.radius end
function modifier_imba_enigma_black_hole_thinker:GetModifierAura()			return "modifier_imba_enigma_black_hole" end

function modifier_imba_enigma_black_hole_thinker:OnCreated(keys)
	if not IsServer() then return end

	self.radius = self:GetAbility().radius
	self.delay = self:GetAbility().delay

	self.sound = "Hero_Enigma.Black_Hole"

	local enemies = FindUnitsInRadius(self:GetCaster():GetTeamNumber(),
		self:GetParent():GetAbsOrigin(),
		nil,
		self.radius,
		DOTA_UNIT_TARGET_TEAM_ENEMY,
		DOTA_UNIT_TARGET_HERO,
		DOTA_UNIT_TARGET_FLAG_MAGIC_IMMUNE_ENEMIES + DOTA_UNIT_TARGET_FLAG_NOT_ILLUSIONS,
		FIND_ANY_ORDER,
		false
	)

	EmitSoundOn(self.sound, self:GetParent())

	local dummy = self:GetParent()

	self:GetParent():SetContextThink("StopBHsound", function()
		StopSoundOn("Hero_Enigma.Black_Hole", dummy)
		StopSoundOn("Imba.EnigmaBlackHoleTi5", dummy)

		return nil
	end, self.delay)

	-- break trees. Because fuck trees.
	GridNav:DestroyTreesAroundPoint(self:GetParent():GetAbsOrigin(), self.radius, false)

	-- Create an FoW viewer for all teams so everyone sees it
--	for i = DOTA_TEAM_FIRST, DOTA_TEAM_CUSTOM_MAX do
--		if i == self:GetParent():GetTeamNumber() then
--			AddFOWViewer(i, self:GetParent():GetAbsOrigin(), self.radius, FrameTime()*2, false)
--		else
--			AddFOWViewer(i, self:GetParent():GetAbsOrigin(), 10, FrameTime()*2, false)
--		end
--	end

	self.particle = ParticleManager:CreateParticle("particles/units/heroes/hero_enigma/enigma_blackhole.vpcf", PATTACH_WORLDORIGIN, nil)
	ParticleManager:SetParticleControl(self.particle, 0, Vector(self:GetParent():GetAbsOrigin().x,self:GetParent():GetAbsOrigin().y,self:GetParent():GetAbsOrigin().z+64))
	ParticleManager:SetParticleControl(self.particle, 10, Vector(self.radius, self.radius, 0))

	--Scepter stuff
	if self:GetCaster():HasScepter() then
		self.scepter = true
	end
end

function modifier_imba_enigma_black_hole_thinker:OnIntervalThink()
	if not IsServer() then return end

	-- Create an FoW viewer for all teams so everyone sees it
--	for i = DOTA_TEAM_FIRST, DOTA_TEAM_CUSTOM_MAX do
--		if i == self:GetParent():GetTeamNumber() then
--			AddFOWViewer(i, self:GetParent():GetAbsOrigin(), self.radius, FrameTime()*2, false)
--		else
--			AddFOWViewer(i, self:GetParent():GetAbsOrigin(), 10, FrameTime()*2, false)
--		end
--	end

	-- Pull effect
	local enemies = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), self:GetParent():GetAbsOrigin(), nil, self.radius, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, DOTA_UNIT_TARGET_FLAG_NONE, FIND_ANY_ORDER, false)

	for _,enemy in pairs(enemies) do
		enemy:AddNewModifier(self:GetCaster(), self:GetAbility(), "modifier_imba_enigma_black_hole_pull", {})
	end

--	GridNav:DestroyTreesAroundPoint(self:GetParent():GetAbsOrigin(), self.radius, false)
end

function modifier_imba_enigma_black_hole_thinker:OnDestroy()
	if not IsServer() then return end

	StopSoundOn(self.sound, self:GetParent())
	EmitSoundOn("Hero_Enigma.Black_Hole.Stop", self:GetParent())
	ParticleManager:DestroyParticle(self.particle, false)
	ParticleManager:ReleaseParticleIndex(self.particle)

	if self.pfx_ulti then
		ParticleManager:DestroyParticle(self.pfx_ulti, false)
		ParticleManager:ReleaseParticleIndex(self.pfx_ulti)
	end

	local damage = self:GetAbility():GetSpecialValueFor("damage")
	local enemies = FindUnitsInRadius(self:GetCaster():GetTeamNumber(), self:GetParent():GetAbsOrigin(), nil, self.radius, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, DOTA_UNIT_TARGET_FLAG_MAGIC_IMMUNE_ENEMIES, FIND_ANY_ORDER, false)

	for _, enemy in pairs(enemies) do
		ApplyDamage({victim = enemy, attacker = self:GetCaster(), damage = damage, damage_type = DAMAGE_TYPE_PURE, ability = self:GetAbility()})
	end

	self:GetParent():ForceKill(false)
end

function modifier_imba_enigma_black_hole:IsDebuff()			return true end
function modifier_imba_enigma_black_hole:IsHidden() 		return false end
function modifier_imba_enigma_black_hole:IsPurgable() 		return false end
function modifier_imba_enigma_black_hole:IsPurgeException() return false end
function modifier_imba_enigma_black_hole:RemoveOnDeath()	return false end
function modifier_imba_enigma_black_hole:IsStunDebuff() 	return true end
function modifier_imba_enigma_black_hole:IsMotionController()  return true end
function modifier_imba_enigma_black_hole:GetMotionControllerPriority()  return DOTA_MOTION_CONTROLLER_PRIORITY_HIGHEST end

function modifier_imba_enigma_black_hole:CheckState()
	if not IsServer() then return end

	return {
		[MODIFIER_STATE_DISARMED] = true,
		[MODIFIER_STATE_ROOTED] = true,
		[MODIFIER_STATE_MUTED] = true,
		[MODIFIER_STATE_STUNNED] = true,
	}
end

function modifier_imba_enigma_black_hole:OnCreated()
	if not IsServer() then return end
	self:StartIntervalThink(FrameTime())
	local ability = self:GetAbility()
	self.radius = self:GetAbility().radius
	self:GetParent():StartGesture(ACT_DOTA_FLAIL)
end

function modifier_imba_enigma_black_hole:OnIntervalThink()
	local enigma = self:GetCaster()
	local ability = self:GetAbility()

	if ability.thinker then
		local distance = CalcDistanceBetweenEntityOBB(ability.thinker, self:GetParent())
		if distance > self.radius then
			self:Destroy()
		end
	end

	self:HorizontalMotion(self:GetParent(), FrameTime())
end

function modifier_imba_enigma_black_hole:HorizontalMotion(unit, time)
	local thinker = self:GetAbility().thinker
	local pos = unit:GetAbsOrigin()
	if thinker and not thinker:IsNull() then
		local thinker_pos = thinker:GetAbsOrigin()
		local next_pos = GetGroundPosition(RotatePosition(thinker_pos, QAngle(0,0.5,0), pos), unit)
		local distance = CalcDistanceBetweenEntityOBB(unit, thinker)

		if distance > 20 then
			next_pos = GetGroundPosition((next_pos + (thinker_pos - next_pos):Normalized() * 1), unit)
		end

		unit:SetAbsOrigin(next_pos)
	end
end

function modifier_imba_enigma_black_hole:OnDestroy()
	if not IsServer() then return end

	self:GetParent():FadeGesture(ACT_DOTA_FLAIL)
	ResolveNPCPositions(self:GetParent():GetAbsOrigin(), 128)
end

function modifier_imba_enigma_black_hole_pull:IsDebuff()		return true end
function modifier_imba_enigma_black_hole_pull:IsHidden() 		return true end
function modifier_imba_enigma_black_hole_pull:IsPurgable() 		return false end
function modifier_imba_enigma_black_hole_pull:IsPurgeException() return false end
function modifier_imba_enigma_black_hole_pull:RemoveOnDeath()	return false end
function modifier_imba_enigma_black_hole_pull:IsStunDebuff() 	return true end
function modifier_imba_enigma_black_hole_pull:IsMotionController()  return true end
function modifier_imba_enigma_black_hole_pull:GetMotionControllerPriority()  return DOTA_MOTION_CONTROLLER_PRIORITY_LOWEST end

function modifier_imba_enigma_black_hole_pull:OnCreated()
	if not IsServer() then return end
	if self:GetParent():IsRoshan() then self:Destroy() end  --Roshan is immune to Black Hole
	self:StartIntervalThink(FrameTime())
	local ability = self:GetAbility()
	self.radius = self:GetAbility().radius
	self.base_pull_distance = ability:GetSpecialValueFor("pull_strength")
end

function modifier_imba_enigma_black_hole_pull:OnIntervalThink()
	local enigma = self:GetCaster()
	local ability = self:GetAbility()

	if ability.thinker then
		local distance = CalcDistanceBetweenEntityOBB(ability.thinker, self:GetParent())
		if distance > self.radius then
			self:Destroy()
		end
	end
	self:HorizontalMotion(self:GetParent(), FrameTime())
end

function modifier_imba_enigma_black_hole_pull:HorizontalMotion(unit, time)
	self.pull_distance =  CalculatePullLength(self:GetCaster(), self:GetParent(), self.base_pull_distance) / (1.0 / FrameTime())
	local thinker = self:GetAbility().thinker
	local pos = unit:GetAbsOrigin()
	if thinker and not thinker:IsNull() and not self:GetParent():HasModifier("modifier_imba_enigma_black_hole") then
		local thinker_pos = thinker:GetAbsOrigin()
		local next_pos = GetGroundPosition((pos + (thinker_pos - pos):Normalized() * self.pull_distance), unit)
		unit:SetAbsOrigin(next_pos)
	end
end

function modifier_imba_enigma_black_hole_pull:OnDestroy()
	if not IsServer() then return end
	ResolveNPCPositions(self:GetParent():GetAbsOrigin(), 128)
end
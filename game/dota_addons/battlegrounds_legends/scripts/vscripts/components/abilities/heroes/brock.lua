brock_rockin_rocket = brock_rockin_rocket or class({})

function brock_rockin_rocket:OnSpellStart()
	local direction = (self:GetCursorPosition() - self:GetCaster():GetAbsOrigin()):Normalized()

	self:GetCaster():EmitSound("Hero_Rattletrap.Rocket_Flare.Fire")
	-- this sound must be cast on a dummy unit to move!
--	self:GetCaster():EmitSound("Hero_Rattletrap.Rocket_Flare.Travel")

	self.projectile =
	{
		Ability				= self,
		EffectName			= "particles/units/heroes/hero_magnataur/magnataur_shockwave.vpcf",
		vSpawnOrigin		= self:GetCaster():GetAbsOrigin(),
		fDistance			= self:GetCastRange(self:GetCaster():GetAbsOrigin(), self:GetCaster()),
		fStartRadius		= self:GetSpecialValueFor("radius"),
		fEndRadius			= self:GetSpecialValueFor("radius"),
		Source				= self:GetCaster(),
		bHasFrontalCone		= false,
		bReplaceExisting	= false,
		iUnitTargetTeam		= DOTA_UNIT_TARGET_TEAM_ENEMY,
		iUnitTargetType		= DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC,
--		fExpireTime 		= GameRules:GetGameTime() + 5.0,
		bDeleteOnHit		= true,
		vVelocity			= Vector(direction.x,direction.y,0) * self:GetSpecialValueFor("speed"),
		bProvidesVision		= false,
	}

	ProjectileManager:CreateLinearProjectile(self.projectile)
end

function brock_rockin_rocket:OnProjectileHit(hTarget, vLocation)
	if IsClient() then return end

	-- this sound must be cast on a dummy unit to move!
--	self:GetCaster():StopSound("Hero_Rattletrap.Rocket_Flare.Travel")

	if hTarget then
		EmitSoundOnLocationWithCaster(vLocation, "Hero_Rattletrap.Rocket_Flare.Explode", self:GetCaster())
	end

	local enemies = FindUnitsInRadius(
		self:GetCaster():GetTeamNumber(),
		vLocation,
		nil,
		self:GetSpecialValueFor("radius"),
		DOTA_UNIT_TARGET_TEAM_ENEMY,
		DOTA_UNIT_TARGET_HERO,
		DOTA_UNIT_TARGET_FLAG_NONE,
		FIND_ANY_ORDER,
		false
	)

	for _, enemy in pairs(enemies) do
		ApplyDamage({victim = enemy, attacker = self:GetCaster(), ability = self, damage = self:GetSpecialValueFor("damage"), damage_type = self:GetAbilityDamageType()})
	end
end

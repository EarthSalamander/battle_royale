LinkLuaModifier("modifier_generic_second_wind", "components/abilities/generic.lua", LUA_MODIFIER_MOTION_NONE)

generic_second_wind = generic_second_wind or class({})

function generic_second_wind:GetIntrinsicModifierName()
	return "modifier_generic_second_wind"
end

modifier_generic_second_wind = modifier_generic_second_wind or class({})

function modifier_generic_second_wind:IsDebuff() return false end
function modifier_generic_second_wind:IsPurgable() return false end
function modifier_generic_second_wind:IsPurgeException() return false end
function modifier_generic_second_wind:IsHidden() return true end

function modifier_generic_second_wind:DeclareFunctions()
	return {
		MODIFIER_EVENT_ON_ABILITY_FULLY_CAST,
		MODIFIER_EVENT_ON_TAKEDAMAGE,
	}
end

function modifier_generic_second_wind:OnCreated()
	if IsClient() then return end

	self:StartIntervalThink(self:GetAbility():GetSpecialValueFor("heal_interval"))
end

function modifier_generic_second_wind:OnIntervalThink()
	if IsClient() then return end

	if self:GetAbility():IsCooldownReady() and self:GetCaster():IsAlive() and self:GetCaster():GetHealth() ~= self:GetCaster():GetMaxHealth() then
		local heal = self:GetCaster():GetMaxHealth() / 100 * self:GetAbility():GetSpecialValueFor("heal_regen_pct")

		if self:GetCaster():GetHealth() + heal > self:GetCaster():GetMaxHealth() then
			SendOverheadEventMessage(nil, OVERHEAD_ALERT_HEAL, self:GetCaster(), self:GetCaster():GetMaxHealth() - self:GetCaster():GetHealth(), nil)
		else
			SendOverheadEventMessage(nil, OVERHEAD_ALERT_HEAL, self:GetCaster(), heal, nil)
		end

		self:GetCaster():Heal(heal, self:GetCaster())
		self:GetCaster():EmitSound("n_creep_ForestTrollHighPriest.Heal")
	end
end

function modifier_generic_second_wind:OnTakeDamage(keys)
	if IsClient() then return end

	if keys.unit == self:GetCaster() then
--		if attacker == keys.unit then
--			-- don't trigger cd with self damage
--			return
--		end

		self:GetAbility():StartCooldown(self:GetAbility():GetCooldown(1))

		-- reset interval think 0.1 sec after being damaged so you wait exactly the cooldown time to get healed
		Timers:CreateTimer(0.1, function()
			self:OnIntervalThink()
		end)
	end
end

function modifier_generic_second_wind:OnAbilityFullyCast(keys)
	if IsClient() then return end

	if keys.unit == self:GetCaster() then
		local pickup_items = {
			"item_power_cube",
			"item_chemical_rune",
		}

		if keys.ability:IsItem() then
			for _, ability_name in pairs(pickup_items) do
				if keys.ability:GetAbilityName() == ability_name then
					return
				end
			end
		end

		self:GetAbility():StartCooldown(self:GetAbility():GetCooldown(1))

		-- reset interval think 0.1 sec after being damaged so you wait exactly the cooldown time to get healed
		Timers:CreateTimer(0.1, function()
			self:OnIntervalThink()
		end)
	end
end

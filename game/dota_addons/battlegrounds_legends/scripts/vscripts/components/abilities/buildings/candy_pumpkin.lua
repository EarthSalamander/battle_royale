LinkLuaModifier("modifier_candy_pumpkin", "components/abilities/buildings/candy_pumpkin.lua", LUA_MODIFIER_MOTION_NONE)

candy_pumpkin = candy_pumpkin or class({})

function candy_pumpkin:GetIntrinsicModifierName()
	return "modifier_candy_pumpkin"
end

modifier_candy_pumpkin = modifier_candy_pumpkin or class({})

function modifier_candy_pumpkin:IsDebuff() return false end
function modifier_candy_pumpkin:IsPurgable() return false end
function modifier_candy_pumpkin:IsPurgeException() return false end
function modifier_candy_pumpkin:IsHidden() return true end

function modifier_candy_pumpkin:DeclareFunctions()
	return {
--		MODIFIER_EVENT_ON_TAKEDAMAGE,
	}
end

function modifier_candy_pumpkin:OnCreated()
	if IsClient() then return end

	self:GetParent():AddNewModifier(self:GetParent(), nil, "modifier_invulnerable", {})
--	self.hit_count = 0
end
--[[
function modifier_candy_pumpkin:OnTakeDamage(keys)
	if IsClient() then return end

	if keys.unit == self:GetCaster() then
		self.hit_count = self.hit_count + 1

		if self.hit_count >= self:GetAbility():GetSpecialValueFor("damage_instance_to_drop") then
			self.hit_count = 0

			local item = CreateItem("item_diretide_candy", nil, nil)
			CreateItemOnPositionSync(pos, item)
			local random_pos = pos + RandomVector(RandomInt(150, 300))
			item:LaunchLoot(false, 300, 0.5, random_pos)
			item:EmitSound("Item.DropGemWorld")

--			local find_trees = GridNav:GetAllTreesAroundPoint(random_pos, 100, true)

--			for _, tree in pairs(find_trees) do
--				tree:CutDownRegrowAfter(99999, -1)
--			end
		end
	end
end
--]]

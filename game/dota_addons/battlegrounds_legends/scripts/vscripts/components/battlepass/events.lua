ListenToGameEvent('game_rules_state_change', function(keys)
	if GameRules:State_Get() == DOTA_GAMERULES_STATE_CUSTOM_GAME_SETUP then
		GetPlayerInfoXP() -- Add a class later
		Battlepass:Init()
	end
end, nil)

ListenToGameEvent('npc_spawned', function(event)
	local npc = EntIndexToHScript( event.entindex )

	if npc:IsRealHero() and npc:GetUnitName() ~= "npc_dota_hero_wisp" then
		Battlepass:AddItemEffects(npc)
	end
end, nil)

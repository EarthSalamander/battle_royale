BAREBONES_VERSION = "1.00"
BAREBONES_DEBUG_SPEW = false

if GameMode == nil then
	DebugPrint( '[BAREBONES] creating barebones game mode' )
	_G.GameMode = class({})
end

require('libraries/timers')
require('libraries/notifications')
require('libraries/animations')
require('libraries/player')
require('libraries/projectiles')
require('libraries/projectiles_new')
require('libraries/rgb_to_hex')

require('internal/gamemode')
require('internal/events')

require('settings')
require('events')

require('components/api/init')
require('components/loading_screen/init')
require('components/battlepass/battlepass')
require('components/battlepass/donator')
require('components/battlepass/experience')
require('components/battlepass/events')

if GetMapName() == "gem_grab" then
	require('components/gem_grab/init')
elseif GetMapName() == "battle_royale" then
	require('components/battle_royale/init')
elseif GetMapName() == "boss_fight" then
	require('components/boss_fight/init')
end

function GameMode:PostLoadPrecache()
	DebugPrint("[BAREBONES] Performing Post-Load precache")    

--	PrecacheItemByNameAsync("item_example_item", function(...) end)

--	PrecacheUnitByNameAsync("npc_dota_hero_viper", function(...) end)
end

function GameMode:OnFirstPlayerLoaded()
	DebugPrint("[BAREBONES] First Player has loaded")
end

function GameMode:OnAllPlayersLoaded()
	DebugPrint("[BAREBONES] All Players have loaded into the game")
end

local hero_in_game_count = 0

function GameMode:OnHeroInGame(hero)
	DebugPrint("[BAREBONES] Hero spawned in game for first time -- " .. hero:GetUnitName())

	hero_in_game_count = hero_in_game_count + 1

	if GameRules:State_Get() < DOTA_GAMERULES_STATE_GAME_IN_PROGRESS then
		hero:AddNewModifier(hero, nil, "modifier_prevent_attack", {})

		if GetMapName() == "battle_royale" then
			if hero_in_game_count == PlayerResource:GetPlayerCount() then
				Timers:CreateTimer(0.1, function()
					BattleRoyale:SpawnHeroes()
				end)
			end
		end
	end

	-- needed with vanilla pick screen and ulti charger not being custom yet
	if hero:GetGold() < 0 then
		hero:ModifyGold(hero:GetGold() * (-1), true, 0)
	end

	-- setup health based on hero level
	if HEROES_HEALTH[hero:GetUnitName()] then
		Timers:CreateTimer(function()
			local health = hero:GetMaxHealth()
			local bonus_health = HEROES_HEALTH[hero:GetUnitName()] * (hero:GetLevel() - 1)

--			print("Set hero health:", hero:GetMaxHealth() + bonus_health)
			hero:SetBaseMaxHealth(hero:GetMaxHealth() + bonus_health)
			hero:SetMaxHealth(hero:GetMaxHealth() + bonus_health)
			hero:SetHealth(hero:GetMaxHealth() + bonus_health)

--			print(hero:GetMaxHealth(), health + bonus_health)
			if hero:GetMaxHealth() ~= health + bonus_health then
				return 0.1
			else
				return nil
			end
		end)
	end

	hero:SetAbilityPoints(0)

	for i = 0, 24 - 1 do
		local ability = hero:GetAbilityByIndex(i)

		if ability and ability:GetLevel() == 0 and ability:GetAbilityName() ~= "generic_hidden" then
			ability:SetLevel(1)
		end
	end
end

function GameMode:InitGameMode()
	GameMode = self
	DebugPrint('[BAREBONES] Starting to load Barebones gamemode...')

	-- insert code below
	GameRules:SetSameHeroSelectionEnabled(true)
	GameRules:SetStrategyTime(0.0)
	GameRules:SetShowcaseTime(0.0)

	local mode = GameRules:GetGameModeEntity()

	mode:SetExecuteOrderFilter(Dynamic_Wrap(GameMode, "FilterExecuteOrder"), self)

	-- Item added to inventory filter
	mode:SetItemAddedToInventoryFilter(function(ctx, event)
		local unit = EntIndexToHScript(event.inventory_parent_entindex_const)
		if unit == nil then return end
		if unit:IsRealHero() then if unit:GetPlayerID() == -1 then return end end
		local item = EntIndexToHScript(event.item_entindex_const)

		-- Remove tp scroll given at the beginning of the game
		if item:GetAbilityName() == "item_tpscroll" and item:GetPurchaser() == nil then
			return false
		end

    	return true
	end, self)

	DebugPrint('[BAREBONES] Done loading Barebones gamemode!\n\n')
end

function GameMode:GoldFilter(keys)
	-- reason_const		12
	-- reliable			1
	-- player_id_const	0
	-- gold				141

	local hero = PlayerResource:GetPlayer(keys.player_id_const):GetAssignedHero()

--	if hero then
		if keys.reason_const ~= DOTA_ModifyGold_Unspecified then return false end
--	end

	return true
end

function GameMode:FilterExecuteOrder( filterTable )
	--[[
	print("-----------------------------------------")
	for k, v in pairs( filterTable ) do
		print("Order: " .. k .. " " .. tostring(v) )
	end
	]]

	local units = filterTable["units"]
	local order_type = filterTable["order_type"]
	local issuer = filterTable["issuer_player_id_const"]
	local abilityIndex = filterTable["entindex_ability"]
	local targetIndex = filterTable["entindex_target"]
	local x = tonumber(filterTable["position_x"])
	local y = tonumber(filterTable["position_y"])
	local z = tonumber(filterTable["position_z"])
	local point = Vector(x,y,z)
	local queue = filterTable["queue"] == 1

	local unit
	local numUnits = 0
	local numBuildings = 0
	if units then
		-- Skip Prevents order loops
		unit = EntIndexToHScript(units["0"])
		if unit then
			if unit.skip then
				unit.skip = false
				return true
			end
		end
	end

	-- Don't need this.
	if order_type == DOTA_UNIT_ORDER_RADAR or order_type == DOTA_UNIT_ORDER_GLYPH or order_type == DOTA_UNIT_ORDER_PURCHASE_ITEM then return false end

	return true
end

function GameMode:EndGame( victoryTeam )
	local overBoss = Entities:FindByName( nil, "@overboss" )

	if overBoss then
		local celebrate = overBoss:FindAbilityByName( 'dota_ability_celebrate' )
		if celebrate then
			overBoss:CastAbilityNoTarget( celebrate, -1 )
		end
	end

	GameRules:SetGameWinner( victoryTeam )
end

function GameMode:CleanMap()
	local units = FindUnitsInRadius(2, Vector(0, 0, 0), nil, FIND_UNITS_EVERYWHERE, DOTA_UNIT_TARGET_TEAM_BOTH, DOTA_UNIT_TARGET_ALL, DOTA_UNIT_TARGET_FLAG_INVULNERABLE + DOTA_UNIT_TARGET_FLAG_MAGIC_IMMUNE_ENEMIES, FIND_ANY_ORDER, false)

	for _, unit in pairs(units) do
		if unit:GetUnitName() == "npc_dota_lone_druid_bear" or unit:GetUnitName() == "npc_treasure" or  unit:GetUnitName() == "npc_power_crate" or string.find(unit:GetUnitName(), "obstacle") then
			UTIL_Remove(unit)
		end
	end

	for _, entity in pairs(Entities:FindAllByClassname("dota_item_drop")) do
		UTIL_Remove(entity)
	end
end

function GameMode:GetRoundScore(iTeamNumber)
	return CustomNetTables:GetTableValue("game_score", tostring(iTeamNumber)).round_score
end

function GameMode:UpdateRoundScore(iTeamNumber, iAmount)
	if iAmount == 0 then return end

	CustomNetTables:SetTableValue("game_score", tostring(iTeamNumber), {
		round_score = CustomNetTables:GetTableValue("game_score", tostring(iTeamNumber)).round_score + iAmount,
		team_score = CustomNetTables:GetTableValue("game_score", tostring(iTeamNumber)).team_score
	})
end

function GameMode:GetTeamScore(iTeamNumber)
	if GetMapName() == "battle_royale" then
		return CustomNetTables:GetTableValue("game_score", tostring(iTeamNumber)).round_score
	else
		return CustomNetTables:GetTableValue("game_score", tostring(iTeamNumber)).team_score
	end
end

function GameMode:UpdateTeamScore(iTeamNumber, iAmount)
	if iAmount == 0 then return end

	if GetMapName() == "battle_royale" then
		CustomNetTables:SetTableValue("game_score", tostring(iTeamNumber), {
			round_score = CustomNetTables:GetTableValue("game_score", tostring(iTeamNumber)).round_score + iAmount,
			team_score = CustomNetTables:GetTableValue("game_score", tostring(iTeamNumber)).team_score
		})
	else
		CustomNetTables:SetTableValue("game_score", tostring(iTeamNumber), {
			round_score = CustomNetTables:GetTableValue("game_score", tostring(iTeamNumber)).round_score,
			team_score = CustomNetTables:GetTableValue("game_score", tostring(iTeamNumber)).team_score + iAmount
		})
	end
end

-- In this file you can set up all the properties and settings for your game mode.

CUSTOM_GAME_TYPE = "FB"

ENABLE_HERO_RESPAWN = true				-- Should the heroes automatically respawn on a timer or stay dead until manually respawned
ALLOW_SAME_HERO_SELECTION = true		-- Should we let people select the same hero as each other

HERO_SELECTION_TIME = 20.0				-- How long should we let people select their hero?
if IsInToolsMode() then
	PRE_GAME_TIME = 5.0						-- How long after people select their heroes should the horn blow and the game start?
	POST_GAME_TIME = 3600.0
else
	PRE_GAME_TIME = 10.0
	POST_GAME_TIME = 60.0					-- How long after people select their heroes should the horn blow and the game start?
end
TREE_REGROW_TIME = 60.0                 -- How long should it take individual trees to respawn after being cut down/destroyed?

GOLD_PER_TICK = 0                     -- How much gold should players get per tick?
GOLD_TICK_TIME = 0                      -- How long should we wait in seconds between gold ticks?

CAMERA_DISTANCE_OVERRIDE = -1           -- How far out should we allow the camera to go?  Use -1 for the default (1134) while still allowing for panorama camera distance changes

MINIMAP_ICON_SIZE = 1                   -- What icon size should we use for our heroes?
MINIMAP_CREEP_ICON_SIZE = 1             -- What icon size should we use for creeps?
MINIMAP_RUNE_ICON_SIZE = 1              -- What icon size should we use for runes?

RUNE_SPAWN_TIME = 120                   -- How long in seconds should we wait between rune spawns?
CUSTOM_BUYBACK_COST_ENABLED = true      -- Should we use a custom buyback cost setting?
CUSTOM_BUYBACK_COOLDOWN_ENABLED = true  -- Should we use a custom buyback time?
BUYBACK_ENABLED = false                 -- Should we allow people to buyback when they die?

DISABLE_FOG_OF_WAR_ENTIRELY = true     -- Should we disable fog of war entirely for both teams?
USE_UNSEEN_FOG_OF_WAR = false           -- Should we make unseen and fogged areas of the map completely black until uncovered by each team? 
										-- Note: DISABLE_FOG_OF_WAR_ENTIRELY must be false for USE_UNSEEN_FOG_OF_WAR to work
USE_CUSTOM_TOP_BAR_VALUES = true        -- Should we do customized top bar values or use the default kill count per team?
TOP_BAR_VISIBLE = true                  -- Should we display the top bar score/count at all?
SHOW_KILLS_ON_TOPBAR = true             -- Should we display kills only on the top bar? (No denies, suicides, kills by neutrals)  Requires USE_CUSTOM_TOP_BAR_VALUES

ENABLE_TOWER_BACKDOOR_PROTECTION = false-- Should we enable backdoor protection for our towers?
REMOVE_ILLUSIONS_ON_DEATH = false       -- Should we remove all illusions if the main hero dies?
DISABLE_GOLD_SOUNDS = false             -- Should we disable the gold sound when players get gold?

END_GAME_ON_KILLS = false				-- Should the game end after a certain number of kills?
KILLS_TO_END_GAME_FOR_TEAM = 50         -- How many kills for a team should signify an end of game?

USE_CUSTOM_HERO_LEVELS = true           -- Should we allow heroes to have custom levels?
MAX_LEVEL = 1                          -- What level should we let heroes get to?
USE_CUSTOM_XP_VALUES = true             -- Should we use custom XP values to level up heroes, or the default Dota numbers?

-- Fill this table up with the required XP per level if you want to change it
XP_PER_LEVEL_TABLE = {}
XP_PER_LEVEL_TABLE[1] = 0

ENABLE_FIRST_BLOOD = true               -- Should we enable first blood for the first kill in this game?
HIDE_KILL_BANNERS = false               -- Should we hide the kill banners that show when a player is killed?
LOSE_GOLD_ON_DEATH = false               -- Should we have players lose the normal amount of dota gold on death?
SHOW_ONLY_PLAYER_INVENTORY = false      -- Should we only allow players to see their own inventory even when selecting other units?
DISABLE_STASH_PURCHASING = false        -- Should we prevent players from being able to buy items into their stash when not at a shop?
DISABLE_ANNOUNCER = false               -- Should we disable the announcer from working in the game?
FORCE_PICKED_HERO = nil					-- What hero should we force all players to spawn as? (e.g. "npc_dota_hero_axe").  Use nil to allow players to pick their own hero.

FIXED_RESPAWN_TIME = 5.0                 -- What time should we use for a fixed respawn timer?  Use -1 to keep the default dota behavior.
FOUNTAIN_CONSTANT_MANA_REGEN = -1       -- What should we use for the constant fountain mana regen?  Use -1 to keep the default dota behavior.
FOUNTAIN_PERCENTAGE_MANA_REGEN = -1     -- What should we use for the percentage fountain mana regen?  Use -1 to keep the default dota behavior.
FOUNTAIN_PERCENTAGE_HEALTH_REGEN = -1   -- What should we use for the percentage fountain health regen?  Use -1 to keep the default dota behavior.
MAXIMUM_ATTACK_SPEED = 600              -- What should we use for the maximum attack speed?
MINIMUM_ATTACK_SPEED = 20               -- What should we use for the minimum attack speed?

GAME_END_DELAY = -1                     -- How long should we wait after the game winner is set to display the victory banner and End Screen?  Use -1 to keep the default (about 10 seconds)
VICTORY_MESSAGE_DURATION = 3            -- How long should we wait after the victory message displays to show the End Screen?  Use 
STARTING_GOLD = 0						-- How much starting gold should we give to each player?
DISABLE_DAY_NIGHT_CYCLE = true			-- Should we disable the day night cycle from naturally occurring? (Manual adjustment still possible)
DISABLE_KILLING_SPREE_ANNOUNCER = false -- Shuold we disable the killing spree announcer?
DISABLE_STICKY_ITEM = false             -- Should we disable the sticky item button in the quick buy area?
SKIP_TEAM_SETUP = false                 -- Should we skip the team setup entirely?
ENABLE_AUTO_LAUNCH = true               -- Should we automatically have the game complete team setup after AUTO_LAUNCH_DELAY seconds?
AUTO_LAUNCH_DELAY = 5.0					-- How long should the default team selection launch timer be?  The default for custom games is 30.  Setting to 0 will skip team selection.
LOCK_TEAM_SETUP = true					-- Should we lock the teams initially?  Note that the host can still unlock the teams 

-- NOTE: You always need at least 2 non-bounty type runes to be able to spawn or your game will crash!
ENABLED_RUNES = {}                      -- Which runes should be enabled to spawn in our game mode?
ENABLED_RUNES[DOTA_RUNE_DOUBLEDAMAGE] = true
ENABLED_RUNES[DOTA_RUNE_HASTE] = true
ENABLED_RUNES[DOTA_RUNE_ILLUSION] = true
ENABLED_RUNES[DOTA_RUNE_INVISIBILITY] = true
ENABLED_RUNES[DOTA_RUNE_REGENERATION] = true
ENABLED_RUNES[DOTA_RUNE_BOUNTY] = true
ENABLED_RUNES[DOTA_RUNE_ARCANE] = true

MAX_NUMBER_OF_TEAMS = 2					-- How many potential teams can be in this game mode?
USE_CUSTOM_TEAM_COLORS = true			-- Should we use custom team colors?
USE_CUSTOM_TEAM_COLORS_FOR_PLAYERS = false          -- Should we use custom team colors to color the players/minimap?

TEAM_COLORS = {}                        -- If USE_CUSTOM_TEAM_COLORS is set, use these colors.
TEAM_COLORS[DOTA_TEAM_GOODGUYS] = { 61, 210, 150 }  --    Teal
TEAM_COLORS[DOTA_TEAM_BADGUYS]  = { 243, 201, 9 }   --    Yellow
TEAM_COLORS[DOTA_TEAM_CUSTOM_1] = { 197, 77, 168 }  --    Pink
TEAM_COLORS[DOTA_TEAM_CUSTOM_2] = { 255, 108, 0 }   --    Orange
TEAM_COLORS[DOTA_TEAM_CUSTOM_3] = { 52, 85, 255 }   --    Blue
TEAM_COLORS[DOTA_TEAM_CUSTOM_4] = { 101, 212, 19 }  --    Green
TEAM_COLORS[DOTA_TEAM_CUSTOM_5] = { 129, 83, 54 }   --    brown
TEAM_COLORS[DOTA_TEAM_CUSTOM_6] = { 27, 192, 216 }  --    Cyan
TEAM_COLORS[DOTA_TEAM_CUSTOM_7] = { 199, 228, 13 }  --    Olive
TEAM_COLORS[DOTA_TEAM_CUSTOM_8] = { 140, 42, 244 }  --    Purple

USE_AUTOMATIC_PLAYERS_PER_TEAM = false   -- Should we set the number of players to 10 / MAX_NUMBER_OF_TEAMS?

CUSTOM_TEAM_PLAYER_COUNT = {}           -- If we're not automatically setting the number of players per team, use this table
CUSTOM_TEAM_PLAYER_COUNT[DOTA_TEAM_CUSTOM_1] = 0
CUSTOM_TEAM_PLAYER_COUNT[DOTA_TEAM_CUSTOM_2] = 0
CUSTOM_TEAM_PLAYER_COUNT[DOTA_TEAM_CUSTOM_3] = 0
CUSTOM_TEAM_PLAYER_COUNT[DOTA_TEAM_CUSTOM_4] = 0
CUSTOM_TEAM_PLAYER_COUNT[DOTA_TEAM_CUSTOM_5] = 0
CUSTOM_TEAM_PLAYER_COUNT[DOTA_TEAM_CUSTOM_6] = 0
CUSTOM_TEAM_PLAYER_COUNT[DOTA_TEAM_CUSTOM_7] = 0
CUSTOM_TEAM_PLAYER_COUNT[DOTA_TEAM_CUSTOM_8] = 0

PLAYER_COLORS = {}															-- Stores individual player colors
PLAYER_COLORS[0] = { 67, 133, 255 }
PLAYER_COLORS[1]  = { 170, 255, 195 }
PLAYER_COLORS[2] = { 130, 0, 150 }
PLAYER_COLORS[3] = { 255, 234, 0 }
PLAYER_COLORS[4] = { 255, 153, 0 }
PLAYER_COLORS[5] = { 190, 255, 0 }
PLAYER_COLORS[6] = { 255, 0, 0 }
PLAYER_COLORS[7] = { 0, 128, 128 }
PLAYER_COLORS[8] = { 255, 250, 200 }
PLAYER_COLORS[9] = { 49, 49, 49 }
PLAYER_COLORS[10] = { 255, 0, 255 }
PLAYER_COLORS[11]  = { 128, 128, 0 }
PLAYER_COLORS[12] = { 100, 255, 255 }
PLAYER_COLORS[13] = { 0, 190, 0 }
PLAYER_COLORS[14] = { 170, 110, 40 }
PLAYER_COLORS[15] = { 0, 0, 128 }
PLAYER_COLORS[16] = { 230, 190, 255 }
PLAYER_COLORS[17] = { 128, 0, 0 }
PLAYER_COLORS[18] = { 144, 144, 144 }
PLAYER_COLORS[19] = { 254, 254, 254 }
PLAYER_COLORS[20] = { 166, 166, 166 }
PLAYER_COLORS[21] = { 255, 89, 255 }
PLAYER_COLORS[22] = { 203, 255, 89 }
PLAYER_COLORS[23] = { 108, 167, 255 }

-- Custom values
GAME_VERSION = 1.0
GAME_WINNER_TEAM = 0 -- Tracks game winner

timers = {}

PLAYER_CAMERA_TARGET_ID = {}
VISION_DUMMIES = {}

PREVENT_TIMER_RUNNING = false
LOOT_SPAWN_RADIUS_MIN = 350
LOOT_SPAWN_RADIUS_MAX = 900

HEROES_HEALTH = {}
--[[
HEROES_HEALTH["npc_dota_hero_queenofpain"] = 180
HEROES_HEALTH["npc_dota_hero_elder_titan"] = 290
HEROES_HEALTH["npc_dota_hero_lone_druid"] = 190
HEROES_HEALTH["npc_dota_hero_rattletrap"] = 140
HEROES_HEALTH["npc_dota_hero_enigma"] = 160
HEROES_HEALTH["npc_dota_hero_pudge"] = 290
--]]

-- fail-safe in case hero value is missing in table below
HEROES_ULTI_LOAD_BASE = 15

HEROES_ULTI_LOAD = {}
HEROES_ULTI_LOAD["npc_dota_hero_enigma"] = 10
HEROES_ULTI_LOAD["npc_dota_hero_lone_druid"] = 20
HEROES_ULTI_LOAD["npc_dota_hero_mars"] = 20
HEROES_ULTI_LOAD["npc_dota_hero_pudge"] = 20
HEROES_ULTI_LOAD["npc_dota_hero_queenofpain"] = 10
HEROES_ULTI_LOAD["npc_dota_hero_rattletrap"] = 20
HEROES_ULTI_LOAD["npc_dota_hero_sniper"] = 5

-- temporary, used in gem grab and boss fight atm
CustomNetTables:SetTableValue("game_score", "2", {round_score = 0, team_score = 0})
CustomNetTables:SetTableValue("game_score", "3", {round_score = 0, team_score = 0})

if GetMapName() == "battle_royale" then
	CustomNetTables:SetTableValue("game_score", "6", {round_score = 0, team_score = 0})
	CustomNetTables:SetTableValue("game_score", "7", {round_score = 0, team_score = 0})
	CustomNetTables:SetTableValue("game_score", "8", {round_score = 0, team_score = 0})
	CustomNetTables:SetTableValue("game_score", "9", {round_score = 0, team_score = 0})
	CustomNetTables:SetTableValue("game_score", "10", {round_score = 0, team_score = 0})
	CustomNetTables:SetTableValue("game_score", "11", {round_score = 0, team_score = 0})
	CustomNetTables:SetTableValue("game_score", "12", {round_score = 0, team_score = 0})
	CustomNetTables:SetTableValue("game_score", "13", {round_score = 0, team_score = 0})

	-- Battle Royale
	MAIN_ITEM_DROP = "item_power_cube"
	BATTLE_ROYALE_SURVIVAL = true
	BATTLE_ROYALE_FOG_SPAWN_DELAY = 10.0
	BATTLE_ROYALE_ARENA_SIZE = 6000
	BATTLE_ROYALE_ARENA_SIZE_REDUCTION = 500
	BATTLE_ROYALE_ARENA_MIN_SIZE = 1000
	BATTLE_ROYALE_USE_CAMERA_LOCKED = false
	BATTLE_ROYALE_FOG_REDUCE_WITH_ARENA_SIZE = true -- DISABLE_FOG_OF_WAR_ENTIRELY must be false when set to true
	BATTLE_ROYALE_ROUND_TO_WIN = 3
	BATTLE_ROYALE_START_ROUND_DELAY = 3.0
	BATTLE_ROYALE_DELAY_ROUND_WON = 3.0
	BATTLE_ROYALE_RESPAWN_TIME = 15.0
	BATTLE_ROYALE_TEAMMATE_RESPAWN = false
	BATTLE_ROYALE_MUTATION = "chemical_rage"

	BATTLE_ROYALE_POWER_CUBE_COUNT_PCT = 50 -- %

	BATTLE_ROYALE_SPAWNERS = {}
	BATTLE_ROYALE_SPAWNERS["battle_royale_obstacle_spawner"] = "npc_battle_royale_obstacle"
	BATTLE_ROYALE_SPAWNERS["battle_royale_power_crate_spawner"] = "npc_power_crate"

	BATTLE_ROYALE_CHEMICAL_RUNE_SPAWNERS = {}
	BATTLE_ROYALE_CHEMICAL_RUNE_SPAWNERS[1] = {0, 0, 0}
	BATTLE_ROYALE_CHEMICAL_RUNE_SPAWNERS[2] = {0, 388.960815, 0}
	BATTLE_ROYALE_CHEMICAL_RUNE_SPAWNERS[3] = {775, -668, 0}
	BATTLE_ROYALE_CHEMICAL_RUNE_SPAWNERS[4] = {1383, -668, 0}
	BATTLE_ROYALE_CHEMICAL_RUNE_SPAWNERS[5] = {1178, -1502, 0}
	BATTLE_ROYALE_CHEMICAL_RUNE_SPAWNERS[6] = {1178, -2158, 0}
	BATTLE_ROYALE_CHEMICAL_RUNE_SPAWNERS[7] = {336, -2550, 0}
	BATTLE_ROYALE_CHEMICAL_RUNE_SPAWNERS[8] = {0, -2500, 0}
	BATTLE_ROYALE_CHEMICAL_RUNE_SPAWNERS[9] = {0, -2000, 0}
	BATTLE_ROYALE_CHEMICAL_RUNE_SPAWNERS[10] = {0, -1327, 0}
	BATTLE_ROYALE_CHEMICAL_RUNE_SPAWNERS[11] = {-411, -547, 0}
	BATTLE_ROYALE_CHEMICAL_RUNE_SPAWNERS[12] = {-1398, -565, 0}

	CustomNetTables:SetTableValue("game_score", "score_to_win", {round_score = 0, team_score = BATTLE_ROYALE_ROUND_TO_WIN})

	BATTLE_ROYALE_MUTATION = {}
	BATTLE_ROYALE_MUTATION["positive"] = "chemical_rage"
	BATTLE_ROYALE_MUTATION["negative"] = "tempest"
	BATTLE_ROYALE_MUTATION["terrain"] = ""

	CustomNetTables:SetTableValue("game_options", "mutation", {BATTLE_ROYALE_MUTATION})

	BATTLE_ROYALE_TREASURES = {
--		"item_ultimate_scepter", -- Every heroes need an Aghanim upgrade on their abilities first
		"item_blink",
		"item_treasure_morbid_mask",
		"item_treasure_scepter_of_mastery",
	}

	nCOUNTDOWNTIMER = BATTLE_ROYALE_FOG_SPAWN_DELAY

	-- Vanilla overidden values
	DISABLE_FOG_OF_WAR_ENTIRELY = false

	MAX_NUMBER_OF_TEAMS = 5
	CUSTOM_TEAM_PLAYER_COUNT[DOTA_TEAM_GOODGUYS] = 2
	CUSTOM_TEAM_PLAYER_COUNT[DOTA_TEAM_BADGUYS]  = 2
	CUSTOM_TEAM_PLAYER_COUNT[DOTA_TEAM_CUSTOM_1] = 2
	CUSTOM_TEAM_PLAYER_COUNT[DOTA_TEAM_CUSTOM_2] = 2
	CUSTOM_TEAM_PLAYER_COUNT[DOTA_TEAM_CUSTOM_3] = 2
elseif GetMapName() == "boss_fight" then
	MAIN_ITEM_DROP = "item_boss_rune"
	CustomNetTables:SetTableValue("game_score", "score_to_win", {round_score = 0, team_score = 0})

	-- Boss Fight
	BEHEMOTH_SPAWN_COUNT = 0
	BEHEMOTH_MAX_SPAWN_COUNT = 5
	BOSS_FIGHT_RUNE_SPAWN_DELAY = 15.0
	BOSS_FIGHT_BASE_TIMER = 120.0

	nCOUNTDOWNTIMER = BOSS_FIGHT_BASE_TIMER

	CUSTOM_TEAM_PLAYER_COUNT[DOTA_TEAM_GOODGUYS] = 3
	CUSTOM_TEAM_PLAYER_COUNT[DOTA_TEAM_BADGUYS]  = 3
elseif GetMapName() == "candy_grab" then
	MAIN_ITEM_DROP = "item_candy"
	CANDY_GRAB_GEM_SPAWN_DELAY = 10.0
elseif GetMapName() == "gem_grab" then
	-- Gem Grab
	MAIN_ITEM_DROP = "item_gem_grab"
	GEM_GRAB_GEM_SPAWN_DELAY = 10.0
	GEM_GRAB_SCORE_TO_WIN = 10
	GEM_GRAB_DELAY_TO_WIN = 15.0
	GEM_GRAB_WINNING_PHASE = false
	GEM_GRAB_DELAY_ROUND_WON = 3.0
	GEM_GRAB_START_ROUND_DELAY = 3.0
	GEM_GRAB_TEAM_SCORE_TO_WIN = 5

	CustomNetTables:SetTableValue("game_score", "score_to_win", {round_score = GEM_GRAB_SCORE_TO_WIN, team_score = GEM_GRAB_TEAM_SCORE_TO_WIN})

	-- Vanilla overidden values
	CUSTOM_TEAM_PLAYER_COUNT[DOTA_TEAM_GOODGUYS] = 3
	CUSTOM_TEAM_PLAYER_COUNT[DOTA_TEAM_BADGUYS]  = 3
end

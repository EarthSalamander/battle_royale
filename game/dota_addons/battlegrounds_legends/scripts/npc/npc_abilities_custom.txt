"DOTAAbilities"
{
	"Version"	"1"

	"dummy_unit_state"
	{
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_HIDDEN"
		"BaseClass" 					"ability_datadriven"
		"AbilityTextureName"			"rubick_empty1"
		"MaxLevel"						"1"

		"Modifiers"
		{
			"modifier_dummy_unit"
			{
				"Passive"                        "1"
				"IsHidden"                       "1"
				"States"
				{
					"MODIFIER_STATE_UNSELECTABLE"		"MODIFIER_STATE_VALUE_ENABLED"
					"MODIFIER_STATE_INVULNERABLE"		"MODIFIER_STATE_VALUE_ENABLED"
					"MODIFIER_STATE_NOT_ON_MINIMAP"		"MODIFIER_STATE_VALUE_ENABLED"
					"MODIFIER_STATE_NO_HEALTH_BAR"		"MODIFIER_STATE_VALUE_ENABLED"
					"MODIFIER_STATE_NO_UNIT_COLLISION"	"MODIFIER_STATE_VALUE_ENABLED"
					"MODIFIER_STATE_OUT_OF_GAME"		"MODIFIER_STATE_VALUE_ENABLED"
				}
			}
		}
	}

	"dota_ability_throw_coin"
	{
		"BaseClass"							"ability_datadriven"
		"AbilityName"						"dota_ability_throw_coin"
		"AbilityBehavior"					"DOTA_ABILITY_BEHAVIOR_NO_TARGET"
		"AbilityCastAnimation"				"ACT_DOTA_CAST_ABILITY_1"
		"AbilityCastPoint"					"1.5"

		"OnSpellStart"
		{
			"RunScript"
			{
				"ScriptFile"			"components/overboss.lua"
				"Function"				"ThrowCoin"
			}
		}

		"Modifiers"
		{
			"modifier_dota_ability_throw_coin"
			{
				"IsHidden"				"1"
				"Passive"				"1"
				
				"States"
				{
					"MODIFIER_STATE_UNSELECTABLE"	"MODIFIER_STATE_VALUE_ENABLED"
					"MODIFIER_STATE_NO_HEALTH_BAR"	"MODIFIER_STATE_VALUE_ENABLED"
					"MODIFIER_STATE_INVULNERABLE"	"MODIFIER_STATE_VALUE_ENABLED"
					"MODIFIER_STATE_OUT_OF_GAME"	"MODIFIER_STATE_VALUE_ENABLED"
				}
			}
		}
	}

	"dota_ability_throw_coin_long"
	{
		"BaseClass"							"ability_datadriven"
		"AbilityName"						"dota_ability_throw_coin_long"
		"AbilityBehavior"					"DOTA_ABILITY_BEHAVIOR_NO_TARGET"
		"AbilityCastAnimation"				"ACT_DOTA_CAST_ABILITY_2"
		"AbilityCastPoint"					"2.5"

		"OnSpellStart"
		{
			"RunScript"
			{
				"ScriptFile"			"components/overboss.lua"
				"Function"				"ThrowCoin"
			}
		}

		"Modifiers"
		{
			"modifier_dota_ability_throw_coin"
			{
				"IsHidden"				"1"
				"Passive"				"1"
				
				"States"
				{
					"MODIFIER_STATE_UNSELECTABLE"	"MODIFIER_STATE_VALUE_ENABLED"
					"MODIFIER_STATE_NO_HEALTH_BAR"	"MODIFIER_STATE_VALUE_ENABLED"
					"MODIFIER_STATE_INVULNERABLE"	"MODIFIER_STATE_VALUE_ENABLED"
					"MODIFIER_STATE_OUT_OF_GAME"	"MODIFIER_STATE_VALUE_ENABLED"
				}
			}
		}
	}

	"dota_ability_celebrate"
	{
		"BaseClass"							"ability_datadriven"
		"AbilityName"						"dota_ability_celebrate"
		"AbilityBehavior"					"DOTA_ABILITY_BEHAVIOR_NO_TARGET"
		"AbilityCastAnimation"				"ACT_DOTA_CAST_ABILITY_3"

		"Modifiers"
		{
			"modifier_dota_ability_celebrate"
			{
				"IsHidden"				"1"
				"Passive"				"1"

				"States"
				{
					"MODIFIER_STATE_UNSELECTABLE"	"MODIFIER_STATE_VALUE_ENABLED"
					"MODIFIER_STATE_NO_HEALTH_BAR"	"MODIFIER_STATE_VALUE_ENABLED"
					"MODIFIER_STATE_INVULNERABLE"	"MODIFIER_STATE_VALUE_ENABLED"
					"MODIFIER_STATE_OUT_OF_GAME"	"MODIFIER_STATE_VALUE_ENABLED"
				}
			}
		}
	}

	//=================================================================================================================
	// Ability: Dummies true sight
	//=================================================================================================================
	"dummy_true_sight"
	{
		// General
		//-------------------------------------------------------------------------------------------------------------
		"BaseClass"						"necronomicon_warrior_sight"
		"AbilityType"					"DOTA_ABILITY_TYPE_BASIC"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE"
		
		// Special
		//-------------------------------------------------------------------------------------------------------------
		"AbilitySpecial"
		{
			"01"
			{
				"var_type"				"FIELD_INTEGER"
				"radius"				"100000"
			}
		}
	}

	//=================================================================================================================
	// Pudge's Meat Hook
	//=================================================================================================================
	"candy_pumpkin"
	{
		// General
		//-------------------------------------------------------------------------------------------------------------
		"BaseClass"						"ability_lua"
		"ScriptFile"					"components/buildings/candy_pumpkin"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE"
		"MaxLevel"						"1"

		// Special
		//-------------------------------------------------------------------------------------------------------------
		"AbilitySpecial"
		{
			"01"
			{
				"var_type"					"FIELD_INTEGER"
				"damage_instance_to_drop"	"3"
			}
		}
	}

	//=================================================================================================================
	// Queen of Pain: Shadow Strike
	//=================================================================================================================
	"shadow_strike"
	{
		// General
		//-------------------------------------------------------------------------------------------------------------
		"BaseClass"						"ability_lua"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_POINT | DOTA_ABILITY_BEHAVIOR_IMMEDIATE | DOTA_ABILITY_BEHAVIOR_IGNORE_BACKSWING"
		"AbilityUnitTargetTeam"			"DOTA_UNIT_TARGET_TEAM_ENEMY"
		"AbilityUnitTargetType"			"DOTA_UNIT_TARGET_HERO | DOTA_UNIT_TARGET_BASIC"
		"AbilityUnitDamageType"			"DAMAGE_TYPE_PHYSICAL"
		"SpellImmunityType"				"SPELL_IMMUNITY_ENEMIES_NO"
		"FightRecapLevel"				"1"
		"AbilityTextureName"			"queenofpain_shadow_strike"
		"ScriptFile"					"components/abilities/heroes/queenofpain"
		"MaxLevel"						"1"

		// Precache
		//-------------------------------------------------------------------------------------------------------------
		"precache"
		{
			"soundfile"					"soundevents/game_sounds_heroes/game_sounds_queenofpain.vsndevts"
			"particle"					"particles/frostivus_herofx/queen_shadow_strike_linear.vpcf"
			"particle"					"particles/units/heroes/hero_queenofpain/queen_shadow_strike_debuff.vpcf"
		}

		// Casting
		//-------------------------------------------------------------------------------------------------------------
		"AbilityCastRange"				"700"
		"AbilityCastPoint"				"0.0"
		"AbilityCastAnimation"			"ACT_DOTA_CAST_ABILITY_1"

		// Time
		//-------------------------------------------------------------------------------------------------------------
		"AbilityCooldown"				"0.3"

		// Cost
		//-------------------------------------------------------------------------------------------------------------
		"AbilityManaCost"				"100"

		// Special
		//-------------------------------------------------------------------------------------------------------------
		"AbilitySpecial"
		{
			"01"
			{
				"var_type"					"FIELD_INTEGER"
				"damage"					"300"
			}
			"02"
			{
				"var_type"					"FIELD_INTEGER"
				"speed"						"1200"
			}
			"03"
			{
				"var_type"					"FIELD_INTEGER"
				"radius"					"60"
			}
			"04"
			{
				"var_type"					"FIELD_INTEGER"
				"projectile_cone"			"30"
			}
			"04"
			{
				"var_type"					"FIELD_INTEGER"
				"projectile_count"			"3"
			}
			"05"
			{
				"var_type"					"FIELD_INTEGER"
				"poison_damage"				"50"
			}
			"06"
			{
				"var_type"					"FIELD_FLOAT"
				"poison_duration"			"3.0"
			}
		}
	}

	//=================================================================================================================
	// Queen of Pain: Sonic Wave
	//=================================================================================================================
	"sonic_wave"
	{
		// General
		//-------------------------------------------------------------------------------------------------------------
		"BaseClass"						"ability_lua"
		"AbilityType"					"DOTA_ABILITY_TYPE_ULTIMATE"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_DIRECTIONAL | DOTA_ABILITY_BEHAVIOR_POINT"
		"AbilityUnitTargetTeam"			"DOTA_UNIT_TARGET_TEAM_ENEMY"
		"AbilityUnitTargetType"			"DOTA_UNIT_TARGET_HERO | DOTA_UNIT_TARGET_BASIC"
		"AbilityUnitTargetFlags"		"DOTA_UNIT_TARGET_FLAG_MAGIC_IMMUNE_ENEMIES"
		"AbilityUnitDamageType"			"DAMAGE_TYPE_PURE"
		"SpellImmunityType"				"SPELL_IMMUNITY_ENEMIES_YES"
		"FightRecapLevel"				"2"
		"AbilityTextureName"			"queenofpain_sonic_wave"
		"ScriptFile"					"components/abilities/heroes/queenofpain"
		"HasScepterUpgrade"				"1"
		"MaxLevel"						"1"

		// Casting
		//-------------------------------------------------------------------------------------------------------------
		"AbilityCastRange"				"700"
		"AbilityCastPoint"				"0.452"
		"AbilityCastAnimation"			"ACT_DOTA_CAST_ABILITY_4"

		// Time
		//-------------------------------------------------------------------------------------------------------------
		"AbilityCooldown"				"0.3"

		// Cost
		//-------------------------------------------------------------------------------------------------------------
		"AbilityGoldCost"				"100"

		// Precache
		//-------------------------------------------------------------------------------------------------------------
		"precache"
		{
			"soundfile"					"soundevents/game_sounds_heroes/game_sounds_queenofpain.vsndevts"
			"particle"					"particles/units/heroes/hero_queenofpain/queen_sonic_wave.vpcf"
			"particle"					"particles/hero/queenofpain/lifesteal.vpcf"
			"particle"					"particles/hero/queenofpain/self_lifesteal.vpcf"
		}

		// Special
		//-------------------------------------------------------------------------------------------------------------
		"AbilitySpecial"
		{
			"01"
			{
				"var_type"				"FIELD_INTEGER"
				"damage"				"900"
			}
			"02"
			{
				"var_type"				"FIELD_INTEGER"
				"start_radius"			"100"
			}
			"03"
			{
				"var_type"				"FIELD_INTEGER"
				"end_radius"			"450"
			}
			"04"
			{
				"var_type"				"FIELD_INTEGER"
				"travel_distance"		"900"
			}
			"05"
			{
				"var_type"				"FIELD_INTEGER"
				"projectile_speed"		"900"
			}
			"06"
			{
				"var_type"				"FIELD_INTEGER"
				"lifesteal_pct_scepter"	"50"
			}
		}
	}

	//=================================================================================================================
	// Lone Druid: Shockwave
	//=================================================================================================================
	"lone_druid_shockwave"
	{
		// General
		//-------------------------------------------------------------------------------------------------------------
		"BaseClass"						"ability_lua"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_POINT | DOTA_ABILITY_BEHAVIOR_IMMEDIATE | DOTA_ABILITY_BEHAVIOR_IGNORE_BACKSWING"
		"AbilityUnitTargetTeam"			"DOTA_UNIT_TARGET_TEAM_ENEMY"
		"AbilityUnitTargetType"			"DOTA_UNIT_TARGET_HERO | DOTA_UNIT_TARGET_BASIC"
		"AbilityUnitDamageType"			"DAMAGE_TYPE_PHYSICAL"
		"SpellImmunityType"				"SPELL_IMMUNITY_ENEMIES_NO"
		"FightRecapLevel"				"1"
		"AbilityTextureName"			"lone_druid_shockwave"
		"ScriptFile"					"components/abilities/heroes/lone_druid"
		"MaxLevel"						"1"

		// Precache
		//-------------------------------------------------------------------------------------------------------------
		"precache"
		{
			"soundfile"					"soundevents/game_sounds_heroes/game_sounds_magnataur.vsndevts"
			"particle"					"particles/units/heroes/hero_magnataur/magnataur_shockwave.vpcf"
			"particle"					"particles/units/heroes/hero_magnataur/magnataur_shockwave_hit.vpcf"
			"particle"					"particles/units/heroes/hero_magnataur/magnataur_shockwave_cast.vpcf"
		}

		// Casting
		//-------------------------------------------------------------------------------------------------------------
		"AbilityCastRange"				"700"
		"AbilityCastPoint"				"0.0"
		"AbilityCastAnimation"			"ACT_DOTA_CAST_ABILITY_1"

		// Time
		//-------------------------------------------------------------------------------------------------------------
		"AbilityCooldown"				"0.3"

		// Cost
		//-------------------------------------------------------------------------------------------------------------
		"AbilityManaCost"				"100"

		// Special
		//-------------------------------------------------------------------------------------------------------------
		"AbilitySpecial"
		{
			"01"
			{
				"var_type"					"FIELD_INTEGER"
				"damage"					"800"
			}
			"02"
			{
				"var_type"					"FIELD_INTEGER"
				"speed"						"1200"
			}
			"03"
			{
				"var_type"					"FIELD_INTEGER"
				"radius"					"150"
			}
		}
	}

	//=================================================================================================================
	// Lone Druid: Summon Bear
	//=================================================================================================================
	"lone_druid_summon_bear"
	{
		// General
		//-------------------------------------------------------------------------------------------------------------
		"BaseClass"						"ability_lua"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_POINT | DOTA_ABILITY_BEHAVIOR_IGNORE_BACKSWING"
		"AbilityUnitTargetTeam"			"DOTA_UNIT_TARGET_TEAM_ENEMY"
		"AbilityUnitTargetType"			"DOTA_UNIT_TARGET_HERO | DOTA_UNIT_TARGET_BASIC"
		"SpellImmunityType"				"SPELL_IMMUNITY_ENEMIES_NO"
		"FightRecapLevel"				"1"
		"AbilityTextureName"			"lone_druid_spirit_bear"
		"ScriptFile"					"components/abilities/heroes/lone_druid"
		"MaxLevel"						"1"

		// Precache
		//-------------------------------------------------------------------------------------------------------------
		"precache"
		{
//			"particle"					"particles/units/heroes/hero_magnataur/magnataur_shockwave.vpcf"
			"soundfile"					"soundevents/game_sounds_heroes/game_sounds_alchemist.vsndevts"
			"soundfile"					"soundevents/game_sounds_heroes/game_sounds_lone_druid.vsndevts"
		}

		// Casting
		//-------------------------------------------------------------------------------------------------------------
		"AbilityCastRange"				"600"
		"AbilityCastPoint"				"0.0"
		"AbilityCastAnimation"			"ACT_DOTA_CAST_ABILITY_1"

		// Time
		//-------------------------------------------------------------------------------------------------------------
		"AbilityCooldown"				"0.3"

		// Cost
		//-------------------------------------------------------------------------------------------------------------
		"AbilityGoldCost"				"100"

		// Special
		//-------------------------------------------------------------------------------------------------------------
		"AbilitySpecial"
		{
			"01"
			{
				"var_type"					"FIELD_FLOAT"
				"delay"						"1.0"
			}
			"01"
			{
				"var_type"					"FIELD_INTEGER"
				"bear_health"				"4000"
			}
			"01"
			{
				"var_type"					"FIELD_INTEGER"
				"bear_damage"				"400"
			}
		}
	}

	//=================================================================================================================
	// Enigma: Eidolings
	//=================================================================================================================
	"enigma_eidolings"
	{
		// General
		//-------------------------------------------------------------------------------------------------------------
		"BaseClass"						"ability_lua"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_POINT | DOTA_ABILITY_BEHAVIOR_IMMEDIATE | DOTA_ABILITY_BEHAVIOR_IGNORE_BACKSWING"
		"AbilityUnitTargetTeam"			"DOTA_UNIT_TARGET_TEAM_ENEMY"
		"AbilityUnitTargetType"			"DOTA_UNIT_TARGET_HERO | DOTA_UNIT_TARGET_BASIC"
		"AbilityUnitDamageType"			"DAMAGE_TYPE_PHYSICAL"
		"SpellImmunityType"				"SPELL_IMMUNITY_ENEMIES_NO"
		"FightRecapLevel"				"1"
		"AbilityTextureName"			"enigma_demonic_conversion"
		"ScriptFile"					"components/abilities/heroes/enigma"
		"MaxLevel"						"1"

		// Precache
		//-------------------------------------------------------------------------------------------------------------
		"precache"
		{
//			"particle"					"particles/units/heroes/hero_sniper/sniper_assassinate.vpcf"
			"particle"					"particles/units/heroes/hero_magnataur/magnataur_shockwave.vpcf"
			"particle"					"particles/units/heroes/hero_magnataur/magnataur_shockwave_hit.vpcf"
			"particle"					"particles/units/heroes/hero_magnataur/magnataur_shockwave_cast.vpcf"
			"soundfile"					"soundevents/game_sounds_heroes/game_sounds_alchemist.vsndevts"
			"soundfile"					"soundevents/game_sounds_heroes/game_sounds_magnataur.vsndevts"
		}

		// Casting
		//-------------------------------------------------------------------------------------------------------------
		"AbilityCastRange"				"1000"
		"AbilityCastPoint"				"0.0"
		"AbilityCastAnimation"			"ACT_DOTA_CAST_ABILITY_1"

		// Time
		//-------------------------------------------------------------------------------------------------------------
		"AbilityCooldown"				"0.4"

		// Cost
		//-------------------------------------------------------------------------------------------------------------
		"AbilityManaCost"				"100"

		// Special
		//-------------------------------------------------------------------------------------------------------------
		"AbilitySpecial"
		{
			"01"
			{
				"var_type"					"FIELD_INTEGER"
				"damage"					"380"
			}
			"02"
			{
				"var_type"					"FIELD_INTEGER"
				"speed"						"1500"
			}
			"03"
			{
				"var_type"					"FIELD_INTEGER"
				"radius"					"80"
			}
			"04"
			{
				"var_type"					"FIELD_INTEGER"
				"projectile_count"			"3"
			}
			"05"
			{
				"var_type"					"FIELD_FLOAT"
				"projectile_delay"			"0.2"
			}
		}
	}

	//=================================================================================================================
	// Enigma: Pocket Black Hole
	//=================================================================================================================
	"enigma_pocket_black_hole"
	{
		// General
		//-------------------------------------------------------------------------------------------------------------
		"BaseClass"						"ability_lua"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_POINT | DOTA_ABILITY_BEHAVIOR_IMMEDIATE | DOTA_ABILITY_BEHAVIOR_IGNORE_BACKSWING | DOTA_ABILITY_BEHAVIOR_AOE"
		"AbilityUnitTargetTeam"			"DOTA_UNIT_TARGET_TEAM_ENEMY"
		"AbilityUnitTargetType"			"DOTA_UNIT_TARGET_HERO | DOTA_UNIT_TARGET_BASIC"
		"AbilityUnitDamageType"			"DAMAGE_TYPE_MAGICAL"
		"SpellImmunityType"				"SPELL_IMMUNITY_ENEMIES_NO"
		"FightRecapLevel"				"1"
		"AbilityTextureName"			"enigma_black_hole"
		"ScriptFile"					"components/abilities/heroes/enigma"
		"AoERadius"						"400"
		"MaxLevel"						"1"

		// Precache
		//-------------------------------------------------------------------------------------------------------------
		"precache"
		{
			"soundfile"					"soundevents/game_sounds_heroes/game_sounds_magnataur.vsndevts"
			"particle"					"particles/units/heroes/hero_magnataur/magnataur_shockwave.vpcf"
			"particle"					"particles/units/heroes/hero_magnataur/magnataur_shockwave_hit.vpcf"
			"particle"					"particles/units/heroes/hero_magnataur/magnataur_shockwave_cast.vpcf"
//			"particle"					"particles/units/heroes/hero_sniper/sniper_assassinate.vpcf"
		}

		// Casting
		//-------------------------------------------------------------------------------------------------------------
		"AbilityCastRange"				"1000"
		"AbilityCastPoint"				"0.0"
		"AbilityCastAnimation"			"ACT_DOTA_CAST_ABILITY_1"

		// Time
		//-------------------------------------------------------------------------------------------------------------
		"AbilityCooldown"				"0.3"

		// Cost
		//-------------------------------------------------------------------------------------------------------------
		"AbilityGoldCost"				"100"

		// Special
		//-------------------------------------------------------------------------------------------------------------
		"AbilitySpecial"
		{
			"01"
			{
				"var_type"					"FIELD_INTEGER"
				"damage"					"800"
			}
			"02"
			{
				"var_type"					"FIELD_INTEGER"
				"radius"					"400"
			}
			"03"
			{
				"var_type"					"FIELD_FLOAT"
				"explosion_delay"			"1.0"
			}
			"04"
			{
				"var_type"					"FIELD_FLOAT"
				"duration"					"1.0"
			}
			"05"
			{
				"var_type"					"FIELD_INTEGER"
				"pull_strength"				"500"
			}
		}
	}

	//=================================================================================================================
	// Pudge's Butchers Cleaver
	//=================================================================================================================
	"bl_pudge_cleaver"
	{
		// General
		//-------------------------------------------------------------------------------------------------------------
		"BaseClass"						"ability_lua"
		"ScriptFile"					"components/abilities/heroes/pudge"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_POINT | DOTA_ABILITY_BEHAVIOR_IMMEDIATE | DOTA_ABILITY_BEHAVIOR_IGNORE_BACKSWING"
		"AbilityUnitTargetTeam"			"DOTA_UNIT_TARGET_TEAM_ENEMY"
		"AbilityUnitTargetType"			"DOTA_UNIT_TARGET_HERO | DOTA_UNIT_TARGET_BASIC"
		"AbilityUnitDamageType"			"DAMAGE_TYPE_MAGICAL"
		"SpellImmunityType"				"SPELL_IMMUNITY_ENEMIES_NO"
		"FightRecapLevel"				"1"
		"AbilityTextureName"			"pudge_cleaver"
		"MaxLevel"						"1"

		// Casting
		//-------------------------------------------------------------------------------------------------------------
		"AbilityCastRange"				"900"
		"AbilityCastPoint"				"0.0"
		"AbilityCastAnimation"			"ACT_DOTA_CAST_ABILITY_1"

		// Time
		//-------------------------------------------------------------------------------------------------------------
		"AbilityCastPoint"				"0.0"
		"AbilityCooldown"				"0.3"

		// Cost
		//-------------------------------------------------------------------------------------------------------------
		"AbilityManaCost"				"100"

		// Precache
		//-------------------------------------------------------------------------------------------------------------
		"precache"
		{
			"soundfile"					"soundevents/game_sounds_heroes/game_sounds_pudge.vsndevts"
			"particle" 					"particles/hero/pudge/pudge_cleaver_overhead.vpcf"
			"particle" 					"particles/hero/pudge/butchers_cleave_projectile.vpcf"
		}

		// Special
		//-------------------------------------------------------------------------------------------------------------
		"AbilitySpecial"
		{
			"01"
			{
				"var_type"				"FIELD_INTEGER"
				"damage"				"900"
			}
			"02"
			{
				"var_type"				"FIELD_INTEGER"
				"radius"				"150"
			}
			"03"
			{
				"var_type"				"FIELD_INTEGER"
				"range"					"1000"
			}
			"04"
			{
				"var_type"				"FIELD_INTEGER"
				"speed"					"900"
			}
		}
	}

	//=================================================================================================================
	// Pudge's Meat Hook
	//=================================================================================================================
	"bl_pudge_meat_hook"
	{
		// General
		//-------------------------------------------------------------------------------------------------------------
		"BaseClass"						"ability_lua"
		"ScriptFile"					"components/abilities/heroes/pudge"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_POINT | DOTA_ABILITY_BEHAVIOR_IGNORE_BACKSWING"
		"AbilityUnitTargetTeam"			"DOTA_UNIT_TARGET_TEAM_ENEMY"
		"AbilityUnitTargetType"			"DOTA_UNIT_TARGET_HERO | DOTA_UNIT_TARGET_BASIC"
		"AbilityUnitDamageType"			"DAMAGE_TYPE_MAGICAL"
		"SpellImmunityType"				"SPELL_IMMUNITY_ENEMIES_NO"
		"FightRecapLevel"				"1"
		"AbilityTextureName"			"pudge_meat_hook"
		"MaxLevel"						"1"

		// Casting
		//-------------------------------------------------------------------------------------------------------------
		"AbilityCastRange"				"1300"
		"AbilityCastPoint"				"0.3"
		"AbilityCastAnimation"			"ACT_DOTA_CAST_ABILITY_1"

		// Time
		//-------------------------------------------------------------------------------------------------------------
		"AbilityCooldown"				"0.3"

		// Cost
		//-------------------------------------------------------------------------------------------------------------
		"AbilityGoldCost"				"100"

		// Precache
		//-------------------------------------------------------------------------------------------------------------
		"precache"
		{
			"soundfile"					"soundevents/game_sounds_heroes/game_sounds_pudge.vsndevts"
//			"soundfile"					"soundevents/game_sounds_heroes/game_sounds_bloodseeker.vsndevts"
//			"particle"					"particles/units/heroes/hero_bloodseeker/bloodseeker_rupture.vpcf"
			"particle"					"particles/units/heroes/hero_pudge/pudge_meathook_chain.vpcf"
			"particle"					"particles/units/heroes/hero_pudge/pudge_meathook_impact.vpcf"			
		}

		// Special
		//-------------------------------------------------------------------------------------------------------------
		"AbilitySpecial"
		{
			"01"
			{
				"var_type"				"FIELD_INTEGER"
				"base_speed"			"1300"
			}
			"03"
			{
				"var_type"				"FIELD_INTEGER"
				"base_range"			"1300"
			}
			"05"
			{
				"var_type"				"FIELD_INTEGER"
				"base_damage"			"1400"
			}
			"07"
			{
				"var_type"				"FIELD_INTEGER"
				"hook_width"			"100"
			}
			"08"
			{
				"var_type"				"FIELD_INTEGER"
				"vision_radius"			"550"
			}
			"09"
			{
				"var_type"				"FIELD_INTEGER"
				"vision_duration"		"4"
			}
			"10"
			{
				"var_type"				"FIELD_FLOAT"
				"cooldown_cap_scepter"	"3.0"
			}
			"11"
			{
				"var_type"				"FIELD_FLOAT"
				"enemy_disable_linger"	"0.2"
			}
			"12"
			{
				"var_type"				"FIELD_INTEGER"
				"number_of_hooks"		"1"
			}
			"13"
			{
				"var_type"				"FIELD_INTEGER"
				"max_targets"			"1"
			}
		}
	}

	//=================================================================================================================
	// Sniper: Gust
	//=================================================================================================================
	"sniper_gust"
	{
		// General
		//-------------------------------------------------------------------------------------------------------------
		"BaseClass"						"ability_lua"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_POINT | DOTA_ABILITY_BEHAVIOR_IMMEDIATE | DOTA_ABILITY_BEHAVIOR_IGNORE_BACKSWING"
		"AbilityUnitTargetTeam"			"DOTA_UNIT_TARGET_TEAM_ENEMY"
		"AbilityUnitTargetType"			"DOTA_UNIT_TARGET_HERO | DOTA_UNIT_TARGET_BASIC"
		"AbilityUnitDamageType"			"DAMAGE_TYPE_PHYSICAL"
		"SpellImmunityType"				"SPELL_IMMUNITY_ENEMIES_NO"
		"FightRecapLevel"				"1"
		"AbilityTextureName"			"sniper_shrapnel"
		"ScriptFile"					"components/abilities/heroes/sniper"
		"MaxLevel"						"1"

		// Precache
		//-------------------------------------------------------------------------------------------------------------
		"precache"
		{
//			"particle"					"particles/units/heroes/hero_sniper/sniper_assassinate.vpcf"
			"particle"					"particles/units/heroes/hero_magnataur/magnataur_shockwave.vpcf"
			"particle"					"particles/units/heroes/hero_magnataur/magnataur_shockwave_hit.vpcf"
			"particle"					"particles/units/heroes/hero_magnataur/magnataur_shockwave_cast.vpcf"
			"soundfile"					"soundevents/game_sounds_heroes/game_sounds_alchemist.vsndevts"
			"soundfile"					"soundevents/game_sounds_heroes/game_sounds_magnataur.vsndevts"
		}

		// Casting
		//-------------------------------------------------------------------------------------------------------------
		"AbilityCastRange"				"1800"
		"AbilityCastPoint"				"0.0"
		"AbilityCastAnimation"			"ACT_DOTA_CAST_ABILITY_1"

		// Time
		//-------------------------------------------------------------------------------------------------------------
		"AbilityCooldown"				"0.6"

		// Cost
		//-------------------------------------------------------------------------------------------------------------
		"AbilityManaCost"				"100"

		// Special
		//-------------------------------------------------------------------------------------------------------------
		"AbilitySpecial"
		{
			"01"
			{
				"var_type"					"FIELD_INTEGER"
				"damage"					"180"
			}
			"02"
			{
				"var_type"					"FIELD_INTEGER"
				"speed"						"1600"
			}
			"03"
			{
				"var_type"					"FIELD_INTEGER"
				"radius"					"80"
			}
			"04"
			{
				"var_type"					"FIELD_INTEGER"
				"projectile_count"			"6"
			}
			"05"
			{
				"var_type"					"FIELD_FLOAT"
				"projectile_delay"			"0.1"
			}
		}
	}

	//=================================================================================================================
	// Sven: Holy Cleave
	//=================================================================================================================
	"bl_sven_holy_cleave"
	{
		// General
		//-------------------------------------------------------------------------------------------------------------
		"BaseClass"						"ability_lua"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_DIRECTIONAL | DOTA_ABILITY_BEHAVIOR_POINT"
		"AbilityUnitTargetTeam"			"DOTA_UNIT_TARGET_TEAM_ENEMY"
		"AbilityUnitTargetType"			"DOTA_UNIT_TARGET_HERO | DOTA_UNIT_TARGET_BASIC"
		"AbilityUnitTargetFlags"		"DOTA_UNIT_TARGET_FLAG_MAGIC_IMMUNE_ENEMIES"
		"AbilityUnitDamageType"			"DAMAGE_TYPE_PHYSICAL"
		"SpellImmunityType"				"SPELL_IMMUNITY_ENEMIES_YES"
		"FightRecapLevel"				"2"
		"AbilityTextureName"			"sven_great_cleave"
		"ScriptFile"					"components/abilities/heroes/sven"
		"MaxLevel"						"1"

		// Casting
		//-------------------------------------------------------------------------------------------------------------
		"AbilityCastRange"				"550"
		"AbilityCastPoint"				"0.5"
//		"AbilityCastAnimation"			"ACT_DOTA_CAST_ABILITY_4"

		// Time
		//-------------------------------------------------------------------------------------------------------------
		"AbilityCooldown"				"0.0"

		// Cost
		//-------------------------------------------------------------------------------------------------------------
		"AbilityManaCost"				"100"

		// Precache
		//-------------------------------------------------------------------------------------------------------------
		"precache"
		{
			"soundfile"					"soundevents/game_sounds_heroes/game_sounds_sven.vsndevts"
			"particle"					"particles/econ/items/sven/sven_ti7_sword/sven_ti7_sword_spell_great_cleave.vpcf"
		}

		// Special
		//-------------------------------------------------------------------------------------------------------------
		"AbilitySpecial"
		{
			"01"
			{
				"var_type"				"FIELD_INTEGER"
				"damage"				"900"
			}
			"02"
			{
				"var_type"				"FIELD_INTEGER"
				"start_radius"			"200"
			}
			"03"
			{
				"var_type"				"FIELD_INTEGER"
				"end_radius"			"400" 
			}
			"04"
			{
				"var_type"				"FIELD_INTEGER"
				"travel_distance"		"150" // + 400 end radius
			}
			"05"
			{
				"var_type"				"FIELD_INTEGER"
				"projectile_speed"		"500" // 0.25 seconds with 400
			}
		}
	}

	//=================================================================================================================
	// Generic: Second Wind
	//=================================================================================================================
	"generic_second_wind"
	{
		// General
		//-------------------------------------------------------------------------------------------------------------
		"BaseClass"						"ability_lua"
		"ScriptFile"					"components/abilities/generic"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE"
		"SpellImmunityType"				"SPELL_IMMUNITY_ALLIES_YES"
		"AbilityTextureName"			"generic_second_wind"
		"MaxLevel"						"1"
		"FightRecapLevel"				"1"

		// Time		
		//-------------------------------------------------------------------------------------------------------------
		"AbilityCooldown"				"4.0"

		// Special
		//-------------------------------------------------------------------------------------------------------------
		"AbilitySpecial"
		{
			"01"
			{
				"var_type"				"FIELD_FLOAT"
				"heal_regen_pct"		"20.0"
			}
			"02"
			{
				"var_type"				"FIELD_FLOAT"
				"heal_interval"			"1.0"
			}
		}
	}

	//=================================================================================================================
	// Drow Ranger: Multishot
	//=================================================================================================================
	"multishot"
	{
		// General
		//-------------------------------------------------------------------------------------------------------------
		"BaseClass"						"ability_lua"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_POINT | DOTA_ABILITY_BEHAVIOR_IMMEDIATE | DOTA_ABILITY_BEHAVIOR_IGNORE_BACKSWING"
		"AbilityUnitTargetTeam"			"DOTA_UNIT_TARGET_TEAM_ENEMY"
		"AbilityUnitTargetType"			"DOTA_UNIT_TARGET_HERO | DOTA_UNIT_TARGET_BASIC"
		"AbilityUnitDamageType"			"DAMAGE_TYPE_PHYSICAL"
		"SpellImmunityType"				"SPELL_IMMUNITY_ENEMIES_NO"
		"FightRecapLevel"				"1"
		"AbilityTextureName"			"multishot"
		"ScriptFile"					"components/abilities/heroes/drow_ranger"
		"MaxLevel"						"1"

		// Precache
		//-------------------------------------------------------------------------------------------------------------
		"precache"
		{
//			"soundfile"					"soundevents/game_sounds_heroes/game_sounds_magnataur.vsndevts"
			"particle"					"particles/units/heroes/hero_drow/drow_frost_arrow.vpcf"
			"particle"					"particles/generic_gameplay/generic_slowed_cold.vpcf"
			"particle"					"particles/status_fx/status_effect_frost.vpcf"
		}

		// Casting
		//-------------------------------------------------------------------------------------------------------------
		"AbilityCastRange"				"700"
		"AbilityCastPoint"				"0.0"
		"AbilityCastAnimation"			"ACT_DOTA_CAST_ABILITY_1"

		// Time
		//-------------------------------------------------------------------------------------------------------------
		"AbilityCooldown"				"0.5"

		// Cost
		//-------------------------------------------------------------------------------------------------------------
		"AbilityManaCost"				"100"

		// Special
		//-------------------------------------------------------------------------------------------------------------
		"AbilitySpecial"
		{
			"01"
			{
				"var_type"					"FIELD_INTEGER"
				"damage"					"250"
			}
			"02"
			{
				"var_type"					"FIELD_INTEGER"
				"speed"						"1200"
			}
			"03"
			{
				"var_type"					"FIELD_INTEGER"
				"radius"					"60"
			}
			"04"
			{
				"var_type"					"FIELD_INTEGER"
				"projectile_cone"			"30"
			}
			"04"
			{
				"var_type"					"FIELD_INTEGER"
				"projectile_count"			"5"
			}
			"05"
			{
				"var_type"					"FIELD_INTEGER"
				"movement_slow"				"20"
			}
			"06"
			{
				"var_type"					"FIELD_FLOAT"
				"duration_slow"				"1.0"
			}
		}
	}

	//=================================================================================================================
	// Windrunner Powershot
	//=================================================================================================================
	"bl_windrunner_powershot"
	{
		// General
		//-------------------------------------------------------------------------------------------------------------
		"BaseClass"						"ability_lua"
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_POINT | DOTA_ABILITY_BEHAVIOR_IMMEDIATE | DOTA_ABILITY_BEHAVIOR_IGNORE_BACKSWING"
		"AbilityUnitTargetTeam"			"DOTA_UNIT_TARGET_TEAM_ENEMY"
		"AbilityUnitTargetType"			"DOTA_UNIT_TARGET_HERO | DOTA_UNIT_TARGET_BASIC"
		"AbilityUnitDamageType"			"DAMAGE_TYPE_PHYSICAL"
		"SpellImmunityType"				"SPELL_IMMUNITY_ENEMIES_NO"
		"FightRecapLevel"				"1"
		"AbilityTextureName"			"windrunner_powershot"
		"ScriptFile"					"components/abilities/heroes/windrunner"
		"MaxLevel"						"1"

		// Precache
		//-------------------------------------------------------------------------------------------------------------
		"precache"
		{
			"soundfile"					"soundevents/game_sounds_heroes/game_sounds_windrunner.vsndevts"
			"particle"					"particles/units/heroes/hero_windrunner/windrunner_spell_powershot.vpcf"
		}

		// Casting
		//-------------------------------------------------------------------------------------------------------------
		"AbilityCastRange"				"900"
		"AbilityCastPoint"				"0.0"
		"AbilityCastAnimation"			"ACT_DOTA_CAST_ABILITY_1"

		// Time
		//-------------------------------------------------------------------------------------------------------------
		"AbilityCooldown"				"0.3"

		// Cost
		//-------------------------------------------------------------------------------------------------------------
		"AbilityManaCost"				"100"

		// Special
		//-------------------------------------------------------------------------------------------------------------
		"AbilitySpecial"
		{
			"01"
			{
				"var_type"					"FIELD_INTEGER"
				"damage"					"700"
			}
			"02"
			{
				"var_type"					"FIELD_INTEGER"
				"speed"						"1500"
			}
			"03"
			{
				"var_type"					"FIELD_INTEGER"
				"radius"					"150"
			}
		}
	}
}

"use strict";

function ShowBossBar(args) {
	$.Msg(args)

	var panel_name = "Radiant"

	if (args.team == 3)
		panel_name = "Dire"

	$("#" + panel_name + "TowerHP").style.visibility = "visible";
	$("#" + panel_name + "TowerLabel").text = $.Localize(args.boss_name);
	$("#" + panel_name + "TowerIcon").heroname = args.boss_icon;
	$("#" + panel_name + "TowerHealth").text = args.boss_health + " / " + args.boss_max_health;
	$("#" + panel_name + "TowerProgressBar").value = args.boss_health / args.boss_max_health;
	$("#" + panel_name + "TowerProgressBar_Left").style.backgroundColor = "gradient( linear, 0% 0%, 0% 100%, from( " + args.dark_color + " ), color-stop( 0.3, " + args.light_color + " ), color-stop( .5, " + args.light_color + " ), to( " + args.dark_color + " ) )";
}

function HideBossBar(args) {
	$("#RadiantTowerHP").style.visibility = "collapse";
	$("#RadiantTowerLabel").text = "";
	$("#RadiantTowerIcon").heroname = "";
	$("#RadiantTowerHealth").text = "";
	$("#RadiantTowerProgressBar").value = 100;
	$("#RadiantTowerProgressBar_Left").style.backgroundColor = "gradient( linear, 0% 0%, 0% 100%, from( #320000 ), color-stop( 0.3, #a30f0f ), color-stop( .5, #a30f0f ), to( #320000 ) )";

	$("#DireTowerHP").style.visibility = "collapse";
	$("#DireTowerLabel").text = "";
	$("#DireTowerIcon").heroname = "";
	$("#DireTowerHealth").text = "";
	$("#DireTowerProgressBar").value = 100;
	$("#DireTowerProgressBar_Left").style.backgroundColor = "gradient( linear, 0% 0%, 0% 100%, from( #320000 ), color-stop( 0.3, #a30f0f ), color-stop( .5, #a30f0f ), to( #320000 ) )";
}

(function () {
	GameEvents.Subscribe("show_boss_hp", ShowBossBar);
	GameEvents.Subscribe("hide_boss_hp", HideBossBar);
})();

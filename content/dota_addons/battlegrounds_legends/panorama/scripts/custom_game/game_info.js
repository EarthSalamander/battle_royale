"use strict";

(function() {
	var panel_color = "#25292f";

	if (Game.GetMapInfo().map_display_name == "gem_grab")
		panel_color = "#004000aa"
	else if (Game.GetMapInfo().map_display_name == "battle_royale")
		panel_color = "#804600aa"
	else if (Game.GetMapInfo().map_display_name == "boss_fight")
		panel_color = "#004050aa"

	$.GetContextPanel().GetParent().GetParent().style.backgroundColor = panel_color;

	$.GetContextPanel().style.backgroundColor = "none";
	$.GetContextPanel().GetParent().style.backgroundColor = "none";

	$("#Intro02").text = $.Localize(Game.GetMapInfo().map_display_name).toUpperCase();
	$("#Intro03").text = $.Localize("tutorial_intro_" + Game.GetMapInfo().map_display_name);

	var AbilityPanel = $("#AbilitiesImage");

	var abilities = [
		"enigma_eidolings",
		"enigma_pocket_black_hole",
		"lone_druid_shockwave",
		"lone_druid_summon_bear",
		"bl_pudge_cleaver",
		"bl_pudge_meat_hook",
		"shadow_strike",
		"sonic_wave",
//		"sniper_gust",
//		"sniper_assassinate",
	]

	if (AbilityPanel) {
		for (var i = 0; i < abilities.length; ++i) {
			var abilityPanelName = "tutorial_ability_" + i;
			var abilityPanel = AbilityPanel.FindChild(abilityPanelName);

			if (abilityPanel === null) {
				// Needs DOTAAbilityImage to be able to load from flash3 images
				// (similar to those used for dota shop, hence reusing
				// existing resources)
				abilityPanel = $.CreatePanel("DOTAAbilityImage", AbilityPanel, abilityPanelName);
				abilityPanel.AddClass("AbilityIcon")
			}

			if (abilities[i]) {
				abilityPanel.abilityname = abilities[i];

				(function (abilityPanel, ability) {
					abilityPanel.SetPanelEvent("onmouseover", function () {
						$.DispatchEvent("DOTAShowAbilityTooltip", abilityPanel, ability);
					})
					abilityPanel.SetPanelEvent("onmouseout", function () {
						$.DispatchEvent("DOTAHideAbilityTooltip", abilityPanel);
					})
				})(abilityPanel, abilities[i]);
			} else {
				abilityPanel.abilityname = "";
			}
		}
	}

	var ItemPanel = $("#ItemsImage");

	var items = [
		"item_treasure_morbid_mask",
		"item_treasure_scepter_of_mastery",
		"item_blink",
	]

	if (ItemPanel) {
		for (var i = 0; i < items.length; ++i) {
			var itemPanelName = "tutorial_item_" + i;
			var itemPanel = ItemPanel.FindChild(itemPanelName);

			if (itemPanel === null) {
				// Needs DOTAItemImage to be able to load from flash3 images
				// (similar to those used for dota shop, hence reusing
				// existing resources)
				itemPanel = $.CreatePanel("DOTAItemImage", ItemPanel, itemPanelName);
				itemPanel.AddClass("AbilityIcon")
			}

			if (items[i]) {
				itemPanel.itemname = items[i];
			} else {
				itemPanel.itemname = "";
			}
		}
	}

	var DeveloperPanel = $("#DeveloperSteam");

	var developers = [
		"76561198015161808",
	]

	if (DeveloperPanel) {
		for (var i = 0; i < developers.length; ++i) {
			var developerPanelName = "tutorial_developer_" + i;
			var developerPanel = DeveloperPanel.FindChild(developerPanelName);

			if (developerPanel === null) {
				var steam_id = $.CreatePanel("DOTAAvatarImage", DeveloperPanel, "player_steamid_" + i);
				steam_id.steamid = developers[i];
				steam_id.style.width = "38px";
				steam_id.style.height = "38px";
				steam_id.style.marginTop = "20px";
				steam_id.style.marginRight = "40px";
				steam_id.style.align = "center center";
			}

//			if (developers[i]) {
//				developerPanel.steamid = developers[i];
//			} else {
//				developerPanel.steamid = "";
//			}
		}
	}
})();

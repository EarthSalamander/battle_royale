"use strict";

function UpdateTimer( data )
{
	var timerText = "";
	timerText += data.timer_minute_10;
	timerText += data.timer_minute_01;
	timerText += ":";
	timerText += data.timer_second_10;
	timerText += data.timer_second_01;

	$( "#Timer" ).text = timerText;

	var behemoth_count = CustomNetTables.GetTableValue("game_score", "behemoth_count");
	var behemoth_max_count = CustomNetTables.GetTableValue("game_score", "behemoth_max_count");

	if (behemoth_count) {
		if (behemoth_max_count) {
			$("#Wave").style.visibility = "visible";
			$("#Wave").text = "Boss Wave: " + behemoth_count[1] + "/" + behemoth_max_count[1];
		}
	}
}

function ShowTimer( data )
{
	$( "#Timer" ).AddClass( "timer_visible" );
}

function AlertTimer( data )
{
	$( "#Timer" ).AddClass( "timer_alert" );
}

function HideTimer( data )
{
	$( "#Timer" ).AddClass( "timer_hidden" );
}
/*
function UpdateKillsToWin()
{
	var victory_condition = CustomNetTables.GetTableValue( "game_state", "victory_condition" );
	if ( victory_condition )
	{
		$("#VictoryPoints").text = victory_condition.kills_to_win;
	}
}
*/
function OnGameScoreChanged( table, key, data )
{
//	$.Msg( "Table '", table, "' changed: '", key, "' = ", data );

	var team_max_score = CustomNetTables.GetTableValue("game_score", "score_to_win").team_score;
	$("#RadiantScore").text = CustomNetTables.GetTableValue("game_score", "2").team_score + "/" + team_max_score;
	$("#DireScore").text = CustomNetTables.GetTableValue("game_score", "3").team_score + "/" + team_max_score;
}

(function()
{
	// We use a nettable to communicate victory conditions to make sure we get the value regardless of timing.
//	UpdateKillsToWin();

	if (Game.GetMapInfo().map_display_name == "battle_royale") {
		$.GetContextPanel().DeleteAsync(0);
		return;
	}

	if (Game.GetMapInfo().map_display_name == "battle_royale" || Game.GetMapInfo().map_display_name == "boss_fight")
	{
		$("#Victory").style.visibility = "visible";

		if (Game.GetMapInfo().map_display_name == "battle_royale") {
			$("#Goal").text = "Tempest Timer";
			$("#RadiantScore").DeleteAsync(0);
			$("#DireScore").DeleteAsync(0);
		}
	} else if (Game.GetMapInfo().map_display_name == "gem_grab") {
		$("#Victory").style.visibility = "visible";
		$("#Goal").text = "Rounds won:";
		$("#Timer").style.visibility = "collapse";
	}

    GameEvents.Subscribe( "countdown", UpdateTimer );
    GameEvents.Subscribe( "show_timer", ShowTimer );
    GameEvents.Subscribe( "timer_alert", AlertTimer );
    GameEvents.Subscribe( "overtime_alert", HideTimer );
	//UpdateTimer();
})();

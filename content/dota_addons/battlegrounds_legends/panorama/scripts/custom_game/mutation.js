function OnItemWillSpawn( msg )
{
//	$.Msg( "OnItemWillSpawn: ", msg );
	$.GetContextPanel().SetHasClass( "item_will_spawn", true );
	$.GetContextPanel().SetHasClass( "item_has_spawned", false );
	GameUI.PingMinimapAtLocation( msg.spawn_location );

//	$( "#AlertMessage_Chest" ).html = true;
//	$( "#AlertMessage_Delivery" ).html = true;
//	$( "#AlertMessage_Chest" ).text = $.Localize( "#Chest" );
//	$( "#AlertMessage_Delivery" ).text = $.Localize( "#ItemWillSpawn" );

	$.Schedule( 3, ClearItemSpawnMessage );
}

function ClearItemSpawnMessage()
{
	$.GetContextPanel().SetHasClass( "item_will_spawn", false );
	$.GetContextPanel().SetHasClass( "item_has_spawned", false );

//	$( "#AlertMessage" ).text = "";
}

var mutation = []

function OnMutationUpdated()
{
	var mutations = CustomNetTables.GetTableValue("game_options", "mutation")["1"]
	mutation[1] = mutations["positive"]
	mutation[2] = mutations["negative"]
	mutation[3] = mutations["terrain"]

	var mutation_info = {}

	for (var j = 1; j <= 2; j++) {
		mutation_info[j] = CustomNetTables.GetTableValue("mutation_info", mutation[j])
		if (mutation_info[j] != undefined) {
			if (mutation[j] == "tempest")
				MutationTimer(mutation_info[j], $("#Mutation2Label"));
		}
	}

//	$.Msg(mutations);
//	$.Msg(mutation_info);
}

var alert = 0
function TimerAlert(data) {
	alert = data["1"]
}

function MutationTimer(data, panel)
{
	var timerText = "";
	timerText += data.timer_minute_10;
	timerText += data.timer_minute_01;
	timerText += ":";
	timerText += data.timer_second_10;
	timerText += data.timer_second_01;

	panel.text = $.Localize("mutation_" + mutation[2]) + ": " + timerText;

	if (alert == 1) {
		if (!panel.BHasClass("alert"))
			panel.AddClass("alert")
	} else {
		if (panel.BHasClass("alert"))
			panel.RemoveClass("alert")
	}
}

(function () {
	GameEvents.Subscribe("update_mutations", OnMutationUpdated);
	GameEvents.Subscribe("timer_alert", TimerAlert);
	GameEvents.Subscribe("item_will_spawn", OnItemWillSpawn);
})();
